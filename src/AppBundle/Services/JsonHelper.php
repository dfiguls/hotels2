<?php
namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use BookingTrend\PDSBundle\Entity\RuleLog;

class JsonHelper {
    
    public static function processCRUD($doctrine, $serializer, $validator, $logger, $user, $request, $id, $className, $groups, $postNew=null, $postNormalize=null, $prePersist=null) {
        return new JsonResponse(self::_processCRUD($doctrine, $serializer, $validator, 
                $logger, $user, $request, $id, $className, $groups, 
                $postNew, $postNormalize, $prePersist));
    }
    
    public static function _processCRUD($doctrine, $serializer, $validator, $logger, $user, $request, $id, $className, $groups, 
            $postNew=null, $postNormalize=null, $prePersist=null, $postPersist=null) {
        $em=$doctrine->getManager();
        $repository = $doctrine->getRepository($className);       
        $idUser=($user?$user->getId():-1);
        $entity=$repository->find($id);
        if (!$entity) {
            $namespaceClassName=$em->getClassMetadata($className)->getName();
            $entity=new $namespaceClassName();
        }
        
        if ($request->isMethod('GET')) {
            $diff=array();
            if ($postNew) $postNew($entity,$diff);
            $normalized=$serializer->normalize($entity, null, array('groups' => $groups));
            if ($postNormalize) $normalized=$postNormalize($normalized, $em, $serializer);
            return $normalized; 
        } else if ($request->isMethod('PUT')){
            //add logs
            $msg='PUT '.$className.': user:'.$idUser.'  id:'.$id.'==>'.$request->getContent();
            $logger->info($msg);
            
            $diff = json_decode($request->getContent(), true);
            if ($postNew) $postNew($entity,$diff); //allows to manipulate $diff
            $entitiesToRemove=array();
            $newEntities=array();
            JsonHelper::updateFromDiffJSON($em,$entity,$diff,$entitiesToRemove,$newEntities);
            $errors = $validator->validate($entity);
            if (count($errors)>0) {
                $normalized=array();
                foreach($errors as $error) 
                    $normalized[]=array('path'=>$error->getPropertyPath(),'message'=>$error->getMessage());
                return array('ok'=>false,'errors'=>$normalized); 
            } else {
                foreach ($entitiesToRemove as $e) $em->remove($e);
                if ($prePersist) $prePersist($entity,$entitiesToRemove);
                $em->persist($entity);
                $em->flush();
                if ($postPersist) $postPersist($entity);
                return array('ok'=>true,'id'=>$entity->getId()); 
            }
        } else if ($request->isMethod('DELETE')){
            $diff=array();
            if ($postNew) $postNew($entity,$diff);
            //add logs
            $msg='DELETE '.$className.' user:'.$idUser.'==>'.$id;
            $logger->info($msg);
            
            $em->remove($entity);
            $em->flush();
            return array('ok'=>true,'id'=>$entity->getId()); 
        }
    }
    
    static function getSingular($name) {
        if (substr($name,-3)==='ies') return substr($name,0,-3).'y'; //replace ...ies to ...y
        return substr($name,0,-1); //remove ending 's'
    }
    
    public static function updateFromDiffJSON($em,$entity,$diff, &$entitiesToDelete, &$newEntities) {      
        $reflection = new \ReflectionObject($entity);
        $meta=$em->getMetadataFactory()->getMetadataFor(get_class($entity));                     
        foreach($diff as $id=>$value) {   
            //value is an string or an array
            if ($id==='id' && $value<0) { 
                $newEntities[$value]=$entity;                 
            } else if ($id==='__row') { //skip row information
            } else if (substr($id,0,2)==='__') { //skip                
            } else if (is_array($value)) { //1-1 or 1-N or M-N relation
                $getMethod = $reflection->getMethod('get'.ucfirst($id)); //id is plural for 1-N, N-M   singular for 1-1
                $idSingular=self::getSingular(ucfirst($id));
                if (!$reflection->hasMethod('add'.$idSingular)) { //1-1 relation
                    $childEntity = $getMethod->invoke($entity); //returns a object
                    if (!$childEntity) { //create new instance
                        $setMethod=$reflection->getMethod('set'.ucfirst($id));
                        $params=$setMethod->getParameters();
                        $param=$params[0]->getClass();
                        $childEntity=$param->newInstance();
                        $setMethod->invoke($entity, $childEntity); //add child to parent
                        
                        //set foreign key (in doctrine, a reference to the main entity)
                        $childsMeta=$meta->getAssociationMapping($id);
                        $mappedBy=$childsMeta['mappedBy'];
                        $childReflection = new \ReflectionObject($childEntity);
                        $childSetMethod = $childReflection->getMethod('set'.ucfirst($mappedBy));
                        $childSetMethod->invoke($childEntity, $entity); //reference parent from child
                    }
                    self::updateFromDiffJSON($em,$childEntity,$value, $entitiesToDelete, $newEntities);
                } else {
                    $addMethod=$reflection->getMethod('add'.$idSingular);
                    $childsMeta=$meta->getAssociationMapping($id);
                    $mappedBy=$childsMeta['mappedBy'];
                    $childEntities = $getMethod->invoke($entity); //returns a collection
                    $originalChildCount=$childEntities->count();
                    $prevChildEntities=$childEntities->toArray();
                    $childEntities->clear();                    
                    $rowsDone=array();
                    for ($i=0;$i<count($value);$i++) {
                        $childDiff=$value[$i];            
                        if (array_key_exists('__row', $childDiff)) {
                            $rowsDone[]=$childDiff['__row'];
                            $childEntity=$prevChildEntities[$childDiff['__row']];
                            $childEntities->add($childEntity);
                            self::updateFromDiffJSON($em, $childEntity, $childDiff, $entitiesToDelete, $newEntities);
                        } else {
                            //create new child entity and add to the collection
                            $params=$addMethod->getParameters();
                            $param=$params[0]->getClass();
                            $className=$param->getName();
                            $repository=$em->getRepository($className);
                            if ($mappedBy) { //1-N
                                $newInstance=  !(array_key_exists('id',$childDiff) && $childDiff['id']>0);
                                if ($newInstance) $childEntity=$param->newInstance();
                                else $childEntity= $repository->find($childDiff['id']);
                                $childEntities->add($childEntity);

                                //set foreign key (in doctrine, a reference to the main entity)
                                $childReflection = new \ReflectionObject($childEntity);
                                $setMethod = $childReflection->getMethod('set'.ucfirst($mappedBy));
                                $setMethod->invoke($childEntity, $entity);   
                                if ($newInstance) self::updateFromDiffJSON($em, $childEntity, $childDiff, $entitiesToDelete, $newEntities);
                            } else { //M-N                            
                                $props=$meta->getAssociationMapping($id);
                                $key=$props['joinTable']['inverseJoinColumns'][0]['referencedColumnName'];
                                $keyValue=$childDiff[$key];
                                $obj = $repository->find($keyValue);
                                $childEntities->add($obj);
                                //$addMethod->invoke($entity,$obj);
                            }
                        }                    
                    }

                    //remove deleted rows
                    $cascadeDelete=in_array('remove',$childsMeta['cascade']);
                    for ($i=$originalChildCount-1;$i>=0;$i--) {
                        if (array_search($i,$rowsDone)===false) {
                            if ($cascadeDelete) {
                                $entitiesToDelete[]=$prevChildEntities[$i];
                            } else {
                                //set foreign key to null
                                $childEntity=$prevChildEntities[$i];
                                $childReflection = new \ReflectionObject($childEntity);
                                //if method exists it is a 1-N relationship, otherwhise is a MN
                                if ($childReflection->hasMethod('set'.$mappedBy)) {
                                    $setMethod = $childReflection->getMethod('set'.$mappedBy);
                                    $setMethod->invoke($childEntity, null);   
                                }
                            }
                        }
                    }
                }
                
            } else {
                //if $id is a N-1 attribute, will end with ...Id-> so remove id to access to the right setter
                $isN1=false;
                if (substr($id,-2)==='Id') { $id=substr($id,0,-2); $isN1=true; }
                
                $method = $reflection->getMethod('set'.ucfirst($id));
                $param=$method->getParameters();
                $param=$param[0]->getClass();
                if ($param) { 
                    if ($isN1) { //N-1 and 1-1 relation--> parameter is an entity
                        $className=$param->getName();
                        $repository=$em->getRepository($className);
                        if ($value===null) $obj=null;
                        else if ($value<0) $obj=$newEntities[$value]; //to reference newly created entities
                        else $obj = $repository->find($value); 
                        $method->invoke($entity,$obj);
                    } else if ($value===null){
                        //is 1-1 and value=null
                        $getMethod = $reflection->getMethod('get'.ucfirst($id));
                        $childEntity = $getMethod->invoke($entity); 
                        $method->invoke($entity,null);
                        $entitiesToDelete[]=$childEntity;
                    }
                    
                } else {
                    $type=$meta->getTypeOfField($id);
                    if($type=='date' || $type=='datetime') $value= ($value?\DateTime::createFromFormat('!Y-m-d', $value):null);
                    elseif ($type==='boolean') $value=$value===true || $value==='true' || $value==='1';
                    $method->invoke($entity,$value);
                }
            }
        }        
    }
    
    
    public static function processDataTables($repository, $fixedConditions, $request, $factory) {
        $draw=$request->get('draw');
        $start=$request->get('start');
        $length=$request->get('length');
        $search=$request->get('search');
        $searchValue=$search['value'];
        $searchCustom=$search['custom'];
        $order=$request->get('order');
        $orderColumn=$order[0]['column'];
        $orderOrder=$order[0]['dir']; //asc, desc        
        
        $total = $repository->count($fixedConditions,$searchValue,$searchCustom);
        $list = $repository->select($fixedConditions,$searchValue,$searchCustom, $orderColumn, $orderOrder, $start, $length)->getQuery()->getResult();
        $result=array();
        foreach($list as $o) {
            $result[]=$factory($o);
        }
        return new JsonResponse(array(
            "length"=>$total,  
            'rows'=>$result
        ));   
    }
    
    public static function selectDataTables($repository, $fixedConditions, $request) {
        $search=$request->get('search');
        $searchValue=$search['value'];
        $searchCustom=$search['custom'];
        $order=$request->get('order');
        $orderColumn=$order[0]['column'];
        $orderOrder=$order[0]['dir']; //asc, desc        
        
        $list = $repository->select($fixedConditions,$searchValue,$searchCustom, $orderColumn, $orderOrder, null, null)->getQuery()->getResult();

        return $list;
    }
    
}

