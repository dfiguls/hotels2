<?php
namespace AppBundle\Services;

use AppBundle\Entity\CacheDispo;
use AppBundle\Entity\CacheRate;
use AppBundle\Entity\RuleSet;

class CacheCalendarManager {
    protected $em, $bd;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em=$em;
        $this->bd=$em->getConnection();
    }
    
    public function ruleSetToCache($ruleSet, $dateIn, $dateEnd) {        
        //if combinations are different --> reset everything and start from zero
        $parameters=$ruleSet->mergeRuleParameters();
        $combinations=$ruleSet->mergeRuleCombinations($parameters);
        if (!RuleSet::combinationsAreEquals($ruleSet->getCombinations(), $combinations)) {
            $this->clearRuleSet($ruleSet);
            list ($dateIn, $dateEnd)=$ruleSet->getDatesInterval();
            foreach($combinations as $combination) {
                $ruleSet->addCombination($combination);
            }
            $this->em->persist($ruleSet);
            $this->em->flush();
        }

        //load rules related to dateIn-dateEnd
        $rules = $this->em->getRepository('AppBundle:Rule')
                ->findByDates($ruleSet, $dateIn->format('Y-m-d'), $dateEnd->format('Y-m-d'))
                ->getQuery()->getResult();
        
        $cacheCalendars=$this->em->getRepository('AppBundle:CacheCalendar')
                ->findByDates($dateIn,$dateEnd, $ruleSet);
        
        //rememver original dispos, reservations and rates to optimize updates, and clear current cacheCalendars
        $originalCalendars=array(); 
        foreach($cacheCalendars as $cacheCalendar) {
            $originalCalendars[]=clone $cacheCalendar; //copy original
            
            $cacheCalendar->setReleaseDays(null);
            $cacheCalendar->setRuleSet(null);
            foreach($cacheCalendar->getCacheDispos() as $cacheDispo) {
                $cacheDispo->setDispo(null);
                $cacheDispo->setReservations(null);
            }
            foreach($cacheCalendar->getCacheRates() as $cacheRate) {
                $cacheRate->setRate(null);        
                $cacheRate->setFormula(null);
            }
        }
        
        foreach($rules as $rule) $this->processRule($dateIn, $rule, $cacheCalendars, $ruleSet->getCombinations());
        
        $this->syncCacheCalendars($originalCalendars, $cacheCalendars);
    }
    
    
    
        
        
        //find reservations
        /*TODO $reservationsRepository=$this->em->getRepository('BookingTrendPDSBundle:Reservation');
        foreach($cacheCalendars as $cacheCalendar) {
            if ($cacheCalendar->getRule()) { //only add reservations if the cachecalendar belongs to a base rule or promo
                //If the admin modifies the dates of a promotion that had a previous reservation, we can leave some days with reservation but without promo
                $reservations=$reservationsRepository->findByDatePromo($cacheCalendar->getDate(),$isBase?null:$rules);
                foreach($reservations as $reservationCount) {
                    $reservation=$reservationCount[0];
                    $count=$reservationCount[1];
                    $cacheDispo=$cacheCalendar->findCacheDispo($reservation->getMoorageType()->getId());
                    if (!$cacheDispo) {
                        $cacheDispo=new CacheDispo();
                        $cacheDispo->setCacheCalendar($cacheCalendar);
                        $cacheDispo->setMoorageType($reservation->getMoorageType());
                        $cacheCalendar->addCacheDispo($cacheDispo);
                    }
                    $cacheDispo->setReservations($count);
                }
            }
        }*/
    
    
   
    
    public function processRule($dateIn, $rule, $cacheCalendars, $ruleSetCombinations) {        
        //map from ruleCombinations to cacheCombinations
        $ruleToCacheCombinations=array();        
        foreach($rule->getCombinations() as $a) {
            $r=array();
            foreach($ruleSetCombinations as $b) {
                if (RuleSet::combinationBelongsTo($a, $b)) $r[]=$b;
            }
            $ruleToCacheCombinations[$a->getTxtIds()]=$r;
        }
        
        $dayOfWeek=$dateIn->format('w');
        foreach($cacheCalendars as $cacheCalendar) {
            $date=$cacheCalendar->getDate();
            if ($rule->containsDate($date,$dayOfWeek)) {
                $this->processRuleCalendar($rule, $cacheCalendar,$ruleToCacheCombinations);
            }
            $dayOfWeek=($dayOfWeek+1)%7;
        }
    }
    
    public function processRuleCalendar($rule, $cacheCalendar, $ruleToCacheCombinations) {            
        if ($cacheCalendar->getRuleSet()===null) $cacheCalendar->setRuleSet($rule->getRuleSet()); 
        if ($rule->getReleaseDays()!==null) $cacheCalendar->setReleaseDays($rule->getReleaseDays());

        foreach($rule->getOptions() as $ruleOption) {
            $cacheDispo=$cacheCalendar->findCacheDispo($ruleOption->getOption()->getId());
            if (!$cacheDispo) {
                $cacheDispo=new CacheDispo();
                $cacheDispo->setCacheCalendar($cacheCalendar);
                $cacheDispo->setOption($ruleOption->getOption());
                $cacheCalendar->addCacheDispo($cacheDispo);
            }
            if ($ruleOption->getDispo()!==null) $cacheDispo->setDispo($ruleOption->getDispo());
        }

        foreach($rule->getCombinations() as $ruleCombination) {
            $combinations=$ruleToCacheCombinations[$ruleCombination->getTxtIds()];
            if (!$combinations) throw new \Exception("ERROR: combinació no trobada");
            foreach($combinations as $combination) {
                $cacheRate=$cacheCalendar->findCacheRate($combination->getId());
                if (!$cacheRate) {
                    $cacheRate=new CacheRate();
                    $cacheRate->setCacheCalendar($cacheCalendar);
                    $cacheRate->setCombination($combination);
                    $cacheRate->setFormula(null);
                    $cacheCalendar->addCacheRate($cacheRate);
                }
                //convert using operations +10, -10, +10%,... based on cacheCalendar rate       
                $value=$ruleCombination->getRate(); //$value is a string  
                if ($value!==null && trim($value)!='') {
                    $value=str_replace(",",".",$value);
                    $sign=substr($value,0,1);
                    if ($sign=='+' || $sign=='-') {
                        $baseValue=$cacheRate->getRate();                       

                        $tpc=substr($value,strlen($value)-1);
                        if ($tpc=='%') {
                            $cacheRate->setFormula('('.$cacheRate->getFormula().')*(100'.substr($value,0,strlen($value)-1).')/100');
                            $value=$baseValue*(100+(float)$value)/100;
                        } else {
                            $cacheRate->setFormula('('.$cacheRate->getFormula().')'.$value);
                            $value=$baseValue+(float)$value;
                        }
                    } else {
                        //constant value
                        $cacheRate->setFormula('R'.$rule->getId().'['.$ruleCombination->getTxtIds().']');
                        $value= (float)$value;    
                    }
                    $cacheRate->setRate($value);
                }
            }
        }
    }
            
        
    
    //removes all cache calendars from a given rule: used when converting a promo rule to base, or base to promo.
    public function clearRuleSet($ruleSet) {
        $this->bd->exec("DELETE FROM cache_calendar where rule_set_id={$ruleSet->getId()}"); //cascade delete on cachedispo and cacherate!!!
        $this->bd->exec("DELETE FROM cache_combination where rule_set_id={$ruleSet->getId()}");
    }
    
    
    
    //save $cacheCalendar to DB
    public function syncCacheCalendars($originalCalendars, $cacheCalendars) {
        $calendarInsertStmt=$this->bd->prepare('INSERT INTO cache_calendar (rule_set_id,date,releaseDays) VALUES (?,?,?)');
        $dispoInsertStmt=$this->bd->prepare('INSERT INTO cache_dispo (cache_calendar_id, option_id, dispo, reservations) VALUES (?,?,?,?)');
        $rateInsertStmt=$this->bd->prepare('INSERT INTO cache_rate (cache_calendar_id, cache_combination_id, rate, formula) VALUES (?,?,?,?)');
        $calendarUpdateStmt=$this->bd->prepare('UPDATE cache_calendar SET releaseDays=? WHERE id=?');
        $dispoUpdateStmt=$this->bd->prepare('UPDATE cache_dispo SET dispo=?,reservations=? WHERE id=?');
        $rateUpdateStmt=$this->bd->prepare('UPDATE cache_rate SET rate=?, formula=? WHERE id=?');
        $calendarDeleteStmt=$this->bd->prepare('DELETE FROM cache_calendar WHERE id=?');
        $dispoDeleteStmt=$this->bd->prepare('DELETE FROM cache_dispo WHERE id=?');
        $rateDeleteStmt=$this->bd->prepare('DELETE FROM cache_rate WHERE id=?');

        
        for($i=0;$i<count($cacheCalendars); $i++) {
            $cacheCalendar=$cacheCalendars[$i];
            $originalCalendar=$originalCalendars[$i];
            if (!$cacheCalendar->getId()) {
                if (!$cacheCalendar->isEmpty()) {
                    $calendarInsertStmt->bindValue(1,$cacheCalendar->getRuleSet()->getId());
                    $calendarInsertStmt->bindValue(2,$cacheCalendar->getDateYMD());
                    $calendarInsertStmt->bindValue(3,$cacheCalendar->getReleaseDays());
                    $calendarInsertStmt->execute();
                    $cacheCalendar->setId($this->bd->lastInsertId());
                }
            } else {
                if ($cacheCalendar->isEmpty()) {
                    $calendarDeleteStmt->bindValue(1,$cacheCalendar->getId());
                    $calendarDeleteStmt->execute();
                } else if ($cacheCalendar->getReleaseDays()!=$originalCalendar->getReleaseDays()) {
                    $calendarUpdateStmt->bindValue(1, $cacheCalendar->getReleaseDays());
                    $calendarUpdateStmt->bindValue(2,$cacheCalendar->getId());
                    $calendarUpdateStmt->execute();     
                }
            }
        }
        $this->bd->beginTransaction();   //speeds up execution *5     
        for($i=0;$i<count($cacheCalendars); $i++) {
            $cacheCalendar=$cacheCalendars[$i];
            $originalCalendar=$originalCalendars[$i];
            
            if (!$cacheCalendar->isEmpty()) {            
                $cacheDispos=$cacheCalendar->getCacheDispos();
                $originalDispos=$originalCalendar->getCacheDispos();            
                for($j=0;$j<count($cacheDispos); $j++) {
                    $cacheDispo=$cacheDispos[$j];
                    $originalDispo=$j<count($originalDispos)?$originalDispos[$j]:null;
                    if ($cacheDispo->getId()) { //existing cache dispo
                        if ($cacheDispo->isEmpty()) {
                                $dispoDeleteStmt->bindValue(1,$cacheDispo->getId());
                                $dispoDeleteStmt->execute();
                        } else if ($cacheDispo->getDispo()!=$originalDispo->getDispo() || 
                                $cacheDispo->getReservations()!=$originalDispo->getReservations()) {
                                $dispoUpdateStmt->bindValue(1,$cacheDispo->getDispo());
                                $dispoUpdateStmt->bindValue(2,$cacheDispo->getReservations());
                                $dispoUpdateStmt->bindValue(3,$cacheDispo->getId());
                                $dispoUpdateStmt->execute();
                        } 
                    } else { //new cache dispo
                        if (!$cacheDispo->isEmpty()) {
                            $dispoInsertStmt->bindValue(1,$cacheCalendar->getId());
                            $dispoInsertStmt->bindValue(2,$cacheDispo->getOption()->getId());
                            $dispoInsertStmt->bindValue(3,$cacheDispo->getDispo());
                            $dispoInsertStmt->bindValue(4,$cacheDispo->getReservations());
                            $dispoInsertStmt->execute();                    
                        }
                    }
                }
            
                $cacheRates=$cacheCalendar->getCacheRates();
                $originalRates=$originalCalendar->getCacheRates();            
                for($j=0;$j<count($cacheRates);$j++) {
                    $cacheRate=$cacheRates[$j];
                    $originalRate=$j<count($originalRates)?$originalRates[$j]:null;                
                    if ($cacheRate->getId()) { //existing cache rate
                        if ($cacheRate->isEmpty()) {
                            $rateDeleteStmt->bindValue(1,$cacheRate->getId());
                            $rateDeleteStmt->execute();
                        } else if ($cacheRate->getRate()!=$originalRate->getRate() || $cacheRate->getFormula()!=$originalRate->getFormula()) {
                            $rateUpdateStmt->bindValue(1,$cacheRate->getRate());
                            $rateUpdateStmt->bindValue(2,$cacheRate->getFormula());
                            $rateUpdateStmt->bindValue(3,$cacheRate->getId());
                            $rateUpdateStmt->execute();
                        }
                    } else {
                         if ($cacheRate->getRate()!==null || $cacheRate->getFormula()!==null) {
                            $rateInsertStmt->bindValue(1,$cacheCalendar->getId());
                            $rateInsertStmt->bindValue(2,$cacheRate->getCombination()->getId());
                            $rateInsertStmt->bindValue(3,$cacheRate->getRate());
                            $rateInsertStmt->bindValue(4,$cacheRate->getFormula());
                            $rateInsertStmt->execute();                    
                         }
                    }
                }        
            }
        }
        $this->bd->commit();
    }
    
    
    
    
    
    
    
    
    
    //===========================================================================
    //             RESERVATIONS
    //===========================================================================
    
    
    public static function addReservation($em, $reservation) {
        $baseSet=$em->getRepository('AppBundle:RuleSet')->find(1);
        self::registerReservationPromo($em,$reservation,$baseSet,1);
        //foreach($reservation->getPromos() as $promo) 
        //    self::registerReservationPromo($em,$reservation,$promo,1);
    }
    
    public static function removeReservation($em, $reservation) {
        $baseSet=$em->getRepository('AppBundle:RuleSet')->find(1);
        self::registerReservationPromo($em,$reservation,$baseSet,-1);
        //foreach($reservation->getPromos() as $promo) 
        //    self::registerReservationPromo($em,$reservation,$promo,-1);
    }
    
    
    static function registerReservationPromo($em, $reservation, $ruleSet, $N) {
        list($rateOptions, $dispoOptions)=$reservation->getClassifiedOptions();
        $dateIn=$reservation->getDateIn();
        $dateEnd=$reservation->getDateEnd();
        $dateEnd->sub(new \DateInterval("P1D"));
        if ($ruleSet->getId()!==1) { //ajustem les dates a les dates de la promo, si és necessari
            list($dIn,$dEnd)=$ruleSet->getDatesInterval();
            if ($dateIn<$dIn) $dateIn=$dIn;
            if ($dateEnd>$dEnd) $dateEnd=$dEnd;
        }
        $cacheCalendars=$em->getRepository('AppBundle:CacheCalendar')
                ->findByDates($dateIn, $dateEnd, $ruleSet);
        foreach($cacheCalendars as $cacheCalendar) {
            if ($cacheCalendar->getId()) { //if the promo has defined this day...update reservations
                foreach($dispoOptions as $option) {
                    $cacheDispo=$cacheCalendar->findCacheDispo($option->getId());
                    if (!$cacheDispo) {
                        $cacheDispo=new CacheDispo();
                        $cacheDispo->setCacheCalendar($cacheCalendar);
                        $cacheDispo->setOption($option);
                        $cacheCalendar->addCacheDispo($cacheDispo);
                    }
                    $reservations=$cacheDispo->getReservations();
                    $reservations+=$N;
                    if ($reservations>0) $cacheDispo->setReservations($reservations);
                    else $cacheDispo->setReservations(null); //si les reserves cauen a 0, posem null
                    $em->persist($cacheDispo);
                }   
            }
        }        
    }
    
    
}

