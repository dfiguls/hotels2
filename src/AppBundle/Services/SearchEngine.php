<?php
namespace AppBundle\Services;

use Symfony\Component\Serializer\Annotation\Groups;

class SearchEngine {
    protected $em, $bd;
    
    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em=$em;
        $this->bd=$em->getConnection();
    }    
    
    
    public function findRoomTypes($dateIn, $_dateEnd, $locale) {
        $result=[];
        $dateEnd=clone($_dateEnd);
        $dateEnd->sub(new \DateInterval("P1D"));
        $ruleSet=$this->em->getRepository('AppBundle:RuleSet')->find(1); //Base rule set
        $roomTypes=$this->em->getRepository('AppBundle:Parameter')->findOptions(1); //roomTypes
        foreach($roomTypes as $roomType) {
            $cacheCombinations=[];
            foreach($ruleSet->getCombinations() as $cacheCombination) {
                if ($cacheCombination->hasOption($roomType)) $cacheCombinations[]=$cacheCombination;
            }
            
            $days=$this->em->getRepository('AppBundle:CacheCalendar')->findDispoAndRates($ruleSet, $dateIn, $dateEnd, $cacheCombinations, array($roomType));
            $minRate=-1;
            for($i=0;$i<count($cacheCombinations);$i++) {
                $rate=0;
                foreach($days as $day) $rate+=$day['rates'][$i];
                if ($rate<$minRate || $minRate===-1) $minRate=$rate;
            }
            
            $dispo=-1;
            foreach($days as $day) if ($dispo===-1 || $day['dispo']<$dispo) $dispo=$day['dispo'];

            $result[]=array('id'=>$roomType->getId(), 'name'=>$roomType->getNameTranslation($locale), 
                'description'=>$roomType->getDescriptionTranslation($locale), 'image'=>$roomType->getMainImage(),
                'pax'=>$roomType->getPax(), 'dispo'=>$dispo, 'minRate'=>$minRate);
        }
        return $result;
    }

    public function findAccommodations($dateIn, $_dateEnd, $roomType, $locale) {
        $result=[];
        $dateEnd=clone($_dateEnd);
        $dateEnd->sub(new \DateInterval("P1D"));
        $ruleSet=$this->em->getRepository('AppBundle:RuleSet')->find(1); //Base rule set
        $accommodations=$this->em->getRepository('AppBundle:Parameter')->findOptions(2); //accommodations
        foreach($accommodations as $accommodation) {
            $cacheCombinations=[];
            foreach($ruleSet->getCombinations() as $cacheCombination) {
                if ($cacheCombination->hasOptions(array($roomType,$accommodation))) $cacheCombinations[]=$cacheCombination;
            }
            
            $days=$this->em->getRepository('AppBundle:CacheCalendar')->findDispoAndRates($ruleSet, $dateIn, $dateEnd, $cacheCombinations, array($roomType));
            $minRate=-1;
            for($i=0;$i<count($cacheCombinations);$i++) {
                $rate=0;
                foreach($days as $day) $rate+=$day['rates'][$i];
                if ($rate<$minRate || $minRate===-1) $minRate=$rate;
            }

            $result[]=array('id'=>$accommodation->getId(), 'name'=>$accommodation->getNameTranslation($locale), 'minRate'=>$minRate);
        }
        return $result;
    }
    
    public function findPaymentTypes($dateIn, $_dateEnd, $roomType, $accommodation, $locale) {
        $result=[];
        $dateEnd=clone($_dateEnd);
        $dateEnd->sub(new \DateInterval("P1D"));        
        $ruleSet=$this->em->getRepository('AppBundle:RuleSet')->find(1); //Base rule set
        $paymentTypes=$this->em->getRepository('AppBundle:Parameter')->findOptions(3); //payment methods
        foreach($paymentTypes as $paymentType) {
            $cacheCombinations=[];
            foreach($ruleSet->getCombinations() as $cacheCombination) {
                if ($cacheCombination->hasOptions(array($roomType,$accommodation,$paymentType))) $cacheCombinations[]=$cacheCombination;
            }
            
            $days=$this->em->getRepository('AppBundle:CacheCalendar')->findDispoAndRates($ruleSet, $dateIn, $dateEnd, $cacheCombinations, array($roomType));
            $minRate=-1;
            for($i=0;$i<count($cacheCombinations);$i++) {
                $rate=0;
                foreach($days as $day) $rate+=$day['rates'][$i];
                if ($rate<$minRate || $minRate===-1) $minRate=$rate;
            }

            $result[]=array('id'=>$paymentType->getId(), 'name'=>$paymentType->getNameTranslation($locale), 'minRate'=>$minRate);
        }
        return $result;
    }
    
          
    
    public function initDaysWithBase(&$aDays, $dateIn, $dateEnd, $rateOptions, $dispoOptions) {
        $hasDispo=true;
        $baseSet=$this->em->getRepository('AppBundle:RuleSet')->find(1); 

        $cacheCombinations=[];
        foreach($baseSet->getCombinations() as $cacheCombination) {
            if ($cacheCombination->hasOptions($rateOptions)) $cacheCombinations[]=$cacheCombination;
        }
        
        $days=$this->em->getRepository('AppBundle:CacheCalendar')
                ->findDispoAndRates($baseSet, $dateIn, $dateEnd, $cacheCombinations, $dispoOptions);
        $dummyToday=new \DateTime();
        $strToday=$dummyToday->format('Y-m-d');
        $today=\DateTime::createFromFormat('!Y-m-d', $strToday);
        foreach($days as &$day) {
            $d=new Day($day['day']->format('d-m'));
            $aDays[]=$d;
            $releaseDay=clone $today;
            if ($day['releaseDays']) $releaseDay->add(new \DateInterval('P'.$day['releaseDays'].'D'));
            if ($day['day']>=$releaseDay && $day['dispo']>0) {
                $d->addOption(new DayOption($day['rates'],$baseSet));
            } else {
                $hasDispo=false;
            }
        }
        unset($day);
        return $hasDispo;        
    }    
       
    
    //TODO: si no hi ha dispo, mirar a moorageTypes superiors
    public function find($dateIn, $dateEnd, $rateOptions, $dispoOptions) {
        $days=array();
        $d=clone($dateEnd);
        $d->sub(new \DateInterval("P1D"));
        $this->initDaysWithBase($days, $dateIn, $d, $rateOptions, $dispoOptions);                    
        
        return array('days'=>$days);
    }
    
    
    
    
    //if now there is dispo and the rate is the same --> the reservation is still valid
    public function checkReservation($reservation) {
        list($rateOptions, $dispoOptions)=$reservation->getClassifiedOptions();
        $result=$this->find($reservation->getDateIn(), $reservation->getDateEnd(), $rateOptions, $dispoOptions);
        $rate=0;
        foreach($result['days'] as $day) 
            if (count($day->options)>0 && count($day->options[0]->rates)>0) {
                $rate+=$day->options[0]->rates[0];
            } else throw new \Exception('Ara ja no hi ha dispo'); //now dispo is over
            
        //add services
        //foreach($reservation->getReservationServices() as $reservationService) {
        //    $service=$reservationService->getService();
        //    if ($service) $rate+=$service->getRate()*$reservationService->getN();
        //}
            
        if (round($reservation->getRate(),2)!=round($rate,2)) throw new \Exception('Les condicions econòmiques han canviat');
    }    
}


/**
 * @Groups({"select"})
 */
class Day {
    /**
     * @Groups({"select"})
     */
    public $date;
    /**
     * @Groups({"select"})
     */
    public $options;
    /**
     * @Groups({"select"})
     */
    public $index;
    
    public function __construct($date) {
        $this->date=$date;
        $this->options=array();
        $this->index=0;
    }
    
    public function addOption($option) {
        $i=0;
        while ($i<count($this->options) && $this->options[$i]->rate<$option->rate)
            $i++;
        array_splice( $this->options, $i, 0, array($option));
    }
    
    public function current() {
        return $this->options[$this->index];
    }
    
    public function next() {
        $this->index++;
    }
    
    public function end() {
        return $this->index>=count($this->options);
    }
}

class DayOption {
    /**
     * @Groups({"select"})
     */
    public $rates;

    public $ruleSet;

    public function __construct($rates, $ruleSet) {
        $this->rates=$rates;
        $this->ruleSet=$ruleSet;
    }
    
    /**
     * @Groups({"select"})
     */
    public function getRuleSetId() { return $this->ruleSet?$this->ruleSet->getId():-1; }
    

}

