<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity(fields="username",message="login repetit")
 */
class User implements UserInterface , \Serializable
{
    const ROLE_ADMIN=1;
    const ROLE_USER=0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"grid","form","reservationForm","select"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=32)
     * @Groups({"form"})
     * @Assert\NotBlank(message="no es pot deixar en blanc")
     */
    private $username;
    
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;
    private $repeatPassword;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", options={"default" = true})
     */
    private $isActive;    

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"grid","form","activity"})
     * @Assert\NotBlank(message="no es pot deixar en blanc")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     * @Groups({"grid","form","activity"})
     * @Assert\NotBlank(message="no es pot deixar en blanc")
     */
    private $surname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     * @Groups({"form"})
     */
    private $tel;
    
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Groups({"form"})
     */
    private $address;
    
    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=10, nullable=true)
     * @Groups({"form"})
     */
    private $postcode;  
    
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     * @Groups({"form"})
     */
    private $city;  
    
    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;   
    
    /**
     * @var string
     *
     * @ORM\Column(name="NIF", type="string", length=30, nullable=true)
     * @Groups({"form"})
     */
    private $NIF;    
       
    /**
     * @var integer
     *
     * @ORM\Column(name="role", type="integer")
     * @Groups({"form"})
     */
    private $role;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registerDate", type="datetime")
     */
    private $registerDate;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;    

    /**
     * @var string
     *
     * @ORM\Column(name="mailConfirmationCode", type="string", length=30, nullable=true)
     */
    private $mailConfirmationCode;    

    public function __construct() {
        $this->name=$this->surname=$this->NIF=$this->username=$this->password="";
        $this->isActive=false;
        $this->registerDate=new \DateTime();
        $this->deleted=false;
        $this->role=0; //default to ROLE_USER        
    }
    
    
    
   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set nIF
     *
     * @param string $nIF
     *
     * @return User
     */
    public function setNIF($nIF)
    {
        $this->NIF = $nIF;

        return $this;
    }

    /**
     * Get nIF
     *
     * @return string
     */
    public function getNIF()
    {
        return $this->NIF;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return User
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
    


    /**
     * Set role
     *
     * @param integer $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set mailConfirmationCode
     *
     * @param string $mailConfirmationCode
     *
     * @return User
     */
    public function setMailConfirmationCode($mailConfirmationCode)
    {
        $this->mailConfirmationCode = $mailConfirmationCode;

        return $this;
    }

    /**
     * Get mailConfirmationCode
     *
     * @return string
     */
    public function getMailConfirmationCode()
    {
        return $this->mailConfirmationCode;
    }    
    
    //==========================================================================
    public function getUsername() {
        return $this->username;
    }
    
    public function getSalt() {
        return null; //$this->salt;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getRoles() {
        return array($this->getRole()===self::ROLE_ADMIN?'ROLE_ADMIN':'ROLE_USER');
    }

    public function eraseCredentials() {        
    }
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    
    public function setRepeatPassword($password)
    {
        $this->repeatPassword = $password;
        return $this;
    }
    
    /**
     * @Assert\Callback
     */        
    public function isPasswordValid(ExecutionContext $context) {
        if ( $this->password!=$this->repeatPassword) 
            $context->addViolationAt('repeatPassword', 'Els passwords no coincideixen', array(), null);     
        
    }
       
    /**
     * @Groups({"select"})
     */
    public function getFullName() {
        return $this->name.' '.$this->surname;
    }
   
    /**
     * @Groups({"form"})
     */
    public function getCountryId()
    {
        return $this->country?$this->country->getId():null;
    }
    
    /**
     */
    public function getRegisterDateYMD()
    {
        return $this->registerDate->format('Y-m-d');
    }

    /**
     * Set registerDate from ymd string
     * @param string $ymd
     * @return User
     */
    public function setRegisterDateYMD($ymd)
    {
        $this->registerDate= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }

    /**
     * @Groups({"grid","form"})
     */
    public function getRegisterDateDMY()
    {
        return $this->registerDate->format('d-m-Y');
    }
        
    
}
