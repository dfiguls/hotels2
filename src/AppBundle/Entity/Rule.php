<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rule
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RuleRepository")
 */
class Rule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"rules","reservationForm","select"})
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer")
     * @Groups({"rules"})
     */
    private $ordering;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="No es pot deixar en blanc")
     * @Groups({"rules","select","reservationForm"})
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Groups({"rules"})
     */
    private $enabled;
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="sellDateIn", type="date", nullable=true)
     */
    private $sellDateIn;
    
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="sellDateEnd", type="date", nullable=true)
     */
    private $sellDateEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="minDays", type="integer", nullable=true)
     * @Assert\Regex(pattern="/\d+/",message="Ha de ser un número")
     * @Groups({"rules"})
     */
    private $minDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="maxDays", type="integer", nullable=true)
     * @Assert\Regex(pattern="/\d+/",message="Ha de ser un número")
     * @Groups({"rules"})
     */
    private $maxDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="releaseDays", type="integer", nullable=true)
     * @Assert\Regex(pattern="/\d+/",message="Ha de ser un número")
     * @Groups({"rules"})
     */
    private $releaseDays;

    
    /**
     * @ORM\OneToMany(targetEntity="Calendar", mappedBy="rule", cascade={"persist","remove"})
     * @Groups({"rules"})
     */
    protected $calendars;
    
    /**
     * @ORM\OneToMany(targetEntity="RuleCombination", mappedBy="rule", cascade={"persist","remove"})
     * @Groups({"rules"})
     */
    protected $combinations;
    
    /**
     * @ORM\OneToMany(targetEntity="RuleOption", mappedBy="rule", cascade={"persist","remove"})
     * @Groups({"rules"})
     */
    protected $options;       
    
    /**
     * @ORM\ManyToOne(targetEntity="RuleSet", inversedBy="rules")
     * @ORM\JoinColumn(name="rule_set_id", referencedColumnName="id")
     */
    private $ruleSet;       
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


   

    /**
     * @Groups({"rules"})
     */
    public function getSellDateInYMD()
    {
        return $this->sellDateIn->format('Y-m-d');
    }

   
    
    
    /**
     * Set minDays
     *
     * @param integer $minDays
     *
     * @return Rule
     */
    public function setMinDays($minDays)
    {
        $this->minDays = $minDays;

        return $this;
    }

    /**
     * Get minDays
     *
     * @return integer
     */
    public function getMinDays()
    {
        return $this->minDays;
    }

    /**
     * Set maxDays
     *
     * @param integer $maxDays
     *
     * @return Rule
     */
    public function setMaxDays($maxDays)
    {
        $this->maxDays = $maxDays;

        return $this;
    }

    /**
     * Get maxDays
     *
     * @return integer
     */
    public function getMaxDays()
    {
        return $this->maxDays;
    }

    /**
     * Set releaseDays
     *
     * @param integer $releaseDays
     *
     * @return Rule
     */
    public function setReleaseDays($releaseDays)
    {
        $this->releaseDays = $releaseDays;

        return $this;
    }

    /**
     * Get releaseDays
     *
     * @return integer
     */
    public function getReleaseDays()
    {
        return $this->releaseDays;
    }

 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->calendars = new \Doctrine\Common\Collections\ArrayCollection();
        $this->combinations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enabled=true;
        $this->sellDateIn=$this->sellDateEnd=$this->minDays=$this->maxDays=$this->releaseDays=null;
    }

    /**
     * Add calendars
     *
     * @param \AppBundle\Entity\Calendar $calendars
     * @return Rule
     */
    public function addCalendar(\AppBundle\Entity\Calendar $calendars)
    {
        $this->calendars[] = $calendars;

        return $this;
    }

    /**
     * Remove calendars
     *
     * @param \AppBundle\Entity\Calendar $calendars
     */
    public function removeCalendar(\AppBundle\Entity\Calendar $calendars)
    {
        $this->calendars->removeElement($calendars);
    }

    /**
     * Get calendars
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCalendars()
    {
        return $this->calendars;
    }


    /**
     * Set order
     *
     * @param integer $ordering
     * @return Rule
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer 
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set sellDateIn
     *
     * @param \DateTime $sellDateIn
     * @return Rule
     */
    public function setSellDateIn($sellDateIn)
    {
        $this->sellDateIn = $sellDateIn;

        return $this;
    }

    /**
     * Get sellDateIn
     *
     * @return \DateTime 
     */
    public function getSellDateIn()
    {
        return $this->sellDateIn;
    }

    /**
     * Set sellDateEnd
     *
     * @param \DateTime $sellDateEnd
     * @return Rule
     */
    public function setSellDateEnd($sellDateEnd)
    {
        $this->sellDateEnd = $sellDateEnd;

        return $this;
    }

    /**
     * Get sellDateEnd
     *
     * @return \DateTime 
     */
    public function getSellDateEnd()
    {
        return $this->sellDateEnd;
    }
    
    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Rule
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add combination
     *
     * @param \AppBundle\Entity\RuleCombination $combination
     *
     * @return Rule
     */
    public function addCombination(\AppBundle\Entity\RuleCombination $combination)
    {
        $this->combinations[] = $combination;

        return $this;
    }

    /**
     * Remove combination
     *
     * @param \AppBundle\Entity\RuleCombination $combination
     */
    public function removeCombination(\AppBundle\Entity\RuleCombination $combination)
    {
        $this->combinations->removeElement($combination);
    }

    /**
     * Get combinations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCombinations()
    {
        return $this->combinations;
    }

    
    /**
     * Add option
     *
     * @param \AppBundle\Entity\RuleOption $option
     *
     * @return Rule
     */
    public function addOption(\AppBundle\Entity\RuleOption $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \AppBundle\Entity\RuleOption $option
     */
    public function removeOption(\AppBundle\Entity\RuleOption $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set ruleSet
     *
     * @param \AppBundle\Entity\RuleSet $ruleSet
     *
     * @return Rule
     */
    public function setRuleSet(\AppBundle\Entity\RuleSet $ruleSet = null)
    {
        $this->ruleSet = $ruleSet;

        return $this;
    }

    /**
     * Get ruleSet
     *
     * @return \AppBundle\Entity\RuleSet
     */
    public function getRuleSet()
    {
        return $this->ruleSet;
    }
    

     //--------------------------------------------------------------------------
    
    public function getDatesInterval() {
        $dateIn=$dateEnd=null;
         foreach($this->calendars as $calendar) {
            if (!$dateIn || $calendar->getDateIn()<$dateIn) $dateIn=$calendar->getDateIn();
            if (!$dateEnd || $calendar->getDateEnd()>$dateEnd) $dateEnd=$calendar->getDateEnd();     
        }
        return array($dateIn, $dateEnd);
    }
    
     public function getDateIn() {
        $dateIn=null;
         foreach($this->calendars as $calendar) {
            if (!$dateIn || $calendar->getDateIn()<$dateIn) $dateIn=$calendar->getDateIn();
        }
        return $dateIn;
    }
    
     public function getDateEnd() {
        $dateEnd=null;
         foreach($this->calendars as $calendar) {
            if (!$dateEnd || $calendar->getDateEnd()>$dateEnd) $dateEnd=$calendar->getDateEnd();     
        }
        return $dateEnd;
    }
    
    public function containsDate($date, $dayOfWeek) {
        if (!$this->enabled) return false;
        $result=false;
        foreach($this->calendars as $calendar) {
            if ($calendar->containsDate($date,$dayOfWeek)) {
                if ($calendar->getIsBlackDay()) return false;
                else $result=true; //wait if another calendar is blackDay
            }
        }
        return $result;
    }
    
    
    
     /**
     * Set sellDate from ymd string
     * @param string $ymd
     * @return User
     */
    public function setSellDateInYMD($ymd)
    {
        $this->sellDateIn= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }
    
     /**
     * @Groups({"rules"})
     */
    public function getSellDateEndYMD()
    {
        return $this->sellDateEnd->format('Y-m-d');
    }

    /**
     * Set sellDate from ymd string
     * @param string $ymd
     * @return User
     */
    public function setSellDateEndYMD($ymd)
    {
        $this->sellDateEnd= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }
    
    public function meetsPreConditions($dateIn, $dateEnd) {
        $today=new \DateTime();
        $msg=array();
        if ($this->sellDateIn && $today<$this->sellDateIn || 
            $this->sellDateEnd && $this->sellDateEnd<$today) $msg[]='Fora de període de compra';
        $nDays=$dateEnd->diff($dateIn)->days+1; 
        if ($this->minDays && $nDays<$this->minDays) $msg[]='Reserva massa curta, mín '.$this->minDays.' dies';
        if ($this->maxDays && $this->maxDays<$nDays) $msg[]='Reserva massa llarga, màx '.$this->maxDays.' dies';
        if (count($msg)>0) return $msg;
        return true;
    }
    
    //TODO checks if a rule applied on a given day $index meets all post conditions (minConsecutiveDays, notCompatibleWith...)
    public function meetsPostConditions($days, $index) {
        return true;
    }
    


    public function hasDependenceToBase() {
        $hasDependence=false;
        if ($this->isPromo) {
            foreach($this->rates as $rate) {
                if ($rate->isIncremental()) {
                    $hasDependence=true;
                }
            }
        }
        return $hasDependence;
    }

    /**
     * @Groups({"rules"})
     */
    public function getRuleSetId()
    {
        return $this->ruleSet?$this->ruleSet->getId():-1;
    }


}
