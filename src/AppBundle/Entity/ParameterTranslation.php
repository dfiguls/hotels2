<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ParameterTranslation
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ParameterTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"parameterForm"})
     */
    private $id;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Groups({"parameterForm"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Parameter", inversedBy="translations")
     * @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     */
    private $parameter;

    
    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ParameterTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set parameter
     *
     * @param \AppBundle\Entity\Parameter $parameter
     *
     * @return ParameterTranslation
     */
    public function setParameter(\AppBundle\Entity\Parameter $parameter = null)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return \AppBundle\Entity\Parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return ParameterTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
        
    
    //=================================================================================================
    
    /**
     * @Groups({"parameterForm"})
     */
    public function getLanguageId()
    {
        return $this->language?$this->language->getId():null;
    }
}
