<?php

namespace AppBundle\Entity;

class ParameterRepository extends DataTablesRepository
{
    protected function getAlias() { return 'p'; }
    private $orderFields=array(null,'p.id','p.name');
    protected function getOrderField($orderBy) { return $this->orderFields[$orderBy]; }
    protected function getSearchFields() { return array('p.name'); }  
    
    public function selectAll() {
        return $this->createQueryBuilder('qb')
            ->select('p')
            ->from('AppBundle:Parameter', 'p')
            ->getQuery()->getResult();        
    }        
    
    public function findOptions($parameterId) {
        return $this->createQueryBuilder('qb')
            ->select('o')
            ->from('AppBundle:Option', 'o')
            ->join('o.parameter','p')
            ->andWhere('p.id='.$parameterId)
            ->orderBy('o.name')
            ->getQuery()->getResult();                
    }
}
