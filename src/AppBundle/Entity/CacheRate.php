<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CacheRate
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CacheRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"select"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CacheCalendar", inversedBy="cacheRates")
     * @ORM\JoinColumn(name="cache_calendar_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $cacheCalendar;  
    
    /**
     * @ORM\ManyToOne(targetEntity="CacheCombination")
     * @ORM\JoinColumn(name="cache_combination_id", referencedColumnName="id")
     */
    private $combination;
      
    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float", nullable=true)
     * @Groups({"select"})
     */
    private $rate;    

    /**
     * @var float
     *
     * @ORM\Column(name="specialRate", type="float",  nullable=true)
     * @Groups({"select"})
     */
    private $specialRate;    
    

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=50, nullable=true)
     * @Groups({"select"})
     */
    private $formula;    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct() {
        $this->rate=null;
        $this->specialRate=null;
    }
    
    
    /**
     * Set cacheCalendar
     *
     * @param \AppBundle\Entity\CacheCalendar $cacheCalendar
     * @return CacheRate
     */
    public function setCacheCalendar(\AppBundle\Entity\CacheCalendar $cacheCalendar = null)
    {
        $this->cacheCalendar = $cacheCalendar;

        return $this;
    }

    /**
     * Get cacheCalendar
     *
     * @return \AppBundle\Entity\CacheCalendar 
     */
    public function getCacheCalendar()
    {
        return $this->cacheCalendar;
    }


      

     /**
     * Set rate
     *
     * @param float $rate
     * @return CacheRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set specialRate
     *
     * @param float $specialRate
     * @return CacheRate
     */
    public function setSpecialRate($specialRate)
    {
        $this->specialRate = $specialRate;

        return $this;
    }

    /**
     * Get specialRate
     *
     * @return float 
     */
    public function getSpecialRate()
    {
        return $this->specialRate;
    }

    /**
     * Set combination
     *
     * @param \AppBundle\Entity\CacheCombination $combination
     *
     * @return CacheRate
     */
    public function setCombination(\AppBundle\Entity\CacheCombination $combination = null)
    {
        $this->combination = $combination;

        return $this;
    }

    /**
     * Get combination
     *
     * @return \AppBundle\Entity\CacheCombination
     */
    public function getCombination()
    {
        return $this->combination;
    }    
  

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return CacheRate
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }    
    
    //==========================================================================
    
    public function getFinalRate() {
        if ($this->specialRate!==null) return $this->specialRate;
        return $this->rate;
    }
        

    public function getCombinationId() {
        return $this->combination?$this->combination->getId():-1;
    }
      
    public function isEmpty() {
        return $this->rate===null && $this->specialRate===null;
    }
    
    /**
     * @Groups({"select"})
     */
    public function getCombinationIds() {
        return $this->combination?$this->combination->getTxtIds():array();
    }


}
