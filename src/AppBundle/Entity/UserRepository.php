<?php
namespace AppBundle\Entity;

class UserRepository extends DataTablesRepository
{
    protected function getAlias() { return 'u'; }
    private $orderFields=array('u.id','u.name','u.surname','u.registerDate');
    protected function getOrderField($orderBy) { return $this->orderFields[$orderBy]; }
    protected function getSearchFields() { return array('u.name','u.surname','u.login'); }
    
    function findUsername($username) {
        $user=$this->createQueryBuilder("u")
                ->andWhere("u.username=:username")
                ->setParameter('username', $username)
                ->getQuery()->getOneOrNullResult();
        return $user;
    }
}
