<?php
namespace AppBundle\Entity;

class RuleRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByDates($ruleSet, $dateInYMD, $dateEndYMD) {
        $query=$this->createQueryBuilder('r')
            ->join('r.calendars', 'c')
            ->andWhere("c.dateIn <= :dateEnd")
            ->andWhere("c.dateEnd >= :dateIn")
            ->andWhere("r.ruleSet = :ruleSet")
            ->setParameter('dateIn', $dateInYMD)
            ->setParameter('dateEnd', $dateEndYMD)
            ->setParameter('ruleSet', $ruleSet)
            ->orderBy('r.ordering')
            ;
        return $query;       
    }    

}

?>
