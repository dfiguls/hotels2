<?php
namespace AppBundle\Entity;

class CacheCalendarRepository extends \Doctrine\ORM\EntityRepository
{
    function filter($qb, $dateIn, $dateEnd, $ruleSet) {
        $qb->andWhere('c.date >= :dateIn')
            ->andWhere('c.date <= :dateEnd')
            ->setParameter('dateIn', $dateIn)
            ->setParameter('dateEnd', $dateEnd)
            ->orderBy('c.date');
        $qb->andWhere('c.ruleSet=:ruleSet')->setParameter('ruleSet',$ruleSet); //one promo rule
    }
    
    public function findByDates($dateIn, $dateEnd, $ruleSet) {        
        $qb=$this->createQueryBuilder('c');           
        $this->filter($qb,$dateIn,$dateEnd,$ruleSet);

        $aDays=$qb->getQuery()->getResult();
        $inc=new \DateInterval('P1D');
        $days=array();
        $date=clone $dateIn;
        $i=0;
        while ($date<=$dateEnd) {            
            if ($i<count($aDays) && $aDays[$i]->getDate()==$date) {
                $days[]=$aDays[$i];
                $i++;
            } else if ($i<count($aDays) && $aDays[$i]->getDate()<$date) {
                throw new \Exception("ERROR: duplicated cacheCalendar date ".$aDays[$i]->getDate()->format('Y-m-d'));
            } else {
                $c=new CacheCalendar();
                $c->setRuleSet($ruleSet);
                $c->setDate(clone $date);
                $days[]=$c;
            }
            $date=$date->add($inc);
        }
        
        return $days;
    }      
    
    
    public function findDispoAndRates($ruleSet, $dateIn, $dateEnd, $cacheCombinations, $dispoOptions) {
        $cacheCalendars=$this->findByDates($dateIn, $dateEnd, $ruleSet);
        $days=array();
        foreach($cacheCalendars as $cacheCalendar) {
            $days[]=array(
                'day'=>$cacheCalendar->getDate(), 
                'releaseDays'=>$cacheCalendar->getReleaseDays(),
                'dispo'=>null, 
                'rates'=>array()
            );
        }
        
        $this->addDispos($days, $ruleSet, $dateIn, $dateEnd, $dispoOptions);
        $this->addRates($days, $ruleSet, $dateIn, $dateEnd, $cacheCombinations);
        return $days;    
    }
   
    
   
    
    public function addDispos(&$days, $ruleSet, $dateIn, $dateEnd, $options) {
        foreach($options as $option) {
            $qb=$this->getEntityManager()->getRepository('AppBundle:CacheDispo')->createQueryBuilder('d')->select('d, c')
                ->join('d.cacheCalendar','c')
                ->andWhere('d.option=:option')
                ->setParameter('option',$option);
            $this->filter($qb,$dateIn,$dateEnd,$ruleSet);

            $aDays=$qb->getQuery()->getResult();
            for ($i=0, $j=0;$i<count($days);$i++) {
                if ($j<count($aDays) && $days[$i]['day']==$aDays[$j]->getCacheCalendar()->getDate()) {
                    $finalDispo=$aDays[$j]->getFinalDispo();
                    if ($days[$i]['dispo']===null || $days[$i]['dispo']>$finalDispo) {
                        $days[$i]['dispo']=$finalDispo;
                    }
                    $j++;
                }
            }
        }
    }
    
    public function addRates(&$days, $ruleSet, $dateIn, $dateEnd, $cacheCombinations) {        
        foreach($cacheCombinations as $combination) {
            $qb=$this->getEntityManager()->getRepository('AppBundle:CacheRate')->createQueryBuilder('d')->select('d, c')
                ->join('d.cacheCalendar','c')
                ->andWhere('d.combination=:combination')
                ->setParameter('combination',$combination);
            $this->filter($qb,$dateIn,$dateEnd,$ruleSet);

            $aDays=$qb->getQuery()->getResult();
            for ($i=0, $j=0;$i<count($days);$i++) {                
                if ($j<count($aDays) && $days[$i]['day']==$aDays[$j]->getCacheCalendar()->getDate()) {
                    $days[$i]['rates'][]=$aDays[$j]->getFinalRate();
                    $j++;
                } else {
                    $days[$i]['rates'][]=null;
                }
            }
        }
    }
    
    
}

?>
