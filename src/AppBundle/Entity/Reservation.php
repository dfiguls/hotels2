<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reservation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ReservationRepository")
 */
class Reservation
{
    const PAYMENT_TRANSF=1;
    const PAYMENT_PAYPAL=2;
    const PAYMENT_TPV=3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"reservationForm","activity"})
     */
    private $id;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIn", type="date")
     */
    private $dateIn;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date")
     */
    private $dateEnd;    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reservationDate", type="datetime")
     */
    private $reservationDate;    
    
    
     /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;  
    
    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float")
     * @Assert\NotBlank(message="No es pot deixar en blanc")
     * @Assert\GreaterThan(value = 0, message="Ha se ser un número més gran de 0")
     * @Groups({"reservationForm"})
     */
    private $rate;  
    
    /**
     * @var integer
     *
     * @ORM\Column(name="paymentMethod", type="integer")
     * @Groups({"reservationForm"})
     */
    private $paymentMethod;
    
        
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Groups({"activity"})
     */
    private $user;    
    
    /**
     * @ORM\OneToMany(targetEntity="ReservationOption", mappedBy="reservation",  cascade={"persist","remove"})
     * @Groups({"reservationForm"})
     */
    private $reservationOptions;          
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="backup", type="text", nullable=true)
     * @Groups({"reservationForm"})
     */
    private $backup;
    
    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=30, nullable=true)
     */
    private $transactionId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="transaction_status", type="string", length=30, nullable=true)
     */
    private $transactionStatus;
    
    /**
     * @var string
     *
     * @ORM\Column(name="transaction_message", type="string", length=200, nullable=true)
     */
    private $transactionMessage;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservationDate=new \DateTime();
        $this->enabled=true; 
        $this->reservationOptions=new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Reservation
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set dateIn
     *
     * @param \DateTime $dateIn
     *
     * @return Reservation
     */
    public function setDateIn($dateIn)
    {
        $this->dateIn = $dateIn;

        return $this;
    }

    /**
     * Get dateIn
     *
     * @return \DateTime
     */
    public function getDateIn()
    {
        return $this->dateIn;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Reservation
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set reservationDate
     *
     * @param \DateTime $reservationDate
     *
     * @return Reservation
     */
    public function setReservationDate($reservationDate)
    {
        $this->reservationDate = $reservationDate;

        return $this;
    }

    /**
     * Get reservationDate
     *
     * @return \DateTime
     */
    public function getReservationDate()
    {
        return $this->reservationDate;
    }

    /**
     * Set rate
     *
     * @param float $rate
     *
     * @return Reservation
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     *
     * @return Reservation
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set backup
     *
     * @param string $backup
     *
     * @return Reservation
     */
    public function setBackup($backup)
    {
        $this->backup = $backup;

        return $this;
    }

    /**
     * Get backup
     *
     * @return string
     */
    public function getBackup()
    {
        return $this->backup;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return Reservation
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set transactionStatus
     *
     * @param string $transactionStatus
     *
     * @return Reservation
     */
    public function setTransactionStatus($transactionStatus)
    {
        $this->transactionStatus = $transactionStatus;

        return $this;
    }

    /**
     * Get transactionStatus
     *
     * @return string
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * Set transactionMessage
     *
     * @param string $transactionMessage
     *
     * @return Reservation
     */
    public function setTransactionMessage($transactionMessage)
    {
        $this->transactionMessage = $transactionMessage;

        return $this;
    }

    /**
     * Get transactionMessage
     *
     * @return string
     */
    public function getTransactionMessage()
    {
        return $this->transactionMessage;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return Reservation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Reservation
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add reservationOption
     *
     * @param \AppBundle\Entity\ReservationOption $reservationOption
     *
     * @return Reservation
     */
    public function addReservationOption(\AppBundle\Entity\ReservationOption $reservationOption)
    {
        $this->reservationOptions[] = $reservationOption;

        return $this;
    }

    /**
     * Remove reservationOption
     *
     * @param \AppBundle\Entity\ReservationOption $reservationOption
     */
    public function removeReservationOption(\AppBundle\Entity\ReservationOption $reservationOption)
    {
        $this->reservationOptions->removeElement($reservationOption);
    }

    /**
     * Get reservationOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservationOptions()
    {
        return $this->reservationOptions;
    }
    
    
    //==============================================================================
     /**
     * @Groups({"activity"})
     */
    public function getNDays() {
        $diff=$this->dateIn->diff($this->dateEnd);
        return (int)$diff->format("%r%a");
    }
    
    public function getDateInDMY()
    {
        return $this->dateIn?$this->dateIn->format('d-m-Y'):'';
    }
    
     /**
     * @Groups({"reservationForm","activity"})
     */
    public function getDateInYMD()
    {
        return $this->dateIn?$this->dateIn->format('Y-m-d'):'';
    }

    /**
     * Set dateIn from ymd string
     * @param string $ymd
     * @return User
     */
    public function setDateInYMD($ymd)
    {
        $this->dateIn= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }        
    
    

    public function getDateEndDMY()
    {        
        return $this->dateEnd?$this->dateEnd->format('d-m-Y'):'';
    }
    
     /**
     * @Groups({"reservationForm","activity"})
     */
    public function getDateEndYMD()
    {
        return $this->dateEnd?$this->dateEnd->format('Y-m-d'):'';
    }

    /**
     * Set dateEnd from ymd string
     * @param string $ymd
     * @return User
     */
    public function setDateEndYMD($ymd)
    {
        $this->dateEnd= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }    
    
    
    public function getReservationDateDMY()
    {        
        return $this->reservationDate?$this->reservationDate->format('d-m-Y'):'';
    }
    
    /**
     * @Groups({"reservationForm"})
     */
    public function getUserId()
    {
        return $this->user?$this->user->getId():null;
    }
    
    public function getClassifiedOptions() {
        $dispoOptions=array();
        $rateOptions=array();
        foreach($this->reservationOptions as $reservationOption) {
            $option=$reservationOption->getOption();
            $parameter=$option->getParameter();
            if ($parameter->getHasDispo()) $dispoOptions[]=$option;
            if ($parameter->getHasRate()) $rateOptions[]=$option;
        }
        return array($rateOptions, $dispoOptions);
    }
    
    public function getRoomTypeName() {
        return $this->getOptionName(1);
    }

    public function getAccommodationName() {
        return $this->getOptionName(2);
    }
    
    public function getPaymentTypeName() {
        return $this->getOptionName(3);
    }
    
    public function getOptionName($parameterId) {
        foreach($this->reservationOptions as $reservationOption) {
            if ($reservationOption->getOption()->getParameter()->getId()===$parameterId)
                return $reservationOption->getOption()->getName();
        }
        return '--';
    }
    
    public function getAges() {
        $ages=array();
        foreach($this->reservationOptions as $reservationOption) {
            if ($reservationOption->getOption()->getParameter()->getId()===6)
                $ages[]=array('name'=>$reservationOption->getOption()->getName(),'pax'=>$reservationOption->getPax());
        }
        return $ages;
    }

}
