<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Option images
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class OptionImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"parameterForm"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Groups({"parameterForm"})
     * @Assert\NotBlank(message="no es pot deixar en blanc")
     */
    private $name;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean")
     * @Groups({"parameterForm"})
     */
    private $main;       
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Option", inversedBy="images")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;    
        



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OptionImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    

    /**
     * Set option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return OptionImage
     */
    public function setOption(\AppBundle\Entity\Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \AppBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return OptionImage
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }
}
