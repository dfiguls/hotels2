<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RuleOption
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RuleOption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"rules"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Option")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * @ORM\ManyToOne(targetEntity="Rule", inversedBy="columns")
     * @ORM\JoinColumn(name="rule_id", referencedColumnName="id")
     */
    private $rule;    
    
    /**
     * @var int
     *
     * @ORM\Column(name="dispo", type="integer", nullable=true)
     * @Groups({"rules"})
     */
    private $dispo;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="isHorizontal", type="boolean")
     * @Groups({"rules"})
     */
    private $isHorizontal;    
    
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dispo
     *
     * @param integer $dispo
     *
     * @return RuleOption
     */
    public function setDispo($dispo)
    {
        $this->dispo = $dispo;

        return $this;
    }

    /**
     * Get dispo
     *
     * @return integer
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * Set option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return RuleOption
     */
    public function setOption(\AppBundle\Entity\Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \AppBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return RuleOption
     */
    public function setRule(\AppBundle\Entity\Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return \AppBundle\Entity\Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
    
    /**
     * Set isHorizontal
     *
     * @param boolean $isHorizontal
     *
     * @return RuleOption
     */
    public function setIsHorizontal($isHorizontal)
    {
        $this->isHorizontal = $isHorizontal;

        return $this;
    }

    /**
     * Get isHorizontal
     *
     * @return boolean
     */
    public function getIsHorizontal()
    {
        return $this->isHorizontal;
    }    
    
    //------------------------------------------------------------------------------------------
    
    /**
     * @Groups({"rules"})
     */
    public function getOptionId()
    {
        return $this->option?$this->option->getId():-1;
    }
    
}
