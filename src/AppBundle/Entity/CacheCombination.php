<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CacheCombination
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CacheCombination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"select"})
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Option")
     * @ORM\JoinTable(name="cache_combination_option",
     *      joinColumns={@ORM\JoinColumn(name="combination_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id")}
     *      )
     * @Groups({"select"})
     */
    private $options;

    /**
     * @ORM\ManyToOne(targetEntity="RuleSet", inversedBy="rates")
     * @ORM\JoinColumn(name="rule_set_id", referencedColumnName="id")
     */
    private $ruleSet;    
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return CacheCombination
     */
    public function addOption(\AppBundle\Entity\Option $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \AppBundle\Entity\Option $option
     */
    public function removeOption(\AppBundle\Entity\Option $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set ruleSet
     *
     * @param \AppBundle\Entity\RuleSet $ruleSet
     *
     * @return CacheCombination
     */
    public function setRuleSet(\AppBundle\Entity\RuleSet $ruleSet = null)
    {
        $this->ruleSet = $ruleSet;

        return $this;
    }

    /**
     * Get ruleSet
     *
     * @return \AppBundle\Entity\RuleSet
     */
    public function getRuleSet()
    {
        return $this->ruleSet;
    }
    
    //-------------------------------------------------------------------------
    public function getIds() {
        $aIds=array();
        foreach($this->options as $option) { $aIds[]=$option->getId(); }
        return $aIds;
    }

    public function getTxtIds() {
        return Option::getTxtIds($this->options);
    }
    
    public function hasOption($option) {
        foreach($this->options as $op) {
            if ($option->getId()===$op->getId()) return true;
        }
        return false;
    }

    public function hasOptions($options) {
        foreach($options as $option) {
            if (!$this->hasOption($option)) return false;
        }
        return true;
    }
}
