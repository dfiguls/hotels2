<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CacheCalendar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CacheCalendarRepository")
 */
class CacheCalendar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"select"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;
    
    /**
     * @ORM\ManyToOne(targetEntity="RuleSet")
     * @ORM\JoinColumn(name="rule_set_id", referencedColumnName="id")
     */
    private $ruleSet;    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="releaseDays", type="integer", nullable=true)
     */
    private $releaseDays;
    
    /**
     * @ORM\OneToMany(targetEntity="CacheDispo", mappedBy="cacheCalendar",cascade={"persist","remove"})
     * @Groups({"select"})
     */
    protected $cacheDispos;
    
    /**
     * @ORM\OneToMany(targetEntity="CacheRate", mappedBy="cacheCalendar",cascade={"persist","remove"})
     * @Groups({"select"})
     */
    protected $cacheRates;
    

  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->releaseDays=null;
        $this->cacheDispos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cacheRates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id=$id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return CacheCalendar
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }



    /**
     * Add cacheDispos
     *
     * @param \AppBundle\Entity\CacheDispo $cacheDispos
     * @return CacheCalendar
     */
    public function addCacheDispo(\AppBundle\Entity\CacheDispo $cacheDispos)
    {
        $this->cacheDispos[] = $cacheDispos;

        return $this;
    }

    /**
     * Remove cacheDispos
     *
     * @param \AppBundle\Entity\CacheDispo $cacheDispos
     */
    public function removeCacheDispo(\AppBundle\Entity\CacheDispo $cacheDispos)
    {
        $this->cacheDispos->removeElement($cacheDispos);
    }

    /**
     * Get cacheDispos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCacheDispos()
    {
        return $this->cacheDispos;
    }

    /**
     * Add cacheRates
     *
     * @param \AppBundle\Entity\CacheRate $cacheRates
     * @return CacheCalendar
     */
    public function addCacheRate(\AppBundle\Entity\CacheRate $cacheRates)
    {
        $this->cacheRates[] = $cacheRates;

        return $this;
    }

    /**
     * Remove cacheRates
     *
     * @param \AppBundle\Entity\CacheRate $cacheRates
     */
    public function removeCacheRate(\AppBundle\Entity\CacheRate $cacheRates)
    {
        $this->cacheRates->removeElement($cacheRates);
    }

    /**
     * Get cacheRates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCacheRates()
    {
        return $this->cacheRates;
    }
    
     /**
     * Set releaseDays
     *
     * @param integer $releaseDays
     * @return CacheCalendar
     */
    public function setReleaseDays($releaseDays)
    {
        $this->releaseDays = $releaseDays;

        return $this;
    }

    /**
     * Get releaseDays
     *
     * @return integer 
     */
    public function getReleaseDays()
    {
        return $this->releaseDays;
    }
    
    /**
     * Set ruleSet
     *
     * @param \AppBundle\Entity\RuleSet $ruleSet
     *
     * @return CacheCalendar
     */
    public function setRuleSet(\AppBundle\Entity\RuleSet $ruleSet = null)
    {
        $this->ruleSet = $ruleSet;

        return $this;
    }

    /**
     * Get ruleSet
     *
     * @return \AppBundle\Entity\RuleSet
     */
    public function getRuleSet()
    {
        return $this->ruleSet;
    }    
    
    //-----------------------------------------------------------------------------------------------------------
    
    public function findCacheDispo($optionId) {
        foreach ($this->cacheDispos as $cacheDispo) {
            if ($cacheDispo->getOption()->getId()==$optionId) {
                return $cacheDispo;
            }
        }
        return null; 
    }
    
    public function findCacheRate($combinationId) {
        foreach ($this->cacheRates as $cacheRate) {
            if ($cacheRate->getCombination()->getId()===$combinationId) {
                return $cacheRate;
            }
        }
        return null; 
    }
    
    public function getRateCombinations() {
        $combinations=array();
        foreach($this->cacheRates as $cacheRate) {
            $combinations[]=$cacheRate->getCombination();
        }
        return $combinations;
    }
    
    /**
     * @Groups({"select"})
     */
    public function getDateYMD() {
        return $this->date->format('Y-m-d');
    }    
    
   public function isEmpty() {
       return $this->ruleSet===null || ($this->releaseDays===null && count($this->cacheDispos)===0 && count($this->cacheRates)===0);
   }

   //clone is used to record original state of dispos and rates and update them 
   //in optimized way (if previous value=current value ==> do nothing...)
    public function __clone() {        
        if ($this->cacheDispos) {
            $cacheDispos=$this->cacheDispos;
            $cacheRates=$this->cacheRates;

            $this->cacheDispos = new \Doctrine\Common\Collections\ArrayCollection();
            foreach($cacheDispos as $cacheDispo) $this->cacheDispos->add(clone $cacheDispo);
            $this->cacheRates = new \Doctrine\Common\Collections\ArrayCollection();
            foreach($cacheRates as $cacheRate) $this->cacheRates->add(clone $cacheRate);
        }
    }
   
}
