<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CacheDispo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CacheDispo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"select"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CacheCalendar", inversedBy="cacheDispos")
     * @ORM\JoinColumn(name="cache_calendar_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $cacheCalendar;      
    
    /**
     * @ORM\ManyToOne(targetEntity="Option")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;  
    
    /**
     * @var int
     *
     * @ORM\Column(name="dispo", type="integer", nullable=true)
     * @Groups({"select"})
     */
    private $dispo;
    
    /**
     * @var int
     *
     * @ORM\Column(name="specialDispo", type="integer", nullable=true)
     * @Groups({"select"})
     */
    private $specialDispo;
    
    /**
     * @var int
     *
     * @ORM\Column(name="reservations", type="integer", nullable=true)
     * @Groups({"select"})
     */
    private $reservations;
     
    
    
     public function __construct() {
        $this->dispo=null;
        $this->reservations=null;
        $this->specialDispo=null;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dispo
     *
     * @param integer $dispo
     * @return CacheDispo
     */
    public function setDispo($dispo)
    {
        $this->dispo = $dispo;

        return $this;
    }

    /**
     * Get dispo
     *
     * @return integer 
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * Set cacheCalendar
     *
     * @param \AppBundle\Entity\CacheCalendar $cacheCalendar
     * @return CacheDispo
     */
    public function setCacheCalendar(\AppBundle\Entity\CacheCalendar $cacheCalendar = null)
    {
        $this->cacheCalendar = $cacheCalendar;

        return $this;
    }

    /**
     * Get cacheCalendar
     *
     * @return \AppBundle\Entity\CacheCalendar 
     */
    public function getCacheCalendar()
    {
        return $this->cacheCalendar;
    }

    /**
     * Set reservations
     *
     * @param integer $reservations
     * @return CacheDispo
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;

        return $this;
    }

    /**
     * Get reservations
     *
     * @return integer 
     */
    public function getReservations()
    {
        return $this->reservations;
    }
    
    /**
     * Set option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return CacheDispo
     */
    public function setOption(\AppBundle\Entity\Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \AppBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set specialDispo
     *
     * @param integer $specialDispo
     * @return CacheDispo
     */
    public function setSpecialDispo($specialDispo)
    {
        $this->specialDispo = $specialDispo;

        return $this;
    }

    /**
     * Get specialDispo
     *
     * @return integer 
     */
    public function getSpecialDispo()
    {
        return $this->specialDispo;
    }

    
    
    //==========================================================================
    
    public function getFinalDispo() {
        if ($this->specialDispo!==null) return $this->specialDispo-$this->reservations;
        if ($this->dispo!==null) return $this->dispo-$this->reservations;
        return 0; 
    }
    
    public function isEmpty() {
        return $this->dispo===null && $this->reservations===null && $this->specialDispo===null;
    }
        
    /**
     * @Groups({"select"})
     */
    public function getOptionId() {
        return $this->option?$this->option->getId():-1;
    }
    
    /**
     * @Groups({"select"})
     */
    public function getOptionName() {
        return $this->option?$this->option->getName():'';
    }
    
}
