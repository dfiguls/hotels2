<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Calendar
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Calendar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"rules"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIn", type="date")
     */
    private $dateIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date")
     */
    private $dateEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="maskDays", type="integer")
     * @Groups({"rules"})
     */
    private $maskDays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isBlackDay", type="boolean")
     * @Groups({"rules"})
     */
    private $isBlackDay;

    /**
     * @ORM\ManyToOne(targetEntity="Rule", inversedBy="calendars")
     * @ORM\JoinColumn(name="rule_id", referencedColumnName="id")
     */
    protected $rule;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateIn
     *
     * @param \DateTime $dateIn
     *
     * @return Calendar
     */
    public function setDateIn($dateIn)
    {
        $this->dateIn = $dateIn;

        return $this;
    }

    /**
     * Get dateIn
     *
     * @return \DateTime
     */
    public function getDateIn()
    {
        return $this->dateIn;
    }
    
    /**
     * @Groups({"rules"})
     */
    public function getDateInYMD()
    {
        return $this->dateIn->format('Y-m-d');
    }

    /**
     * Set dateIn from ymd string
     * @param string $ymd
     * @return User
     */
    public function setDateInYMD($ymd)
    {
        $this->dateIn= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }
    
    

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Calendar
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @Groups({"rules"})
     */
    public function getDateEndYMD()
    {
        return $this->dateEnd->format('Y-m-d');
    }

    /**
     * Set dateEnd from ymd string
     * @param string $ymd
     * @return User
     */
    public function setDateEndYMD($ymd)
    {
        $this->dateEnd= \DateTime::createFromFormat('!Y-m-d', $ymd);
        return $this;
    }    
    
    /**
     * Set maskDays
     *
     * @param integer $maskDays
     *
     * @return Calendar
     */
    public function setMaskDays($maskDays)
    {
        $this->maskDays = $maskDays;

        return $this;
    }

    /**
     * Get maskDays
     *
     * @return integer
     */
    public function getMaskDays()
    {
        return $this->maskDays;
    }

    /**
     * Set isBlackDay
     *
     * @param boolean $isBlackDay
     *
     * @return Calendar
     */
    public function setIsBlackDay($isBlackDay)
    {
        $this->isBlackDay = $isBlackDay;

        return $this;
    }

    /**
     * Get isBlackDay
     *
     * @return boolean
     */
    public function getIsBlackDay()
    {
        return $this->isBlackDay;
    }

    /**
     * Set rule
     *
     * @param \AppBundle\Entity\Rule $rule
     * @return Calendar
     */
    public function setRule(\AppBundle\Entity\Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return \AppBundle\Entity\Rule 
     */
    public function getRule()
    {
        return $this->rule;
    }
    
    
    
    
      
    
    static $dayMasks=array(64, 1, 2, 4, 8, 16, 32);
    
    public function containsDate($date, $dayOfWeek) {
        return  $this->dateIn<=$date && 
                $date<=$this->dateEnd && 
                $this->maskDays & self::$dayMasks[$dayOfWeek];        
    }
    
}
