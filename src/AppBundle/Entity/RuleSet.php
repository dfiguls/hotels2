<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RuleSet
 *
 * @ORM\Table()
 * @ORM\Entity(")
 */
class RuleSet
{
    const BASE_TYPE=1;
    const EXTRA_TYPE=2;
    const PROMO_TYPE=3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"select"})
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer")
     */
    private $ordering;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="No es pot deixar en blanc")
     * @Groups({"select"})
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;
    
    /**
     * @ORM\OneToMany(targetEntity="Rule", mappedBy="ruleSet", cascade={"persist","remove"})
    */
    private $rules;
    
    /**
     * @ORM\OneToMany(targetEntity="CacheCombination", mappedBy="ruleSet", cascade={"persist","remove"})
     */
    private $combinations;   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return RuleSet
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RuleSet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return RuleSet
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return RuleSet
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->combinations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return RuleSet
     */
    public function addRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param \AppBundle\Entity\Rule $rule
     */
    public function removeRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRules()
    {
        return $this->rules;
    }
    
    /**
     * Add combination
     *
     * @param \AppBundle\Entity\CacheCombination $combination
     *
     * @return RuleSet
     */
    public function addCombination(\AppBundle\Entity\CacheCombination $combination)
    {
        $this->combinations[] = $combination;

        return $this;
    }

    /**
     * Remove combination
     *
     * @param \AppBundle\Entity\CacheCombination $combination
     */
    public function removeCombination(\AppBundle\Entity\CacheCombination $combination)
    {
        $this->combinations->removeElement($combination);
    }

    /**
     * Get combinations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCombinations()
    {
        return $this->combinations;
    }
    
    //-----------------------------------------------------------------------
    
    public function getDatesInterval() {
        $dateIn=$dateEnd=null;
         foreach($this->rules as $rule) {
            list ($inDateIn, $inDateEnd)=$rule->getDatesInterval();
            if (!$dateIn || $inDateIn<$dateIn) $dateIn=$inDateIn;
            if (!$dateEnd || $inDateEnd>$dateEnd) $dateEnd=$inDateEnd;     
        }
        return array($dateIn, $dateEnd);
    }
    
    public function findCombination($txtIds) {
        foreach($this->combinations as $combination) {
            if ($combination->getTxtIds()===$txtIds) return $combination;
        }
        return null;
    }
    
    public function mergeRuleParameters() {
        //find options used by rules in this ruleSet
        $options=[];
        foreach($this->rules as $rule) {            
            foreach($rule->getOptions() as $ruleOption) {
                $option=$ruleOption->getOption();
                if (!array_key_exists($option->getId(), $options)) {
                    $options[$option->getId()]=$option; 
                }
            }                
        }
        $parameters=[];
        foreach($options as $option) {
            $parameter=$option->getParameter();
            if (!array_key_exists($parameter->getId(), $parameters)) {
                $parameters[$parameter->getId()]=array('parameter'=>$parameter, 'options'=>[]);
            }
            $parameters[$parameter->getId()]['options'][]=$option;
        }        
        return $parameters;
    }
    
    public function mergeRuleCombinations($parameters) {
        $combinations=array();
        $iterator=new CombinationIterator($parameters);
        while ($iterator->next()) {
            $combination=new CacheCombination();
            $combination->setRuleSet($this);
            foreach($iterator->getOptions() as $option) {
                $combination->addOption($option);
            }
            $combinations[]=$combination;
        }
        
        return $combinations;
    }
    
    public static function combinationsAreEquals($a, $b) {
        if (count($a)!=count($b)) return false;
        $aIds=array();
        foreach($a as $combination) $aIds[$combination->getTxtIds()]=true;
        foreach($b as $combination) 
            if (!array_key_exists($combination->getTxtIds(), $aIds)) 
                return false;
        return true;
    }
    
    //$a is a ruleCombination with all or less parameters
    //$b is a cacheCombination with all parameters
    public static function combinationBelongsTo($a, $b) {
        foreach($a->getOptions() as $oa) {
            if (!$b->hasOption($oa)) return false;
        }
        return true;
    }
    
}


class CombinationIterator {
    protected $parameters;
    protected $indexs;
    public function __construct($parameters) {
        $this->parameters=[];
        foreach($parameters as $parameter) $this->parameters[]=$parameter; //remove parameterId in key
        $this->indexs=[];
        foreach($parameters as $parameters) $this->indexs[]=0;
        $this->indexs[0]=-1;
    }
    public function next() {
        $done=false;
        $i=0;
        while (!$done) {
            $this->indexs[$i]++;
            $done=true;
            if ($this->indexs[$i]>=count($this->parameters[$i]['options'])) {
                $done=false;
                $this->indexs[$i]=0;
                $i++;
                if ($i>=count($this->parameters)) { return false; }
            }
        }
        return true;
    }
    
    public function getOptions() {
        $combination=array();
        foreach($this->indexs as $i=>$index) {
            $p=$this->parameters[$i];
            $o=$p['options'][$index];
            $combination[]=$o;
        }
        return $combination;
    }

}
