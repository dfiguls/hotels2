<?php

namespace AppBundle\Entity;

abstract class DataTablesRepository extends \Doctrine\ORM\EntityRepository
{
    abstract protected function getAlias();
    abstract protected function getOrderField($orderBy);
    abstract protected function getSearchFields();
    protected function getJoin() { return array(); }
        
    public function select($fixedConditions, $searchValue=null, $searchCustom=null, $orderBy=null, $orderOrder=null, $start=null, $length=null) {
        $query=$this->selectFields($fixedConditions, null, $searchValue, $searchCustom, $orderBy, $orderOrder);
        if ($start || $length) $query->setFirstResult($start)->setMaxResults($length);
        return $query;
    }
    
    public function count($fixedConditions, $searchValue=null, $searchCustom=null) {
        $alias=$this->getAlias();
        $query=$this->selectFields($fixedConditions, 'COUNT('.$alias.'.id)', $searchValue, $searchCustom, null, null);
        $dql=$query->getQuery();
        return $dql->getSingleScalarResult();
    }
    
    private function selectFields($fixedConditions, $fields, $searchValue, $searchCustom, $orderBy, $orderOrder) {
        $alias=$this->getAlias();
        $query=$this->createQueryBuilder($alias);
        
        foreach($this->getJoin() as $field=>$fieldAlias) 
            $query->leftJoin($field, $fieldAlias);
        
        if ($fields) $query->select($fields);
       
        if ($orderBy!==null) {
            $orderField=$this->getOrderField($orderBy);
            if (is_array($orderField)) {
                foreach($orderField as $order) {
                    $query->addOrderBy($order, $orderOrder);
                }
            } else {
                $query->orderBy($orderField, $orderOrder);
            }
        }
        
        if ($searchValue) {
            $cond='1=0';
            foreach ($this->getSearchFields() as $searchField) 
                $cond.=' OR '.$searchField.' like :name';
            $query->andWhere($cond);
            $query->setParameter('name', '%'.$searchValue.'%');
        }
        
        if ($searchCustom) {
            $query->andWhere($searchCustom);
        }
        
        if ($fixedConditions) {
            foreach($fixedConditions as $condition) {
                $query->andWhere($condition);
            }
        }
        return $query;       
    }
    
}
