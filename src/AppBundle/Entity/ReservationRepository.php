<?php
namespace AppBundle\Entity;

class ReservationRepository extends DataTablesRepository
{
    protected function getAlias() { return 'r'; }
    private $orderFields=array('r.id','r.id','r.reservationDate','c.name','c.surname','r.dateIn','r.dateEnd','c.NIF');
    protected function getOrderField($orderBy) { return $this->orderFields[$orderBy]; }
    protected function getSearchFields() { return array('c.name','c.surname','c.NIF'); }
    protected function getJoin() { return array('r.user'=>'c'); }
    
    public function findByDates($dateInYMD, $dateEndYMD) {
        $query=$this->createQueryBuilder('r')
            ->andWhere("r.dateIn <= :dateEnd")
            ->andWhere("r.dateEnd >= :dateIn")
            ->setParameter('dateIn', $dateInYMD)
            ->setParameter('dateEnd', $dateEndYMD)
            ;
        return $query;       
    }
    
    public function findByDatePromo($date, $promo){
        $query=$this->createQueryBuilder('r')
            ->select('r, count(r)')
            ->andWhere('r.enabled=true')
            ->andWhere("r.dateIn <= :date")
            ->andWhere("r.dateEnd >= :date")
            ->setParameter('date', $date)
            ->groupBy('r.moorageType')
            ;
        if ($promo) $query->join('r.promos','p')->andWhere("p=:promo")->setParameter('promo',$promo);
        
        return $query->getQuery()->getResult(); 
    }
    
    public function findCheckIns($dateIn, $dateEnd){
        $query=$this->createQueryBuilder('r')
            ->select('r')
            ->andWhere("r.enabled=true")
            ->andWhere("r.dateIn >= :dateIn")
            ->andWhere("r.dateIn <= :dateEnd")
            ->setParameter('dateIn', $dateIn)
            ->setParameter('dateEnd', $dateEnd)
            ;
        
        return $query->getQuery()->getResult(); 
    }

    public function findCheckOuts($dateIn, $dateEnd){
        $query=$this->createQueryBuilder('r')
            ->select('r')
            ->andWhere("r.enabled=true")
            ->andWhere("r.dateEnd >= :dateIn")
            ->andWhere("r.dateEnd <= :dateEnd")
            ->setParameter('dateIn', $dateIn)
            ->setParameter('dateEnd', $dateEnd)
            ;
        
        return $query->getQuery()->getResult(); 
    }
    
    public function clearPromo($promo) {
        $this->getEntityManager()->getConnection()->exec("DELETE FROM reservation_promos WHERE rule_id=".$promo->getId());
    }
    
}

?>
