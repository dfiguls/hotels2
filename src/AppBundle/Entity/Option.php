<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Option
 *
 * @ORM\Table(name="options")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OptionRepository")
 */
class Option
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"grid","parameterForm","select","rules"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="No es pot deixar en blanc")
     * @Groups({"grid","parameterForm","select"})
     */
    private $name;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer")
     * @Assert\GreaterThan(value = 0, message="Ha se ser un número més gran de 0")
     * @Groups({"parameterForm"})
     */
    private $order;        
    
    /**
     * @var integer
     *
     * @ORM\Column(name="dispo", type="integer", nullable=true)
     * @Groups({"parameterForm"})
     */
    private $dispo;      
        
    /**
     * @var integer
     *
     * @ORM\Column(name="pax", type="integer", nullable=true)
     * @Groups({"parameterForm"})
     */
    private $pax;         
    
     /**
     * @ORM\ManyToOne(targetEntity="Parameter", inversedBy="options")
     * @ORM\JoinColumn(name="parameter_id", referencedColumnName="id")
     * @Groups({"selectOption"})
     */
    private $parameter;
    
    /**
    * @var ArrayCollection $translations
    *
    * @ORM\OneToMany(targetEntity="OptionTranslation", mappedBy="option",  cascade={"persist","remove"})
    * @Groups({"parameterForm"})
    */
    protected $translations;
    
    /**
     * @ORM\OneToMany(targetEntity="OptionImage", mappedBy="option", cascade={"persist","remove"}))
     * @Groups({"parameterForm"})
     */
    private $images;      

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Option
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Option
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set parameter
     *
     * @param \AppBundle\Entity\Parameter $parameter
     *
     * @return Option
     */
    public function setParameter(\AppBundle\Entity\Parameter $parameter = null)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return \AppBundle\Entity\Parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Add translation
     *
     * @param \AppBundle\Entity\OptionTranslation $translation
     *
     * @return Option
     */
    public function addTranslation(\AppBundle\Entity\OptionTranslation $translation)
    {
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \AppBundle\Entity\OptionTranslation $translation
     */
    public function removeTranslation(\AppBundle\Entity\OptionTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
    
    
   /**
     * Set dispo
     *
     * @param integer $dispo
     *
     * @return Option
     */
    public function setDispo($dispo)
    {
        $this->dispo = $dispo;

        return $this;
    }

    /**
     * Get dispo
     *
     * @return integer
     */
    public function getDispo()
    {
        return $this->dispo;
    }

    /**
     * Set pax
     *
     * @param integer $pax
     *
     * @return Option
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }

    /**
     * Get pax
     *
     * @return integer
     */
    public function getPax()
    {
        return $this->pax;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\OptionImage $image
     *
     * @return Option
     */
    public function addImage(\AppBundle\Entity\OptionImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\OptionImage $image
     */
    public function removeImage(\AppBundle\Entity\OptionImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }    
    
    //===================================================================================
    public function getNameTranslation($locale) {
        if (!$locale) $locale='ca';
        foreach($this->translations as $translation) {
            if ($translation->getLocale()===$locale && $translation->getName()!=null) return $translation->getName();
        }
        return $this->name;
    }    

    public function getDescriptionTranslation($locale) {
        if (!$locale) $locale='ca';
        foreach($this->translations as $translation)
            if ($translation->getLocale()===$locale) return $translation->getDescription();
        return '---';
    }    

    /**
     * @Groups({"select"})
     */
    function getParameterId() {
        return $this->parameter?$this->parameter->getId():-1;
    }
    
    public static function getTxtIds($options) {
        $aIds=array();
        foreach($options as $option) { $aIds[]=$option->getId(); }
        sort($aIds);
        return implode(',',$aIds);
    }
    
    public function getMainImage() {
        if (count($this->images)>0) return $this->images[0]->getName();
        return null;
    }
    
}
