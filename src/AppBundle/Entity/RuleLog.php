<?php

namespace BookingTrend\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rule
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RuleLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"rulelogs","rulelogForm"})
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="modification", type="text")
     * @Groups({"rulelogForm"})
     */
    private $modification;


    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @Groups({"rulelogs","rulelogForm"})
     */
    private $date;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({"rulelogs","rulelogForm"})
     */
    private $user;    

   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modification
     *
     * @param string $modification
     * @return RuleLog
     */
    public function setModification($modification)
    {
        $this->modification = $modification;

        return $this;
    }

    /**
     * Get modification
     *
     * @return string 
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return RuleLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \BookingTrend\HotelBundle\Entity\User $user
     * @return RuleLog
     */
    public function setUser(\BookingTrend\HotelBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BookingTrend\HotelBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
