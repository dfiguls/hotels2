<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rule
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class RuleCombination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"rules"})
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Option")
     * @ORM\JoinTable(name="rule_combination_option",
     *      joinColumns={@ORM\JoinColumn(name="combination_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id")}
     *      )
     * @Groups({"rules"})
     */
    private $options;

    /**
     * @ORM\ManyToOne(targetEntity="Rule", inversedBy="rates")
     * @ORM\JoinColumn(name="rule_id", referencedColumnName="id")
     */
    private $rule;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rate", type="string", length=50, nullable=true)
     * @Groups({"rules"})
     */
    private $rate;
    


    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Combination
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Add option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return Combination
     */
    public function addOption(\AppBundle\Entity\Option $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \AppBundle\Entity\Option $option
     */
    public function removeOption(\AppBundle\Entity\Option $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return Combination
     */
    public function setRule(\AppBundle\Entity\Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return \AppBundle\Entity\Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
    
    
    
    //============================================================================================
    public function isIncremental() {
        if ($this->rate) {
            $sign=substr($this->rate,0,1);
            return $sign==='+' || $sign==='-';
        }
        return false;
    }

    public function getTxtIds() {
        return Option::getTxtIds($this->options);
    }

}
