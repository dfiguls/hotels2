<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ConditionType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ParameterRepository")
 */
class Parameter
{
    const ROOM_TYPE=1;
    const ACCOMMODATION=2;
    const PAYMENT_METHOD=3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"grid","parameterForm","select"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="No es pot deixar en blanc")
     * @Groups({"grid","parameterForm","select"})
     */
    private $name;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="hasDispo", type="boolean", nullable=true)
     * @Groups({"parameterForm","select"})
     */
    private $hasDispo;         

    /**
     * @var boolean
     *
     * @ORM\Column(name="hasRate", type="boolean", nullable=true)
     * @Groups({"parameterForm","select"})
     */
    private $hasRate;         

    /**
     * @var boolean
     *
     * @ORM\Column(name="hasPax", type="boolean", nullable=true)
     * @Groups({"parameterForm","select"})
     */
    private $hasPax;             
    
     /**
     * @ORM\OneToMany(targetEntity="Option", mappedBy="parameter", cascade={"persist","remove"})
     * @Groups({"parameterForm"})
     */
    protected $options;
    
    /**
    * @var ArrayCollection $translations
    *
    * @ORM\OneToMany(targetEntity="ParameterTranslation", mappedBy="parameter",  cascade={"persist","remove"})
    * @Groups({"parameterForm"})
    */
    protected $translations;
    


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Parameter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return Parameter
     */
    public function addOption(\AppBundle\Entity\Option $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \AppBundle\Entity\Option $option
     */
    public function removeOption(\AppBundle\Entity\Option $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Add translation
     *
     * @param \AppBundle\Entity\parameterTranslation $translation
     *
     * @return Parameter
     */
    public function addTranslation(\AppBundle\Entity\parameterTranslation $translation)
    {
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \AppBundle\Entity\parameterTranslation $translation
     */
    public function removeTranslation(\AppBundle\Entity\parameterTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

  

    /**
     * Set hasDispo
     *
     * @param boolean $hasDispo
     *
     * @return Parameter
     */
    public function setHasDispo($hasDispo)
    {
        $this->hasDispo = $hasDispo;

        return $this;
    }

    /**
     * Get hasDispo
     *
     * @return boolean
     */
    public function getHasDispo()
    {
        return $this->hasDispo;
    }

    /**
     * Set hasRate
     *
     * @param boolean $hasRate
     *
     * @return Parameter
     */
    public function setHasRate($hasRate)
    {
        $this->hasRate = $hasRate;

        return $this;
    }

    /**
     * Get hasRate
     *
     * @return boolean
     */
    public function getHasRate()
    {
        return $this->hasRate;
    }

    /**
     * Set hasPax
     *
     * @param boolean $hasPax
     *
     * @return Parameter
     */
    public function setHasPax($hasPax)
    {
        $this->hasPax = $hasPax;

        return $this;
    }

    /**
     * Get hasPax
     *
     * @return boolean
     */
    public function getHasPax()
    {
        return $this->hasPax;
    }
}
