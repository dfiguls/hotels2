<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ReservationOption
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ReservationOption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"reservationForm"})
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="pax", type="integer", nullable=true)
     * @Groups({"reservationForm"})
     */
    private $pax;    

    /**
     * @ORM\ManyToOne(targetEntity="Option")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;    
    
    /**
     * @ORM\ManyToOne(targetEntity="Reservation", inversedBy="options")
     * @ORM\JoinColumn(name="reservation_id", referencedColumnName="id")
     */
    private $reservation;    



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pax
     *
     * @param integer $pax
     *
     * @return ReservationOption
     */
    public function setPax($pax)
    {
        $this->pax = $pax;

        return $this;
    }

    /**
     * Get pax
     *
     * @return integer
     */
    public function getPax()
    {
        return $this->pax;
    }

    /**
     * Set option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return ReservationOption
     */
    public function setOption(\AppBundle\Entity\Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \AppBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     *
     * @return ReservationOption
     */
    public function setReservation(\AppBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \AppBundle\Entity\Reservation
     */
    public function getReservation()
    {
        return $this->reservation;
    }
    
    
    //-------------------------------------------
    /**
     * @Groups({"reservationForm"})
     */
    public function getOptionId() {
        return $this->option?$this->option->getId():-1;
    }
}
