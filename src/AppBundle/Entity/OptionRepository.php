<?php

namespace AppBundle\Entity;

class OptionRepository extends DataTablesRepository
{
    protected function getAlias() { return 'b'; }
    private $orderFields=array('b.id','b.name');
    protected function getOrderField($orderBy) { return $this->orderFields[$orderBy]; }
    protected function getSearchFields() { return array('b.name'); }
    
    public function findOptions($optionIds) {
        $str=implode(',',$optionIds);
        $qb=$this->createQueryBuilder('c')
            ->andWhere('c.id in ('.$str.')');
        return $qb->getQuery()->getResult();
    }
    
    public function selectAll() {
        return $this->createQueryBuilder('qb')
            ->select('o')
            ->from('AppBundle:Option', 'o')
            ->getQuery()->getResult();        
    }         
}
