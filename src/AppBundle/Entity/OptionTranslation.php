<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * OptionTranslation
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class OptionTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"parameterForm"})
     */
    private $id;
   
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups({"parameterForm"})
     */
    private $name;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"parameterForm"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Option", inversedBy="translations")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    
    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

  

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OptionTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set option
     *
     * @param \AppBundle\Entity\Option $option
     *
     * @return OptionTranslation
     */
    public function setOption(\AppBundle\Entity\Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \AppBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return OptionTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }
    

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OptionTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }    
    
    //=================================================================================================
    
    /**
     * @Groups({"parameterForm"})
     */
    public function getLanguageId()
    {
        return $this->language?$this->language->getId():null;
    }    
    
    public function getLocale() {
        return $this->language?$this->language->getLocale():null;
    }

}
