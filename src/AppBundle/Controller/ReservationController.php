<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Services\CacheCalendarManager;
use AppBundle\Services\JsonHelper;
use AppBundle\Entity\User;
/**
* @Route("/reservations")
*/
class ReservationController extends Controller
{
    /**
     * @Route("/", name="reservations")
     */
    public function indexAction()
    {
        return $this->render('reservation/index.html.twig');
    }
    
     /**
     * @Route("/json/reservations", name="reservations_json")
     * @Method("GET")
     */
    public function jsonReservationsAction(Request $request)
    {                
        $repository = $this->getDoctrine()->getRepository('AppBundle:Reservation');        
        $user=$this->getUser();
        $fixedConditions=$user->getRole()===User::ROLE_USER?array('c.id='.$user->getId()):null;
        return JsonHelper::processDataTables($repository,$fixedConditions,$request, function($o){
            return array(
                'id'=>$o->getId(),
                'reservationDate'=>$o->getReservationDateDMY(),
                'name'=>$o->getUser()->getName(), 
                'surname'=>$o->getUser()->getSurname(), 
                'dateIn'=>$o->getDateInDMY(),
                'dateEnd'=>$o->getDateEndDMY(),
                'nif'=>$o->getUser()->getNIF(),
                'paymentMethod'=>$o->getPaymentMethod(),
                'transactionId'=>$o->getTransactionId(),
                'enabled'=>$o->getEnabled()            );
        });     
    }        
    
    
    /**
     * @Route("/reservation.html", name="reservation_html")
     */
    public function reservationAction()
    {
        return $this->render('reservation/reservation.html.twig');
    }
    
    /**
     * @Route("/viewReservationDlg.html", name="view_reservation_dlg_html")
     */
    public function viewReservationDlgAction()
    {
         return $this->render('reservation/viewReservationDlg.html.twig');
    }
        
    /**
     * @Route("/view/{id}", name="reservation_view")
     */
    public function viewAction($id=-1)
    {
        $reservation=$this->getDoctrine()->getRepository('AppBundle:Reservation')->find($id);
        if (!$reservation)  throw new NotFoundHttpException("Reserva $id no trobada");
        
        $language=$reservation->getLanguage();
        if (!$language) $language=$this->getDoctrine()->getRepository('AppBundle:Language')->find(1);
        if (!$language) throw new \Exception('Reservation without defined language');
        $translator = $this->get('translator');
        //$sessionLocale = $translator->getLocale();
        $translator->setLocale($language->getLocale());
        
        return $this->render('reservation/mail.html.twig',array('reservation' => $reservation));
    }
    
    
    
    /**
     * @Route("/viewReservationCalendarDlg.html", name="view_reservation_calendar_dlg_html")
     */
    public function viewReservationCalendarDlgAction()
    {
        return $this->render('reservation/viewReservationCalendarDlg.html.twig');
    }
        

    
    /**
     * @Route("/viewCalendar/{id}", name="reservation_view_calendar")
     */
    public function viewCalendarAction($id=-1)
    {
        $reservation=$this->getDoctrine()->getRepository('AppBundle:Reservation')->find($id);
        if (!$reservation)  throw new NotFoundHttpException("Reserva $id no trobada");
        
        $language=$reservation->getLanguage();
        if (!$language) $language=$this->getDoctrine()->getRepository('AppBundle:Language')->find(1);
        if (!$language) throw new \Exception('Reservation without defined language');
        $translator = $this->get('translator');
        //$sessionLocale = $translator->getLocale();
        $translator->setLocale($language->getLocale());
        
        return $this->render('reservation\mail.html.twig',array('reservation' => $reservation));
    }
    
     /**
     * @Route("/pdf/{id}", name="reservation_pdf")
     */
    public function pdfAction($id=-1)
    {
        $reservation=$this->getDoctrine()->getRepository('AppBundle:Reservation')->find($id);
        if (!$reservation)  throw new NotFoundHttpException("Reserva $id no trobada");
        
        $language=$reservation->getLanguage();
        if (!$language) $language=$this->getDoctrine()->getRepository('AppBundle:Language')->find(1);
        if (!$language) throw new \Exception('Reservation without defined language');
        $translator = $this->get('translator');
        //$sessionLocale = $translator->getLocale();
        $translator->setLocale($language->getLocale());

        $templating=$this->get('templating');
        $html=$templating->render('reservation/mail.pdf.twig',array('reservation'=>$reservation));

        $pdf = $this->get("white_october.tcpdf")->create();
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(TRUE, 'PDF_MARGIN_BOTTOM');
        $pdf->AddPage();
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $txt=$pdf->Output('','S');
        
        return new Response($txt, 200, array('content-type' => 'application/pdf'));
    }
    
    
     
    /**
     * @Route("/json/reservation/{id}", name="reservation_json")
     * @Method({"GET","PUT","DELETE"})
     */
    public function jsonReservationAction(Request $request, $id=-1)
    {              
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository('AppBundle:Reservation');
        $em->getConnection()->beginTransaction(); 
        try {
            $reservation=$repository->find($id);
            if ($request->isMethod('PUT') || ($request->isMethod('DELETE') && $reservation->getEnabled()) ){                
                if ($reservation) CacheCalendarManager::removeReservation($em, $reservation);
                $em->flush();
            }

            if ($request->isMethod('DELETE')) {
                $reservation->setEnabled(false);
                $em->persist($reservation);
                $em->flush();
                $response=array('ok'=>true);
            } else {
                $response=JsonHelper::_processCRUD($this->getDoctrine(), $this->get('serializer'), 
                    $this->get('validator'), $this->get('logger'), $this->getUser(), $request, $id, 
                    'AppBundle:Reservation',array('reservationForm')/*, function($new) use (&$reservation) { $reservation=$new; }*/);
            }
            
            if ($request->isMethod('PUT')){
                if ($response['ok']) {
                    if (!$reservation) $reservation=$repository->find($response['id']);
                    CacheCalendarManager::addReservation($em, $reservation);
                    $em->flush();
                } else throw new \Exception();
            }
            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            if (!$response || $response['ok']!==false) throw $e;
        }
        return new JsonResponse($response);
    }
    
}
