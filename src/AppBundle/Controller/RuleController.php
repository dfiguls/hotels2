<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Services\JsonHelper;
use AppBundle\Services\CacheCalendarManager;
use AppBundle\Entity\Rule;

/**
* @Route("/admin/rules")
*/
class RuleController extends Controller
{
    /**
     * @Route("/", name="rules")
     */
    public function indexAction()
    {
        return $this->render('rule/index.html.twig');
    }   
        
    
    /**
     * @Route("/json/rules", name="rules_json")
     * @Method("GET")
     */
    public function jsonRulesAction(Request $request)
    {        
        $serializer = $this->get('serializer');
                
        $dateIn= \DateTime::createFromFormat('!d-m-Y', $request->get('dateIn'));
        $dateEnd= \DateTime::createFromFormat('!d-m-Y', $request->get('dateEnd'));
        $aRules=$this->selectByDate($dateIn,$dateEnd);        

        $parameters=$this->getDoctrine()->getRepository('AppBundle:Parameter')->selectAll();
        $aParameters=$serializer->normalize($parameters,null,array('groups'=>array('select')));

        $options=$this->getDoctrine()->getRepository('AppBundle:Option')->selectAll();
        $aOptions=$serializer->normalize($options,null,array('groups'=>array('select')));

        $ruleSets=$this->getDoctrine()->getRepository('AppBundle:RuleSet')->findBy(array());
        $aRuleSets=$serializer->normalize($ruleSets,null,array('groups'=>array('select')));

        return new JsonResponse(array('rules'=>$aRules,'parameters'=>$aParameters, 'options'=>$aOptions, 'ruleSets'=>$aRuleSets));                   
    }
    
    
    
        
    /**
     * @Route("/rule.html", name="rule_html")
     */
    public function ruleAction()
    {
        return $this->render('rule/rule.html.twig');
    }
    
    /**
     * @Route("/json/rule/{id}", name="rule_json")
     * @Method({"GET","PUT","DELETE"})
     */
    public function jsonRuleAction(Request $request, $id=-1)
    {       
        $em=$this->getDoctrine()->getManager();
        $logger=$this->get('logger');

        //remember initial dates interval
        $inDateIn=$inDateEnd=null;
        $rule=$em->getRepository('AppBundle:Rule')->find($id);
        if ($rule) {
            list ($inDateIn, $inDateEnd)=$rule->getDatesInterval();
        }
        
        if ($request->isMethod('DELETE')) {
            $logger->info('rule: DELETE '.$id);
            //update cache
            $mgr=new CacheCalendarManager($em);
            if ($inDateIn && $inDateEnd) { 
                $mgr->ruleSetToCache($rule->getRuleSet(), $inDateIn, $inDateEnd);                 
            }
            
            $em->remove($rule);
            $em->flush();

            $response=array('ok'=>true); 
        } else if ($request->isMethod('PUT')) {
            $response= JsonHelper::_processCRUD($this->getDoctrine(), $this->get('serializer'), $this->get('validator'),
                $this->get('logger'), $this->getUser(), $request, $id, 'AppBundle:Rule',array('ruleForm'));
        
            //sync cache
            $rule=$em->getRepository('AppBundle:Rule')->find($response['id']); //for new rules--> the new id!
            list ($dateIn, $dateEnd)=$rule->getDatesInterval();
            $mgr=new CacheCalendarManager($em);
            $dIn =$inDateIn  && $inDateIn <$dateIn ?$inDateIn :$dateIn;
            $dEnd=$inDateEnd && $inDateEnd>$dateEnd?$inDateEnd:$dateEnd;
            if ($dIn && $dEnd) {
                $mgr->ruleSetToCache($rule->getRuleSet(), $dIn, $dEnd);
                //TODO $mgr->updateDependentPromos($dIn, $dEnd);
            } 
        }         
        return new JsonResponse($response);
    }
        
    /**
     * @Route("/json/rules/order", name="rules_order_json")
     * @Method({"PUT"})
     */
    public function jsonRuleOrderAction(Request $request)
    {       
        $em=$this->getDoctrine()->getManager();
        $params = json_decode($request->getContent(), true);
        $id1=$params['id1'];
        $id2=$params['id2'];
        
        $rule1=$em->getRepository('AppBundle:Rule')->find($id1);
        $rule2=$em->getRepository('AppBundle:Rule')->find($id2);
        if ($rule1 && $rule2) {
            $o=$rule1->getOrdering();
            $rule1->setOrdering($rule2->getOrdering());
            $rule2->setOrdering($o);
            $em->persist($rule1);
            $em->persist($rule2);
            $em->flush();
            return new JsonResponse('OK');
        }
        return new \Symfony\Component\HttpKernel\Exception\HttpException(404);
    }
    
    
     /*TODO
     * @Route("/json/selectPromos", name="promosselect_json")
     * @Method("GET")
     *
    public function jsonPromosSelectAction(Request $request)
    {    
        $serializer = $this->get('serializer');

        $repository = $this->getDoctrine()->getRepository('AppHotelBundle:Rule');
        $promos=$repository->findBy(array('isPromo'=>true));
        $aPromos=$serializer->normalize($promos, null, array('groups' => array('select')));
        return new JsonResponse($aPromos);
    }*/
    
    
    
    /**
     * @Route("/json/selectByDate", name="rulesselectbydate_json")
     * @Method({"GET"})
     */
    public function jsonSelectByDateAction(Request $request)
    {              
        $dateIn= \DateTime::createFromFormat('!d-m-Y', $request->get('dateIn'));
        $dateEnd= \DateTime::createFromFormat('!d-m-Y', $request->get('dateEnd'));
        $aRules=$this->selectByDate($dateIn,$dateEnd);        
        return new JsonResponse($aRules); 
    }
    
    
    public function selectByDate($dateIn, $dateEnd) {
        $ruleSet=$this->getDoctrine()->getRepository('AppBundle:RuleSet')->find(1); //base rules
        $serializer = $this->get('serializer');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Rule');
        $queryBuilder=$repository->findByDates($ruleSet, $dateIn->format('Y-m-d'), $dateEnd->format('Y-m-d'));
        $rules=$queryBuilder->getQuery()->getResult();
        $aRules=$serializer->normalize($rules, null, array('groups' => array('rules')));        
        return $aRules;
    }
    
}
