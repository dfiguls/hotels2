<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\JsonHelper;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/admin/parameters")
*/
class ParameterController extends Controller
{
    /**
     * @Route("/", name="parameters")
     */
    public function indexAction()
    {
        return $this->render('parameter/index.html.twig');
    }
    
    /**
     * @Route("/parameter.html", name="parameter_html")
     */
    public function parameterAction()
    {
        return $this->render('parameter/parameter.html.twig');
    }
    
    /**
     * @Route("/option.html", name="option_html")
     */
    public function optionAction()
    {
        return $this->render('parameter/option.html.twig');
    }    
        
    
    /**
     * @Route("/json/parameter/{id}", name="parameter_json")
     * @Method({"GET","PUT","DELETE"})
     */
    public function jsonParameterAction(Request $request, $id=-1)
    {       
        $path = $this->get('kernel')->getRootDir() . '/../web/optionImages/';
        $response=JsonHelper::_processCRUD($this->getDoctrine(), $this->get('serializer'), $this->get('validator'),
            $this->get('logger'), $this->getUser(), $request, $id, 'AppBundle:Parameter',array('parameterForm'),
                function($entity,&$diff) use($path) {                    
                    if (array_key_exists('options',$diff)) {
                        foreach($diff['options'] as &$option) {
                            if (array_key_exists('images',$option)) {
                                foreach($option['images'] as &$image) {
                                    if (array_key_exists('data', $image)) {                                
                                        $filename = tempnam($path, "FOO");
                                        $image['name']=basename($filename);
                                        $handle = fopen($filename, "wb"); 
                                        $data = explode(',', $image['data']);
                                        if (count($data)>1) { //image data
                                            fwrite($handle, base64_decode($data[1])); 
                                        } else if (substr($image['data'],0,4)==='http') { //image url
                                            fwrite($handle, file_get_contents($image['data']));
                                        }
                                        fclose($handle);
                                        chmod($filename,0644);  //0xxx --> octal
                                        unset($image['data']);
                                    }
                                }
                                unset($image);
                            }                             
                        }
                    }
                },null,function($entity,$entitiesToRemove) use ($path){
                    foreach($entitiesToRemove as &$ee) {
                        $c=get_class($ee);
                        if ($c=='AppBundle\\Entity\\OptionImage') {
                            unlink($path.$ee->getName());
                        }
                    }
                }
            );
        if ($request->getMethod()=='GET') {
            $serializer = $this->get('serializer');
            $languages=$this->getDoctrine()->getRepository('AppBundle:Language')->findAll();
            $response=array('parameter'=>$response, 'languages'=>$serializer->normalize($languages, null, array('groups' => 'form')));
        }
        return new JsonResponse($response);
    }
        

    
    /**
     * @Route("/json/parameters", name="parameters_json")
     * @Method("GET")
     */
    public function jsonParametersAction(Request $request)
    {        
        $repository = $this->getDoctrine()->getRepository('AppBundle:Parameter');        
        return JsonHelper::processDataTables($repository,null,$request, function($o){
            return array(
                'id'=>$o->getId(), 
                'name'=>$o->getName()
            );
        });          
    }
    
    /**
     * @Route("/json/selectParameters", name="parametersselect_json")
     * @Method("GET")
     */
    public function jsonParametersSelectAction(Request $request)
    {    
        $serializer = $this->get('serializer');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Parameter');
        $entities=$repository->findAll();
        $a=$serializer->normalize($entities, null, array('groups' => array('select')));
        return new JsonResponse($a);
    }
    
}
