<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Services\JsonHelper;

/**
* @Route("/admin/users")
*/
class UserController extends Controller
{
    /**
     * @Route("/", name="users")
     */
    public function indexAction()
    {
        $em=$this->getDoctrine()->getManager();
        $countries=$em->getRepository('AppBundle:Country')->findAll();
        return $this->render('user/index.html.twig',array('countries'=>$countries));
    }
    
    /**
     * @Route("/user.html", name="user_html")
     */
    public function userAction()
    {
        return $this->render('user/user.html.twig');
    }
        
     
    /**
     * @Route("/json/user/{id}", name="user_json")
     * @Method({"GET","PUT","DELETE"})
     */
    public function jsonUserAction(Request $request, $id=-1)
    {       
        $user=$repository = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        $oldPassword=$user?$user->getPassword():null;
        $encoder=$this->get('security.password_encoder');
        return JsonHelper::processCRUD($this->getDoctrine(), $this->get('serializer'), $this->get('validator'),
                $this->get('logger'), $this->getUser(), $request, $id, 
                'AppBundle:User', array('form'), 
                function($o) { 
                    $o->setPassword(null);                    
                }, null, 
                function($user) use ($encoder, $oldPassword) {  
                    $newPassword=$user->getPassword();
                    if ($newPassword===null) $user->setPassword($oldPassword);
                    else $user->setPassword($encoder->encodePassword($user, $newPassword));                            
                });
    }
       
    
    
    
    /**
     * @Route("/json/users", name="users_json")
     * @Method("GET")
     */
    public function jsonUsersAction(Request $request)
    {                
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');        
        return JsonHelper::processDataTables($repository,null,$request,function($o) {
            return array(
                'id'=>$o->getId(), 
                'name'=>$o->getName(), 
                'surname'=>$o->getSurname(), 
                'date'=>$o->getRegisterDateDMY()
            );
        });
    }
    
    
     /**
     * @Route("/json/select", name="usersselect_json")
     * @Method("GET")
     */
    public function jsonUsersSelectAction(Request $request)
    {    
        $serializer = $this->get('serializer');

        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $users=$repository->findAll();
        $aUsers=$serializer->normalize($users, null, array('groups' => array('select')));
        return new JsonResponse($aUsers);
    }
}
