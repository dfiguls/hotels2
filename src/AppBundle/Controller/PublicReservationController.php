<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Services\JsonHelper;

use AppBundle\Services\CacheCalendarManager;
use AppBundle\Services\SearchEngine;

use AppBundle\Entity\Reservation;

use Braintree_ClientToken;
use Braintree_Transaction;

//use Redsys\RedsysAPI;
use Redsys\RedsysAPI;

/**
* @Route("/public/{_locale}/reservations")
*/
class PublicReservationController extends Controller
{
    /**
     * @Route("", name="public_reservations")
     */
    public function indexAction()
    {        
        $em=$this->getDoctrine()->getManager();
        $countries=$em->getRepository('AppBundle:Country')->findAll();
        
        //PayPal
        $this->get('comet_cult_braintree.factory');
        $clientToken = Braintree_ClientToken::generate();
        return $this->render('publicReservation/index.html.twig', array('countries'=>$countries, 'clientToken'=>$clientToken));
    }
    
    
    /**
     * @Route("/json/checkroomtypes", name="checkroomtypes_json")
     * @Method({"PUT"})
     */
    public function jsonCheckRoomTypesAction(Request $request)
    {             
        $reservation = json_decode($request->getContent(), true);
        $dateIn=\DateTime::createFromFormat('!Y-m-d', $reservation['dateInYMD']); 
        $dateEnd=\DateTime::createFromFormat('!Y-m-d', $reservation['dateEndYMD']);

        try {
            $eng=new SearchEngine($this->getDoctrine()->getManager());
            $result=$eng->findRoomTypes($dateIn, $dateEnd, $request->getLocale());            
            return new JsonResponse(array(
                'ok'=>true, 
                'result'=>$result
            ));
        } catch (\Exception $e) {
            $translator = $this->get('translator');
            $trans=$translator->trans($e->getMessage(),array(),'validators');
            return new JsonResponse(array('ok'=>false, 'error'=>$trans));
        }                   
    }
    
    /**
     * @Route("/json/checkaccommodations", name="checkaccommodations_json")
     * @Method({"PUT"})
     */
    public function jsonCheckAccommodationsAction(Request $request)
    {             
        $em=$this->getDoctrine()->getManager();
        $reservation = json_decode($request->getContent(), true);
        $dateIn=\DateTime::createFromFormat('!Y-m-d', $reservation['dateInYMD']); 
        $dateEnd=\DateTime::createFromFormat('!Y-m-d', $reservation['dateEndYMD']);
        $roomType=$em->getRepository('AppBundle:Option')->find($reservation['roomType']['id']); //roomTypes

        try {
            $eng=new SearchEngine($em);
            $result=$eng->findAccommodations($dateIn, $dateEnd, $roomType, $request->getLocale());            
            return new JsonResponse(array(
                'ok'=>true, 
                'result'=>$result
            ));
        } catch (\Exception $e) {
            $translator = $this->get('translator');
            $trans=$translator->trans($e->getMessage(),array(),'validators');
            return new JsonResponse(array('ok'=>false, 'error'=>$trans));
        }                      
    }
    
    
    /**
     * @Route("/json/checkpaymenttypes", name="checkpaymenttypes_json")
     * @Method({"PUT"})
     */
    public function jsonCheckPaymentTypesAction(Request $request)
    {             
        $em=$this->getDoctrine()->getManager();
        $reservation = json_decode($request->getContent(), true);
        $dateIn=\DateTime::createFromFormat('!Y-m-d', $reservation['dateInYMD']); 
        $dateEnd=\DateTime::createFromFormat('!Y-m-d', $reservation['dateEndYMD']);
        $roomType=$em->getRepository('AppBundle:Option')->find($reservation['roomType']['id']);
        $accommodation=$em->getRepository('AppBundle:Option')->find($reservation['accommodation']['id']);

        try {
            $eng=new SearchEngine($em);
            $result=$eng->findPaymentTypes($dateIn, $dateEnd, $roomType, $accommodation, $request->getLocale());            
            return new JsonResponse(array(
                'ok'=>true, 
                'result'=>$result
            ));
        } catch (\Exception $e) {
            $translator = $this->get('translator');
            $trans=$translator->trans($e->getMessage(),array(),'validators');
            return new JsonResponse(array('ok'=>false, 'error'=>$trans));
        }                      
    }    
    
    /*
     * @Route("/json/checkdispo", name="checkdispo_json")
     * @Method({"PUT"})
     *
    public function jsonCheckDispoAction(Request $request)
    {              
        $em=$this->getDoctrine()->getEntityManager();
        
        $reservation = json_decode($request->getContent(), true);
        //$locale=$request->getLocale();
        $roomType=$em->getRepository('AppBundle:Option')->find($reservation['roomType']['id']);
        $accommodation=$em->getRepository('AppBundle:Option')->find($reservation['accommodation']['id']);
        $paymentType=$em->getRepository('AppBundle:Option')->find($reservation['paymentType']['id']);
        $ages=array();
        foreach($reservation['ages'] as $ag) {
            $pax=null; if (array_key_exists('pax', $ag) && $ag['pax']) $pax=$ag['pax']; 
            $ages[]=array('option'=>$em->getRepository('AppBundle:Option')->find($ag['id']),'pax'=>$pax);
        }
        $dateIn=\DateTime::createFromFormat('!Y-m-d', $reservation['dateInYMD']); 
        $dateEnd=\DateTime::createFromFormat('!Y-m-d', $reservation['dateEndYMD']);
        
        try {
            //$language=$this->getDoctrine()->getRepository('AppBundle:Language')->findOneBy(array('locale'=>$locale));
            $eng=new SearchEngine($this->getDoctrine()->getManager());
            $result=$eng->find($dateIn, $dateEnd, array($roomType,$accommodation), array($roomType, $accommodation, $paymentType), $ages);
            $serializer = $this->get('serializer');
            $response=$serializer->normalize($result, null, array('groups' => array('select','conditiontext')));
            
            return new JsonResponse(array(
                'ok'=>true, 
                'result'=>$response
            ));
        } catch (\Exception $e) {
            $translator = $this->get('translator');
            $trans=$translator->trans($e->getMessage(),array(),'validators');
            return new JsonResponse(array('ok'=>false, 'error'=>$trans));
        }                       
    }**/
    
    
    
    
    
    
    function getReservationJSON($request) {
        $reservationjson=$request->get("reservation");
        if (get_magic_quotes_gpc()) $reservationjson=stripslashes($reservationjson);
        $reservationJSON=json_decode($reservationjson, true);   
        $arrangedJSON=array(
            'dateInYMD'=>$reservationJSON['dateInYMD'],
            'dateEndYMD'=>$reservationJSON['dateEndYMD'],
            'paymentMethod'=>$reservationJSON['paymentMethod'],
            'rate'=>$reservationJSON['rate'],
            'userId'=>$reservationJSON['userId'],
            'backup'=>$reservationJSON['backup'],
            'reservationOptions'=>array(
                array('optionId'=>$reservationJSON['roomType']['id']),
                array('optionId'=>$reservationJSON['accommodation']['id']),
                array('optionId'=>$reservationJSON['paymentType']['id']),
            ),
        );
        /*foreach($reservationJSON['ages'] as $age)
            $arrangedJSON['reservationOptions'][]=array(
                'optionId'=>$age['id'],
                'pax'=>  array_key_exists ('pax',$age)?$age['pax']:0 
            );*/
        return $arrangedJSON;
    }
    
    
    /**
     * @Route("/checkoutBankTransfer", name="checkoutBankTransfer")
     * @Method({"POST"})
     */
    public function checkoutBankTransferAction(Request $request) //Bank transfer
    {              
        $reservationJSON=$this->getReservationJSON($request);
        $result=$this->processReservation($reservationJSON, $request->getLocale());
        return $this->render('publicReservation/end.html.twig', $result);
    }
    
    
    /**
     * @Route("/checkoutPayPal", name="checkoutPayPal")
     * @Method({"POST"})
     */
    public function checkoutPayPalAction(Request $request) //PAYPAL 
    {
        $nonceFromTheClient = $request->get("payment_method_nonce");
        $reservationJSON=$this->getReservationJSON($request);   
        $result=$this->processReservation($reservationJSON, $request->getLocale(), $nonceFromTheClient);
        return $this->render('publicReservation/end.html.twig', $result);
    }
       
    
    /**
     * @Route("/checkoutTPV", name="checkoutTPV")
     * @Method({"POST"})
     */
    public function checkoutTPVAction(Request $request) //TPV 
    {
        $reservationJSON=$this->getReservationJSON($request);
        try {
            $reservation=$this->saveReservation($reservationJSON, $request->getLocale());
        } catch (\Exception $ex) {
            return $this->render('publicReservation/end.html.twig', array('reservationMessage'=>$ex->getMessage()));
        }
        
        $request->getSession()->set('idReservation',$reservation->getId()); //remember id reservation
        
        $miObj = new RedsysAPI();
        $fuc="008412777";
        $terminal="3";
        $moneda="978";
        $trans="0";
        $url=$this->generateUrl('confirmTPV', array(), true);
        $urlOKKO=$this->generateUrl('endTPV', array(), true);
        $id='0000000000'.$reservation->getId();
        $id=substr($id,strlen($id)-10);
        $amount=round($reservation->getRate()*100);
        $version="HMAC_SHA256_V1";
        $kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//Clave recuperada de CANALES

        // Se Rellenan los campos
        $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
        $miObj->setParameter("DS_MERCHANT_ORDER",strval($id));
        $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$fuc);
        $miObj->setParameter("DS_MERCHANT_CURRENCY",$moneda);
        $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$trans);
        $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
        $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$url);
        $miObj->setParameter("DS_MERCHANT_URLOK",$urlOKKO);		
        $miObj->setParameter("DS_MERCHANT_URLKO",$urlOKKO);

        //Datos de configuración

        // Se generan los parámetros de la petición
        $params = $miObj->createMerchantParameters();
        $signature = $miObj->createMerchantSignature($kc);

        return $this->render('publicReservation/TPV.html.twig', array(
            'reservation'=>$reservation, 'version'=>$version, 'params'=>$params, 'signature'=>$signature)
        );
    }
    
    
    
    /**
     * @Route("/confirmTPV", name="confirmTPV")
     * @Method({"POST"})
     */
    public function confirmTPVAction(Request $request) {
        $em=$this->getDoctrine()->getManager();
        
	$miObj = new RedsysAPI();
        $version = $request->request->get('Ds_SignatureVersion');
        $datos = $request->request->get('Ds_MerchantParameters');
        $signatureRecibida = $request->request->get('Ds_Signature');


        $decodec = $miObj->decodeMerchantParameters($datos);	
        $this->get('logger')->info($decodec);
        $kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; //Clave recuperada de CANALES
        $firma = $miObj->createMerchantSignatureNotif($kc,$datos);	

        if ($firma === $signatureRecibida){
            // FIRMA OK
            $id=$miObj->getParameter("Ds_Order");
            $idTransaction=$miObj->getParameter("Ds_AuthorisationCode");
            $errorCode=$miObj->getParameter("Ds_ErrorCode");
            
            $this->get('logger')->info($id);
            $reservation=$this->getDoctrine()->getRepository('AppBundle:Reservation')->find(intval($id));
            if ($reservation) {
                if (!$errorCode) {
                    $reservation->setTransactionId($idTransaction);
                    $reservation->setTransactionStatus("Payed");
                    $em->persist($reservation);
                    $em->flush();

                    $this->get('logger')->info('enviant mail...');
                    $mailMessage='';
                    try {
                        $this->sendReservationMail($reservation);
                        $this->get('logger')->info('mail enviat!');
                    } catch (\Exception $ex) {
                        $mailMessage=$ex->getMessage();
                        $this->get('logger')->error($mailMessage);                                    
                    }
                    $reservation->setTransactionMessage($mailMessage);
                    $em->persist($reservation);
                    $em->flush();
                } else {
                    $this->removeReservation($reservation);  
                    $reservation->setTransactionStatus($errorCode);
                    $em->persist($reservation);
                    $em->flush();
                }
            } else {
                $this->get('logger')->error('reserva '.$id.' no trobada');             
            }            

        } else {
            //FIRMA KO
            $this->get('logger')->error('firma no correcte');
        }

        return new Response('OK');
    }
    
    /**
     * @Route("/endTPV", name="endTPV")
     * @Method({"GET"})
     */
    public function endTPVAction(Request $request) {
       $session=$request->getSession();
       $idReservation=$session->get('idReservation');
       $session->remove('idReservation');
       if ($idReservation) {
            $reservation=$this->getDoctrine()->getRepository('AppBundle:Reservation')->find(intval($idReservation));
            if ($reservation) {
                if ($reservation->getTransactionId()) {
                    $result=array('reservation'=>$reservation, 'mailMessage'=>$reservation->getTransactionMessage());
                } else {
                    $result=array('paymentMessage'=>$reservation->getTransactionStatus());
                }
            } else {
                $result=array('paymentMessage'=>'ERROR reserva '.$idReservation.' no trobada');
            }
       } else {
           $result=array('paymentMessage'=>'ERROR sessio perduda');
       }
       return $this->render('publicReservation/end.html.twig', $result);
    }
       
       
    
    //for PayPay and bank transfer
    private function processReservation($reservationJSON, $locale, $paymentMethodNonce=null) {
        //check and save reservation
        try {
            $reservation=$this->saveReservation($reservationJSON, $locale);
        } catch (\Exception $ex) {
            return array('reservationMessage'=>$ex->getMessage());
        }
        
        //do payment
        try {            
            if ($reservation->getPaymentMethod()==Reservation::PAYMENT_PAYPAL) {
                $this->processPayPalPayment($reservation, $paymentMethodNonce);
            } 
        } catch (\Exception $ex) {
            $this->removeReservation($reservation);            
            return array('paymentMessage'=>$ex->getMessage());
        }
        
        //send mail
        $mailMessage='';
        try {
            $this->sendReservationMail($reservation);
        } catch (\Exception $ex) {
            $mailMessage=$ex->getMessage();
        }
        return array('reservation'=>$reservation, 'mailMessage'=>$mailMessage);
    }
    
    private function processPayPalPayment($reservation, $paymentMethodNonce) {
        $em=$this->getDoctrine()->getManager();
        
        //PayPal
        $this->get('comet_cult_braintree.factory');
        $result = Braintree_Transaction::sale(array(
          'amount' => $reservation->getRate(),
          'paymentMethodNonce' => $paymentMethodNonce,
          'options' => array(
            'submitForSettlement' => True
          )
        ));        
       
        if ($result->success) {    
            $transaction=$result->transaction;
            if ($transaction) {
                $idTransaction=$transaction->id;
                $status=$transaction->status;
                $reservation->setTransactionId($idTransaction);
                $reservation->setTransactionStatus($status);
                $em->persist($reservation);
                $em->flush();
                $message='ok';
            } else {
                $message='success but without transaction';
            }
        } else {
            throw new \Exception($result->message);
        }
        return $message;
    }
    
    
    
    private function saveReservation($reservationJSON, $locale) {
        $em=$this->getDoctrine()->getManager();
        $reservation=new Reservation();

        //set language
        $language=$em->getRepository('AppBundle:Language')->findOneBy(array('locale'=>$locale));
        if (!$language) $em->getRepository('AppBundle:Language')->find(1);
        $reservation->setLanguage($language);
        
        $entitiesToRemove=array(); //won't be used, it is a new entity
        $newEntities=array();
        JsonHelper::updateFromDiffJSON($em,$reservation,$reservationJSON,$entitiesToRemove,$newEntities);
        $validator = $this->get('validator');
        $errors = $validator->validate($reservation);
        if (count($errors)>0) {
            $messages="<ul>";
            foreach($errors as $error) $messages.='<li>'.$error->getMessage().'</li>';
            throw new \Exception($messages);
        } 
        $em->getConnection()->beginTransaction(); 
        try {
            //Lock cacheCalendar from dateIn to dateEnd
            $em->getConnection()->query("SELECT * from cache_calendar where date between ".
                    $reservation->getDateInYMD()." AND ".$reservation->getDateEndYMD()." FOR UPDATE");
            //check reservation
            $eng=new SearchEngine($this->getDoctrine()->getManager());
            $eng->checkReservation($reservation);
            $em->persist($reservation);
            $em->flush(); //-->assign id from database
            CacheCalendarManager::addReservation($em, $reservation);
            $em->flush();
            $em->getConnection()->commit();
        } catch (\Exception $ex) {
            $em->getConnection()->rollback();
            throw new \Exception($ex->getMessage());
        }
        return $reservation;
    }
    
    
    //Does not remove the reservation, just mark as deleted (enabled = false) 
    private function removeReservation($reservation) {
        $em=$this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        CacheCalendarManager::removeReservation($em, $reservation);
        
        //foreach($reservation->getReservationServices() as $service) $em->remove($service);
        //$em->remove($reservation);
        
        $reservation->setEnabled(false);
        
        $em->persist($reservation);                
        $em->flush();
        $em->getConnection()->commit();
    }
    
            
            
         
    private function sendReservationMail($reservation) {                
        $templating=$this->get('templating');
        $txt=$templating->render('reservation\mail.html.twig',array('reservation' => $reservation));

        //send confirmation mail to user
        $message = \Swift_Message::newInstance()
            ->setSubject('Confirmació de Reserva')
            ->setFrom('nautic@cnps.cat')
            ->setTo($reservation->getUser()->getUsername())
            ->setBcc('nautic@cnps.cat')
            ->setBody(
                $txt,
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
                

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @Route("/json/checkuser", name="checkuser_json")
     * @Method({"POST"})
     */
    public function jsonCheckUserAction(Request $request)
    {              
        $form = json_decode($request->getContent(), true);
        
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository('AppBundle:User');
        $user=$repository->findUsername($form['email']);

        $response=false;
        if ($user && $user->getIsActive() && !$user->getDeleted()) {
            $encoder=$this->get('security.password_encoder');
            $password=$encoder->encodePassword($user, $form['password']);
            if ($password==$user->getPassword()) {
                $serializer = $this->get('serializer');
                $user->setPassword(null);
                $response=$serializer->normalize($user, null, array('groups' => 'form'));
            }
        }
        
        return new JsonResponse($response);
    }    
    
     /**
     * @Route("/json/checkmailconfirmation", name="check_mail_confirmation_json")
     * @Method({"POST"})
     */
    public function jsonCheckMailConfirmationAction(Request $request)
    {              
        $em=$this->getDoctrine()->getManager();
        $fields = json_decode($request->getContent(), true);
        
        $repository=$em->getRepository('AppBundle:User');
        $user=$repository->find($fields['id']);
        if ($user && $user->getMailConfirmationCode()==$fields['mailConfirmationCode']) {
            $user->setIsActive(true);
            $em->persist($user);
            $em->flush();
            return new JsonResponse(array('ok'=>true));
        }
        return new JsonResponse(array('ok'=>false));
    }    
    
    
    
    
        /**
     * @Route("/json/newuser", name="newuser_json")
     * @Method({"PUT"})
     */
    public function jsonNewUserAction(Request $request)
    {       
        $em=$this->getDoctrine()->getManager();
        $encoder=$this->get('security.password_encoder');
        $response=JsonHelper::_processCRUD($this->getDoctrine(), $this->get('serializer'), $this->get('validator'),
                $this->get('logger'), $this->getUser(), $request, -1, 
                'AppBundle:User', array('form'), 
                function($o) { 
                    $o->setPassword(null);                    
                }, null, 
                function($user) use ($encoder) {  
                    $newPassword=$user->getPassword();
                    if ($newPassword===null) $user->setPassword(null);
                    else $user->setPassword($encoder->encodePassword($user, $newPassword));                            
                });
        if ($response['ok']) { //donat d'alta correctament
            //send mail to user
            try {
                $repository=$em->getRepository('AppBundle:User');
                $user=$repository->find($response['id']);
                $user->setMailConfirmationCode(uniqid());
                $em->persist($user);
                $em->flush($user);
                if ($user) {
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Confirmació e-mail')
                        ->setFrom('hotels@bookingtrend.com')
                        ->setTo($user->getUsername())
                        ->setBody(
                            "Codi confirmació : ".$user->getMailConfirmationCode(),
                            'text/html'
                        );
                    $this->get('mailer')->send($message);
                }
            } catch (\Exception $ex) {}
        }

        return new JsonResponse($response);
    }
    
    
     /**
     * @Route("/json/passwordreset", name="passwordreset_json")
     * @Method({"POST"})
     */
    public function jsonPasswordResetAction(Request $request)
    {              
        $form = json_decode($request->getContent(), true);
        
        $em=$this->getDoctrine()->getManager();
        $repository=$em->getRepository('BookingTrendPDSBundle:User');
        $user=$repository->findLogin($form['email']);

        $response=false;
        if ($user) {
            try {
                $encoder=$this->get('security.password_encoder');
                $password=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
                $user->setPassword($encoder->encodePassword($user, $password));
                $user->setEnabled(true);

                //send mail to user
                $message = \Swift_Message::newInstance()
                    ->setSubject('Password reset')
                    ->setFrom('nautic@cnps.cat')
                    ->setTo($user->getLogin())
                    ->setBody(
                        "Nou password: ".$password,
                        'text/html'
                    );
                $this->get('mailer')->send($message);

                $em->persist($user);
                $em->flush();
                $response=true;
                
            } catch (\Exception $ex) {
                
            }
        }
        
        return new JsonResponse($response);
    }
    
    
    
    

    
   
    
}
