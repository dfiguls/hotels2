<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/admin/countries")
*/
class CountryController extends Controller
{    
    /**
     * @Route("/json/selectCountries", name="countriesselect_json")
     * @Method("GET")
     */
    public function jsonCountriesSelectAction(Request $request)
    {    
        $serializer = $this->get('serializer');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Country');
        $entities=$repository->findAll();
        $a=$serializer->normalize($entities, null, array('groups' => array('select')));
        return new JsonResponse($a);
    }
    
}
