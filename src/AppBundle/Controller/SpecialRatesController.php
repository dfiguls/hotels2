<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Services\JsonHelper;

/**
* @Route("/admin/specialrates")
*/
class SpecialRatesController extends Controller
{
    /**
     * @Route("/", name="specialrates")
     */
    public function indexAction()
    {
        $serializer = $this->get('serializer');
        $ruleSets=$this->getDoctrine()->getRepository('AppBundle:RuleSet')->findBy(array());
        $aRuleSets=$serializer->normalize($ruleSets, null, array('groups' => array('select')));
        return $this->render('specialRate/index.html.twig', array('ruleSets'=>json_encode($aRuleSets)));        
    }   
        
    
    /**
     * @Route("/json/specialrates", name="specialrates_json")
     * @Method({"GET","PUT"})
     */
    public function jsonSpecialRatesAction(Request $request)
    {        
        $serializer = $this->get('serializer');
        $dateIn= \DateTime::createFromFormat('!d-m-Y h:i', $request->get('dateIn').' 00:00');
        $dateEnd= \DateTime::createFromFormat('!d-m-Y h:i', $request->get('dateEnd').' 00:00');
        $idRuleSet=$request->get('idRuleSet');

        $ruleSet = $this->getDoctrine()->getRepository('AppBundle:RuleSet')->find($idRuleSet);

        $cacheCalendarRepository = $this->getDoctrine()->getRepository('AppBundle:CacheCalendar');
        $map=$cacheCalendarRepository->findByDates($dateIn, $dateEnd, $ruleSet);
        $normalized=$serializer->normalize($map, null, array('groups' => array('select')));
        
        if ($request->getMethod()=='GET') {            
            $parameters=$ruleSet->mergeRuleParameters();
            $aParameters=array();
            foreach($parameters as $parameter) {
                $param=$serializer->normalize($parameter['parameter'], null, array('groups' => array('select')));
                $param['options']=$serializer->normalize($parameter['options'],null,array('groups'=>array('select')));
                $aParameters[]=$param;
            }            
            return new JsonResponse(array('map'=>$normalized,'parameters'=>$aParameters));
        } else {
            $em=$this->getDoctrine()->getManager();
            $diff = json_decode($request->getContent(), true);
            $entitiesToRemove=array();
            $newEntities=array();
            for($i=0;$i<count($map);$i++) JsonHelper::updateFromDiffJSON($em,$map[$i],$diff[$i],$entitiesToRemove, $newEntities);
            foreach ($entitiesToRemove as $e) $em->remove($e);
            foreach ($map as $mapDay) $em->persist($mapDay);
            $em->flush();
            return new JsonResponse(array('ok'=>true));
        }
    }
    
    
    
           
}
