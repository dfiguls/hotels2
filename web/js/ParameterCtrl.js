app.controller('ParametersCtrl', function ($scope, $http, $uibModal) {
    $scope.datatable={
        pageLength: pageLength,            
        ajax: entitiesJson,
        order: [ 1, 'asc' ],
        columns: [{ "orderable": false},{},{},{"orderable": false}],
        del:function(entity, reload) {
            var id=entity?entity.id:-1;
            var $dlgClient = $uibModal.open({
                animation: true,
                templateUrl: 'dlg.html',
                controller:'DlgCtrl',
                size:'md',
                resolve: { title: function(){return 'Eliminar Paràmetre';}, message: function(){return 'Vols eliminar el paràmetre '+entity.name+' ?';}, }
            });
            $dlgClient.result.then(function () {
                    $http.delete(entityJson+'/'+id).then(function(){
                        reload(); //refresh datatable                    
                    });
                }, function () {      
                });
        },
        edit:function(entity, reload) {
            var $dlg = $uibModal.open({
                animation: true,
                templateUrl: 'parameter.html',
                controller: 'ParameterCtrl',
                size:'lg',
                resolve: { id: function(){return entity?entity.id:-1;} }
            });
            $dlg.result.then(function (c) {
                    reload();
                }, function () {      
                });
        }
    };      
});

app.controller('ParameterCtrl', function ($scope, $http, $uibModal, $uibModalInstance, id) {
    $http.get(entityJson+'/'+id).then(function(response) {
        $scope.languages=response.data.languages;        
        $scope.initial= response.data.parameter;
        jsonInit($scope.initial);
        $scope.entity=angular.copy($scope.initial);        
        
        //add missing languages in parameters
        var translations=[];
        var j=0;
        for (var i=0;i<response.data.languages.length;i++) {
            if (j<$scope.initial.translations.length && $scope.initial.translations[j].languageId===response.data.languages[i].id) {
                translations.push($scope.initial.translations[j]);
                j++;
            } else {
                translations.push({languageId:response.data.languages[i].id,description:''});
            }
        }
        $scope.entity.translations=translations;  
        
        //add missing languages in options
        for (var k=0;j<$scope.entity.options.length;k++) {
            var option=$scope.entity.options[k];
            var translations=[];
            var j=0;
            for (var i=0;i<response.data.languages.length;i++) {
                if (j<option.translations.length && option.translations[j].languageId===response.data.languages[i].id) {
                    translations.push(option.translations[j]);
                    j++;
                } else {
                    translations.push({languageId:response.data.languages[i].id,name:'',description:''});
                }
            }
            option.translations=translations;  
        }        
    });    

    $scope.delOption=function($index) {
        $scope.entity.options.splice($index,1);
    };
    
    $scope.editOption=function(index) {
        var $dlg = $uibModal.open({
            animation: true,
            templateUrl: 'option.html',
            controller: 'OptionCtrl',
            size:'lg',
            resolve: { parameter: function() {return $scope.entity; }, languages: function() {return $scope.languages;},
                entity: function(){return index>=0?$scope.entity.options[index]:{};} 
            }
        });
        $dlg.result.then(function (c) {
                if (index>=0) $scope.entity.options[index]=c;
                else $scope.entity.options.push(c);
            }, function () {      
        });
    };
    
    $scope.ok = function () {
        //set options order
        for (var i=0;i<$scope.entity.options.length;i++) {
            $scope.entity.options[i].order=i;
        }
        var diff=jsonDiff($scope.initial,$scope.entity);
        var data = angular.toJson(diff);
        $http.put(entityJson+'/'+id,data).then(function(response){
            if (response.data.ok) $uibModalInstance.close();            
            else {
                jsonAddErrors($scope.entity,response.data.errors);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };    
    
    $scope.normalizeFloat=function(field) {
        $scope.entity[field]=normalizeFloat($scope.entity[field]);
    };

});



app.controller('OptionCtrl', function ($scope, $uibModalInstance, parameter, languages, entity) {
    $scope.parameter=parameter;
    $scope.languages=languages;
    $scope.entity=angular.copy(entity);      
    
    $scope.addImage = function(img) {
        $scope.$apply($scope.entity.images.push({data:img, main:false}));
    };

    $scope.delImage=function(index) {
        $scope.entity.images.splice(index, 1);
    };
    
    $scope.imagePath=function(path,image) {
        if (image.data) return image.data;
        return path+'/'+image.name;
    };    

    $scope.ok = function () {
        $uibModalInstance.close($scope.entity);     
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };    
});




