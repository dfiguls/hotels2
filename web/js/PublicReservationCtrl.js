app.controller('PublicReservationCtrl', function ($scope, $http) {
    $scope.PG_RESERVATION=1;
    $scope.PG_ROOMTYPE=2;
    $scope.PG_ACCOMMODATION=3;
    $scope.PG_PAYMENTTYPE=4;
    $scope.PG_LOGIN=5;
    $scope.PG_CLIENTDATA=6;
    $scope.PG_CONFIRM_MAIL=7;
    $scope.PG_PAYMENT=8;
    $scope.PG_CONFIRM=9;
    
    $scope.dateOptions={
        startingDay: 1, 
        showWeeks: false,
    };

    
    $scope.colors=['#bff073','#0dc9f7','#bfbfbf','#f09b87','#ed1c24','#6085bc','#ecdfbd'];
    
    $scope.reservation={
        roomType:null,
        accommodation:null,
        paymentType:null,        
        paymentMethod:1, //transf=1, PayPal=2, TPV=3
    };
    $scope.selectedServices={};
    $scope.dateIn=new Date();
    $scope.dateEnd=new Date();
    $scope.dateEnd.setDate($scope.dateEnd.getDate()+1);

    $scope.email='';
    $scope.password='';
    
    $scope.countries=countries;
    
    $scope.user={id:-1, name:'',countryId:1};
    $scope.initial=angular.copy($scope.user);
    $scope.page=$scope.PG_RESERVATION;
    $scope.accept=false;   
    
    $scope.changeDateIn=function() {
        if ($scope.dateIn>$scope.dateEnd)
            $scope.dateEnd=$scope.dateIn;
    };
    
    $scope.changeDateEnd=function() {
        if ($scope.dateIn>$scope.dateEnd)
            $scope.dateIn=$scope.dateEnd;        
    };
    
    $scope.numDays=function() {
        return Math.round(($scope.dateEnd-$scope.dateIn)/(1000*60*60*24));
    };
        
    $scope.isPasswordValid=function() {
        return $scope.user?$scope.user.password==$scope.user.repeatPassword:true;
    };
    
    $scope.myValid=function() {        
        return $scope.dateIn<$scope.dateEnd;
    };
    
    $scope.setRoomType=function(roomType) {
        $scope.reservation.roomType=roomType;
        $scope.next();
    };
    
    $scope.setAccommodation=function(accommodation) {
        $scope.reservation.accommodation=accommodation;
        $scope.next();
    };
    
    $scope.setPaymentType=function(paymentType) {
        $scope.reservation.paymentType=paymentType;
        $scope.next();
    };
    
    $scope.next=function(n) {
        $scope.message='';
        if (n) $scope.page=n; 
        else {
            switch($scope.page) {
                case $scope.PG_RESERVATION: 
                    $scope.reservation.dateInYMD=dateToYMD($scope.dateIn);
                    $scope.reservation.dateEndYMD=dateToYMD($scope.dateEnd);
                    var data = JSON.stringify($scope.reservation);
                    $http.put(checkRoomTypesURL,data).then(function(response) {
                        if (response.data.ok) {
                            $scope.roomTypes=response.data.result;
                            $scope.page=$scope.PG_ROOMTYPE;
                        } else {
                            $scope.message=response.data.error;
                        }
                    }); 
                    break;
                case $scope.PG_ROOMTYPE: 
                    var data = JSON.stringify($scope.reservation);
                    $http.put(checkAccommodationsURL,data).then(function(response) {
                        if (response.data.ok) {
                            $scope.accommodations=response.data.result;
                            $scope.page=$scope.PG_ACCOMMODATION;
                        } else {
                            $scope.message=response.data.error;
                        }
                    }); 
                    break;
                case $scope.PG_ACCOMMODATION: 
                    var data = JSON.stringify($scope.reservation);
                    $http.put(checkPaymentMethodsURL,data).then(function(response) {
                        if (response.data.ok) {
                            $scope.paymentTypes=response.data.result;
                            $scope.page=$scope.PG_PAYMENTTYPE;
                        } else {
                            $scope.message=response.data.error;
                        }
                    }); 
                    break;                    
                case $scope.PG_PAYMENTTYPE: 
                    if ($scope.user.id!==-1) $scope.page=$scope.PG_PAYMENT; 
                    else $scope.page=$scope.PG_LOGIN; 
                    break;
                case $scope.PG_LOGIN: 
                    var data=JSON.stringify({email:$scope.email,password:$scope.password});
                    $http.post(checkUserURL,data).then(function(response) {                        
                        if (response.data) {
                            $scope.user=response.data;
                            $scope.page=$scope.PG_PAYMENT;
                        } else {
                            $scope.message=transUsuariPasswordIncorrectes;
                        }
                    });
                    break;
                case $scope.PG_CLIENTDATA: 
                    var diff=jsonDiff($scope.initial,$scope.user);
                    var data = JSON.stringify(diff);
                    $http.put(newUserURL,data).then(function(response){
                        if (response.data.ok) {
                            $scope.user.id=response.data.id;
                            $scope.message=transMailCodi;
                            $scope.page=$scope.PG_CONFIRM_MAIL;
                        }            
                        else jsonAddErrors($scope.user,response.data.errors);
                    });              
                    break;
                case $scope.PG_CONFIRM_MAIL:
                    $scope.message='';
                    var data = JSON.stringify({mailConfirmationCode: $scope.mailConfirmationCode, id:$scope.user.id});
                    $http.post(checkMailConfirmationURL,data).then(function(response){
                        if (response.data.ok) {
                            $scope.page=$scope.PG_PAYMENT;
                        }            
                        else $scope.message=transCodiErroni;
                    });     
                    break;
                case $scope.PG_PAYMENT: 
                    $scope.reservation.userId=$scope.user.id;
                    $scope.reservation.rate=$scope.reservation.paymentType.minRate;

                    var reservation=angular.copy($scope.reservation);
                    reservation.backup='';
                    
                    //promos
                    //for (var i=0;i<reservation.promos.length;i++) {
                    //    reservation.promos[i]={id:reservation.promos[i].id};
                    //}
                    
                    //services
                    //reservation.reservationServices=[];            
                    //for (var i=0;i<$scope.dispo.services.length;i++) {
                    //    var serviceId=$scope.dispo.services[i].id;                        
                    //    if ($scope.selectedServices[serviceId]) {
                    //        reservation.reservationServices.push({n:$scope.selectedServices[serviceId], serviceId:serviceId});
                    //    }
                    //}
                    
                    //conditions
                    //var conditions='';
                    //for (var i=0;i<reservation.conditions.length;i++) {
                    //    conditions+=(i+1)+' '+reservation.conditions[i].description+'\r\n';
                    //}
                    //reservation.conditions=conditions;
                    
                    //backup days and services
                    //reservation.backup=JSON.stringify(backupRates());
                    

                    $scope.reservationJSON = JSON.stringify(reservation);
                    $scope.page=$scope.PG_CONFIRM; 
                    break;
            }            
        }
    };
    
    $scope.prev=function() {
        switch($scope.page) {
            case $scope.PG_ROOMTYPE: $scope.page=$scope.PG_RESERVATION; break;
            case $scope.PG_ACCOMMODATION: $scope.page=$scope.PG_ROOMTYPE; break;
            case $scope.PG_PAYMENTTYPE: $scope.page=$scope.PG_ACCOMMODATION; break;
            case $scope.PG_LOGIN: $scope.page=$scope.PG_PAYMENTTYPE; break;
            case $scope.PG_CLIENTDATA: $scope.page=$scope.PG_LOGIN; break;
            case $scope.PG_PAYMENT: $scope.page=$scope.PG_PAYMENTTYPE; break;
            case $scope.PG_CONFIRM: $scope.page=$scope.PG_PAYMENT; break;
        }
    };
    
    

    
    $scope.passwordReset=function() {
        var data=JSON.stringify({email:$scope.email});
        $http.post(passwordResetURL,data).then(function(response) {
            $scope.user=response.data;
            if (response.data) {
                $scope.message=transMailNouPassword;
            } else {
                $scope.message=transMailIncorrecte;
            }
        });
    };
    
    var dayIndex;
    
    function prepareMonth(year, month, aDays, ages) {
        var names=transMonths;
        var days=[];
        var d=new Date(year,month,1);
        for (var i=0;i<(d.getDay()+6)%7;i++) days.push({date:null,rate:null,dispo:null});
        
        while (d.getMonth()===month) {
            if (dayIndex===-1) {
                var txt=two0(d.getDate())+'-'+two0(d.getMonth()+1);
                if (txt==aDays[0].date) dayIndex=0;
            }
            if (dayIndex===-1 || dayIndex>=aDays.length ) {
                days.push({date:d.getDate(), rate:null, dispo:null});
            } else if (aDays[dayIndex].options.length===0) {
                days.push({date:d.getDate(), rate:null, dispo:false}); //no dispo
                dayIndex++;
            } else { 
                var idPromo=aDays[dayIndex].options[0].ruleSetId;
                var index=0;
                //var index=-1;
                //for (var i=0;i<$scope.reservation.promos.length;i++) {
                //    if ($scope.reservation.promos[i].id===idPromo) index=i;
                //}
                var rates=aDays[dayIndex].options[0].rates;
                var rate=0;
                for (var i=0;i<rates.length;i++) if (ages[i].pax) rate+=rates[i]*ages[i].pax;
                days.push({date:d.getDate(), rate:rate, dispo:true, color: index});
                dayIndex++;
            }
            d.setDate(d.getDate()+1);
        }
        return {name:names[month],days:days};
    }
    
    function prepareMonths(dateIn, dateEnd, days, ages) {
        var months=[];
        var year=dateIn.getFullYear();
        var month=dateIn.getMonth();
        dayIndex=-1;
        while (year<dateEnd.getFullYear() || year===dateEnd.getFullYear() && month<=dateEnd.getMonth()) {
            months.push(prepareMonth(year,month,days,ages));
            month++;
            if (month>11) {
                month=0;
                year++;
            }
        }
        return months;
    }
    
    $scope.totalRate=function() {
        var rate=0;
        if ($scope.dispo) {
            rate=$scope.reservation.rate;
            /*for (var i=0;i<$scope.dispo.services.length;i++) {
                var service=$scope.dispo.services[i];
                if ($scope.selectedServices[service.id]>0)
                    rate+=$scope.selectedServices[service.id]*service.rate;
            }*/
        }
        return Math.round(rate*100)/100;
    };
    
    function backupRates() {
        var backup={days:[], services:[]};
        for (var i=0;i<$scope.dispo.days.length;i++) {
            var day=$scope.dispo.days[i];
            backup.days.push({date:day.date, ruleSetId:day.options[0].ruleSetId, rates:day.options[0].rates});
        }
        //for (var i=0;i<$scope.dispo.services.length;i++) {
        //        var service=$scope.dispo.services[i];
        //        if ($scope.selectedServices[service.id]>0)
        //            backup.services.push({name:service.description, rate:service.rate, n: $scope.selectedServices[service.id]}); 
        //}
        return backup;
    }
    
    $scope.YMDToDMY=function(ymd) {
        return YMDToDMY(ymd);
    };
});

