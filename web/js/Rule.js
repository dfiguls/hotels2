function Rule(rule) {
    this.calendars=[];
    this.options=[];
    this.combinations=[];
    
    var stretchs=[];    


    if (rule) {
        this.id=rule.id;
        this.ordering=rule.ordering;
        this.name=rule.name;
        this.enabled=rule.enabled;
        this.ruleSetId=rule.ruleSetId;
        this.sellDateInYMD=rule.sellDateInYMD;
        this.sellDateEndYMD=rule.sellDateEndYMD;
        this.__sellDateIn=YMDToDate(rule.sellDateInYMD);
        this.__sellDateEnd=YMDToDate(rule.sellDateEndYMD);
        this.minDays=rule.minDays;
        this.maxDays=rule.maxDays;
        this.releaseDays=rule.releaseDays;
        this.n=rule.n;
        for (var i=0;i<rule.calendars.length;i++) this.calendars.push(new Calendar(rule.calendars[i]));    
        this.options=rule.options;
        this.combinations=rule.combinations;
    } else {
        this.id=-1;
        this.type=1;
        this.enabled=true;
    }

    
    
   

    
       
    this.in=function(d) {
        var active=false;
        var isBlackDay=false;
        for (var i=0;i<this.calendars.length;i++) {
            if (this.calendars[i].in(d)) {
                active=true;
                if (this.calendars[i].isBlackDay) isBlackDay=true;
            }
        }
        return isBlackDay?false:active;
    };
    
    this.getStretchs=function() { return stretchs; };
    this.updateStretchs=function(periodeIn,periodeEnd) {
        stretchs=[];
        
        var cal=[];
        var d=new Date(periodeIn.getTime());
        while (d<periodeEnd) {            
            cal.push(this.in(d));
            d.setDate(d.getDate()+1);
        }
                    
        var current=cal[0];
        var iCurrent=0,nCurrent=0;
        var N=cal.length;
        for (var i=0;i<=N;i++) {
            if (i<N && cal[i]===current) nCurrent++;
            else {
                if (current) stretchs.push({left:iCurrent*100/N,width:nCurrent*100/N});
                iCurrent=i;
                nCurrent=1;
                current=cal[i];
            }
        }        
    };
    
    this.update=function() {
        for (var i=0;i<this.calendars.length;i++) this.calendars[i].update();
        this.sellDateInYMD=dateToYMD(this.__sellDateIn);
        this.sellDateEndYMD=dateToYMD(this.__sellDateEnd);
    };
       
}

function Calendar(cal) {
    function getDaysArray(mask) { 
        var a=[false,false,false,false,false,false,false];
        var i=0;
        while (mask>0) {
            if (mask & 1) a[i]=true;
            mask=mask>>1;
            i++;
        }
        return a; 
    };
    this.getDaysArray=function() { return aDays; };
    this.updateDaysArray=function() {
        var days=0;
        var mask=1;
        for (var i=0;i<aDays.length;i++) {
            if (aDays[i]) days+=mask;
            mask=mask<<1;
        }
        this.maskDays=days;
    };
    
    this.update=function() {
        this.dateInYMD=dateToYMD(this.__dateIn);
        this.dateEndYMD=dateToYMD(this.__dateEnd);
        this.updateDaysArray();
        this.refresh();
    };
            
    var aDays;
    this.refresh=function() {
        aDays=getDaysArray(this.maskDays);
    };
    
    this.in=function(day) {
      return this.__dateIn<=day && day<this.__dateEnd && aDays[(day.getDay()+6)%7];
    };
    
    if (cal) {
        this.id=cal.id;
        this.dateInYMD=cal.dateInYMD;
        this.dateEndYMD=cal.dateEndYMD;
        this.__dateIn=YMDToDate(cal.dateInYMD);
        this.__dateEnd=YMDToDate(cal.dateEndYMD);
        this.maskDays=cal.maskDays;
        this.isBlackDay=cal.isBlackDay;
    } else {
        //this.id=-1;
        this.__dateIn=new Date();
        this.__dateEnd=new Date();
        this.maskDays=127;
        this.isBlackDay=false;
    }
    this.refresh();
}

 

Calendar.prepareMonths=function(periodeIn,periodeEnd) {
    var months=[];
    var names=['gen','feb','mar','abr','mai','jun','jul','ago','set','oct','nov','des'];
    var N = Calendar.daysBetween(periodeIn, periodeEnd);
    
    var d=new Date(periodeIn.getTime());
    var month=periodeIn.getMonth();
    var iMonth=0, nMonth=0;
    while (d<=periodeEnd) {
        if (d<periodeEnd && d.getMonth()===month) nMonth++;
        else {
            months.push({left:iMonth*100/N,width:nMonth*100/N,name:names[month]});
            iMonth+=nMonth;
            nMonth=1;
            month=d.getMonth();
        }
        d.setDate(d.getDate()+1);
    }
    return months;
};

Calendar.daysBetween=function(a,b) {
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    return Math.round(Math.abs((b.getTime() - a.getTime())/(oneDay))) ;  
};

