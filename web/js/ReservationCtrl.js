app.controller('ReservationsCtrl', function ($scope, $http, $uibModal) {
    $scope.datatable={
        pageLength: pageLength,            
        ajax: entitiesJson,
        order: [ 1, 'asc' ],
        columns: [{ "orderable": false},{},{},{"orderable": false}],
        del:function(entity, reload) {
            var id=entity?entity.id:-1;
            var $dlgClient = $uibModal.open({
                animation: true,
                templateUrl: 'dlg.html',
                controller:'DlgCtrl',
                size:'md',
                resolve: { title: function(){return 'Eliminar Reserva';}, message: function(){return 'Vols eliminar la reserva '+entity.id+' ?';}, }
            });
            $dlgClient.result.then(function () {
                    $http.delete(entityJson+'/'+id).then(function(){
                        reload(); //refresh datatable                    
                    });
                }, function () {      
                });
        },
        edit:canWrite?function(entity, reload) {
            var $dlg = $uibModal.open({
                animation: true,
                templateUrl: 'reservation.html',
                controller: 'ReservationCtrl',
                size:'md',
                resolve: { id: function(){return entity?entity.id:-1;} }
            });
            $dlg.result.then(function (c) {
                    reload();
                }, function () {      
                });
        }:false,
        view:function(entity) {
            var $dlgClient = $uibModal.open({
                animation: true,
                templateUrl: 'viewReservationDlg.html',
                controller: 'ViewReservationCtrl',
                size:'md',
                resolve: { id: function(){return entity.id;} }
            });
            $dlgClient.result.then(function (c) {                
                }, function () {      
                });
        },
        viewCalendar:function(entity) {
            var $dlgClient = $uibModal.open({
                animation: true,
                templateUrl: 'viewReservationCalendarDlg.html',
                controller: 'ViewReservationCalendarCtrl',
                size:'md',
                resolve: { id: function(){return entity.id;} }
            });
            $dlgClient.result.then(function (c) {                
                }, function () {      
                });
        },
        pdf:function(entity) {
            var win = window.open(pdfReservation+'/'+entity.id, '_blank');
            win.focus();
            //window.location=pdfReservation+'/'+id;        
        }
    };          
});

app.controller('ReservationCtrl', function ($scope, $http, $uibModalInstance, id) {
    $http.get(clientsSelect).then(function(response) {
        $scope.clients=response.data;         
    });
    
    $http.get(promosSelect).then(function(response) {
        response.data.push({id:null,name:''});
        $scope.promos=response.data;         
    });
        
    $http.get(boatTypesSelect).then(function(response) {
        $scope.boatTypes=response.data;         
    });
        
    $http.get(moorageTypesSelect).then(function(response) {
        $scope.moorageTypes=response.data;         
    });
    
    $http.get(countriesSelect).then(function(response) {
        $scope.countries=response.data;
    });
        
    $http.get(entityJson+'/'+id).then(function(response) {
            jsonInit(response.data);
            $scope.entity= response.data;
            $scope.initial=angular.copy($scope.entity);
        });  
    
    $scope.ok = function () {
        var diff=jsonDiff($scope.initial,$scope.entity);
        var data = JSON.stringify(diff);
        $http.put(entityJson+'/'+id,data).then(function(response){
            if (response.data.ok) $uibModalInstance.close();            
            else {
                jsonAddErrors($scope.entity,response.data.errors);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };    
    
    $scope.dateInOpened=false;
    $scope.dateInOpen=function() { $scope.dateInOpened=true; };
    $scope.dateEndOpened=false;
    $scope.dateEndOpen=function() { $scope.dateEndOpened=true; };
    
});


app.controller('ViewReservationCtrl', function ($scope, $sce, $http, id) {
    $http.get(viewReservation+'/'+id).then(function(response) {
        $scope.reservation=response.data;       
    });        
    
    $scope.getTxt = function() {
        return $sce.trustAsHtml($scope.reservation);
    };
});


app.controller('ViewReservationCalendarCtrl', function ($scope, $sce, $http, id) {
    $http.get(entityJson+'/'+id).then(function(response) {
        $scope.reservation=response.data;   
        $scope.backup=JSON.parse($scope.reservation.backup);
        $scope.months=prepareMonths($scope.reservation, $scope.backup);
    });        
    
    $scope.colors=['#bff073','#0dc9f7','#bfbfbf','#f09b87','#ed1c24','#6085bc','#ecdfbd'];


    function prepareMonth(year,month,aDays,ages) {
        var names=transMonths;
        var days=[];
        var d=new Date(year,month,1);
        for (var i=0;i<(d.getDay()+6)%7;i++) days.push({date:null,rate:null,dispo:null});
        
        while (d.getMonth()===month) {
            if (dayIndex===-1) {
                var txt=two0(d.getDate())+'-'+two0(d.getMonth()+1);
                if (txt==aDays[0].date) dayIndex=0;
            }
            if (dayIndex===-1 || dayIndex>=aDays.length ) {
                days.push({date:d.getDate(), rate:null, dispo:null});
            } else { 
                //var idPromo=aDays[dayIndex].ruleId;
                //var index=-1;
                //for (var i=0;i<$scope.reservation.promos.length;i++) {
                //    if ($scope.reservation.promos[i].id===idPromo) index=i;
                //}
                var index=0;
                var rates=aDays[dayIndex].rates;
                var rate=0;
                for (var i=0;i<rates.length;i++) if (ages[i].pax) rate+=rates[i]*ages[i].pax;
                days.push({date:d.getDate(), rate:rate, dispo:true, color: index});
                dayIndex++;
            }
            d.setDate(d.getDate()+1);
        }
        return {name:names[month],days:days};
    }
    
    function prepareMonths(reservation, backup) {
        var months=[];
        var dateIn=YMDToDate(reservation.dateInYMD);
        var dateEnd=YMDToDate(reservation.dateEndYMD);
        var year=dateIn.getFullYear();
        var month=dateIn.getMonth();
        var ages=[reservation.reservationOptions[3],reservation.reservationOptions[4]];
        dayIndex=-1;
        while (year<dateEnd.getFullYear() || year===dateEnd.getFullYear() && month<=dateEnd.getMonth()) {
            months.push(prepareMonth(year,month,backup.days,ages));
            month++;
            if (month>11) {
                month=0;
                year++;
            }
        }
        return months;
    }
});

