var pageLength=25;


var app = angular.module('hotels', ['ngAnimate','ngSanitize','ui.bootstrap','ui.select'])
  .config(['$compileProvider', function( $compileProvider ) {
          //needed to show html5 file data in ng-src (image upload)
          $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob):|data:)/);
        }
    ]);

app.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.directive('mytable', function($compile,$http) {
    function compileAndAdd($scope, $element, $html) {
        var comp=$compile($html);
        var dom=comp($scope);
        $element.append(dom);
        return dom;
    }
    
    function getParams(scope) {
        var q=scope.mytable.getSearch?scope.mytable.getSearch(scope.mytable.search):{sql:'',params:[]};
        var s='?order[0][column]='+scope.mytable.order[0]+
            '&order[0][dir]='+scope.mytable.order[1]+
            '&start='+(scope.currentPage-1)*scope.mytable.pageLength+
            '&length='+scope.mytable.pageLength+'&search[value]='+scope.searchValue+
            '&search[custom]='+q.sql;
        for (var i=0;i<q.params.length;i++)
            s+='&'+q.params[i].name+'='+q.params[i].value;
        return s;
    }
    
    function loadData(scope) {
        var nextElements = [];
        $http.get(scope.mytable.ajax+getParams(scope)).then(function(result){
            scope.data=result.data; //{length,rows}
            var collection = result.data.rows;
            var i;
            for (i = 0; i < collection.length; i++) (function(){
                var element=collection[i];
                // create a new scope for every element in the collection.
                var childScope = scope.$new();
                // pass the current element of the collection into that scope
                childScope.$element = element;
                childScope.action=function(target) { scope.action(target,element); };
                if (i<scope.currentElements.length) {
                    scope.currentElements[i].el.remove();
                    scope.currentElements[i].scope.$destroy();
                }
                nextElements.push({scope:childScope, el:compileAndAdd(childScope, scope.$tbody, scope.$template.clone())});
            })();
            for (var j=i;j<scope.currentElements.length;j++) {
                scope.currentElements[j].el.remove();
                scope.currentElements[j].scope.$destroy();
            }
            scope.currentElements=nextElements;
        });
    }
    
    return {
        scope: {mytable: '=mytable'},
        link: function($scope, $element, $attr, controller, transclude) {
            var $table=$element.find('table');
            var $tbody=$table.find('tbody');
            var $search=angular.element($element[0].querySelector('#search'));
            var $template=$tbody.find('tr').clone();
            $scope.$tbody=$tbody;
            $scope.$template=$template;
            $scope.mytable.reload=function(){loadData($scope);};
            $scope.currentElements=[];

            $tbody.empty();

            $scope.data={length:0, rows:[]};
            $scope.currentPage=1;
                        
            $scope.pageChange=function() {
                loadData($scope);
            };
            $scope.action=function(target,element) {
                $scope.mytable[target](element, function(){ loadData($scope);});
            };
            $scope.print=function() {
                window.open($scope.mytable.print+getParams($scope),'_blank');
            };
            $scope.searchValue='';
            $scope.search=function() {
                loadData($scope);
            };
            $scope.toggleSearch=function() {
                if ($search) {
                    $search.toggleClass('hidden');
                    $scope.mytable.search.visible=!$scope.mytable.search.visible;
                }                
            };
            //add sorting buttons
            var $ths=$table.find('th');
            for (var i=0;i<$scope.mytable.columns.length;i++) (function(){
                var col=$scope.mytable.columns[i];
                var ii=i;
                if (col.orderable!==false) {
                    var e=angular.element($ths[i]);
                    e.addClass('sorting');
                    e.on('click',function(){
                        //clear previous ordering
                        var prev=angular.element($ths[$scope.mytable.order[0]]);
                        if (prev[0]!==e[0]) { 
                            prev.removeClass('sorting-asc').removeClass('sorting-desc');
                            $scope.mytable.order[0]=ii;                        
                            $scope.mytable.order[1]='asc'; 
                            e.addClass('sorting-asc');
                        } else {
                            if (e.hasClass('sorting-asc')) { 
                                $scope.mytable.order[1]='desc'; e.removeClass('sorting-asc').addClass('sorting-desc'); 
                            } else if (e.hasClass('sorting-desc')) { 
                                $scope.mytable.order[1]='asc'; e.removeClass('sorting-desc').addClass('sorting-asc'); 
                            }
                        }
                        loadData($scope);
                    });
                }
            })();
            angular.element($ths[$scope.mytable.order[0]]).addClass($scope.mytable.order[1]?'sorting-asc':'sorting-desc');
            

            //add pagination
            compileAndAdd($scope, $element,'<ul uib-pagination total-items="data.length" items-per-page="mytable.pageLength" '+
                'ng-model="currentPage" ng-change="pageChange()" max-size="5" class="pagination-sm pull-left" '+
                'boundary-links="true" force-ellipses="true" previous-text="&lsaquo;" '+
                'next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;" ></ul>');
                
            //add search input  
            compileAndAdd($scope, $element, '<div class="col-sm-3 pull-right">'+
                ($scope.mytable.search?'<span ng-click="toggleSearch();"  style="position:absolute;margin-left:-50px;font-family:\'FontAwesome\';vertical-align:-10px; font-size:20px;cursor:pointer;">{[{mytable.search.visible?"&#xf056;":"&#xf055;"}]}</span>':'')+
                ($scope.mytable.print?'<span ng-click="print();"  style="position:absolute;margin-left:-25px;font-family:\'FontAwesome\';vertical-align:-10px; font-size:20px;cursor:pointer;">&#xf02f;</span>':'')+
                '<input type="text" class="form-control" placeholder="buscar..." ng-model="searchValue" ng-model-options="{ debounce: 1000 }" ng-change="search()"/>'+
                '</div>'
            );

            //add new item button
            if ($scope.mytable.edit) {
                compileAndAdd($scope, $element, '<div class="text-center">'+
                '<span ng-click="action(\'edit\',null);"  style="font-family:\'FontAwesome\';vertical-align:-10px; font-size:40px;cursor:pointer;">&#xf055;</span>'+
                '</div>');   
            }
        
            //hide search
            if ($scope.mytable.search) {
                $scope.mytable.search.visible=false;
                $search.addClass('hidden');
            }
            
            //load first page
            loadData($scope);
        }
    };
});


app.directive('selectColor', function() {    
    return {
        scope: {enable: '=selectColor',
            colors:'=colors',
            onChange:'&colorChange'
        },
        link: function($scope, $element, $attr, controller, transclude) {
            if ($scope.enable) prepare($scope, $element);
            $scope.$watch(function(scope) { return scope.enable; },
                function(newValue, oldValue) {
                  if (oldValue===false && newValue===true) prepare($scope, $element);
                }
            );
        }
    };
    
    function prepare($scope, $element) {
        var divColors=angular.element('<div style="position:absolute;width:90px;"></div>');
        for (var i=0;i<$scope.colors.length;i++) {
            var color=angular.element('<div color="'+$scope.colors[i]+'" class="color" style="background-color:'+$scope.colors[i]+';"></div>');
            color.on('click',function(event){
                event.preventDefault();
                event.stopPropagation();
                var color=angular.element(this).attr('color');
                $scope.onChange({color:color});
                divColors.detach();
            });
            divColors.append(color);
        };                                
        $element.on('contextmenu',function(event){
            event.preventDefault();
            return false;
        });

        $element.on('mousedown',function(event){
            if( event.button === 2 ) { 
                $element.append(divColors);
                event.preventDefault();
            }
        });        
    }
});


app.directive('contextualMenu', function($compile) {   
    return {
        scope: {contextualMenu: '=contextualMenu'
        },
        link: function($scope, $element, $attr, controller, transclude) {
            $element.on('contextmenu',function(event){
                event.preventDefault();
                return false;
            });

            var $menu;
            $scope.$parent.$close=function() {
                if ($menu) $menu.remove();
                $menu=null;
            };
            $element.on('mousedown',function(event){
                if( event.button === 2 ) { 
                    $scope.$parent.$contextualevent=event;
                    $scope.$apply(function() {
                        var template=angular.element(document.getElementById($attr.contextualMenu)).html();
                        var dom=$compile(template)($scope.$parent);
                        $menu=angular.element('<div class="bicycleDayMenu"></div>');
                        $menu.append(dom);
                        $element.prepend($menu);
                    });
                    event.preventDefault();  
                    var entered=false;
                    $menu.on('mouseenter',function(event){ entered=true; });
                    $menu.on('mouseleave',function(event){ if (entered) $scope.$parent.$close(); });
                }
            });      
        }
    };
});


app.controller('DlgCtrl', function ($scope, $uibModalInstance, title, message) {
    $scope.title=title;
    $scope.message=message;
    
    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };    
    
});



app.directive("imagedrop", function ($parse,$document,$compile) {
    return {
        restrict: "A",
        scope: {imagedrop: '=imagedrop'},
        link: function (scope, element, attrs) {
            $document.bind("dragover", function(e){e.preventDefault();});
            
            element.bind('dragover',function(e){
                element.addClass('dragover');
            });
            element.bind('dragleave',function(e){
                element.removeClass('dragover');
            });
            
            //var target = $parse(attrs.imagedrop);
            element.bind("drop", function (e) {
                e.preventDefault();
                if (e.dataTransfer.files.length>0) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        scope.imagedrop(event.target.result);
                    };
                    reader.readAsDataURL(e.dataTransfer.files[0]);
                } else {
                    var str = e.dataTransfer.getData('text/html');
                    var html=$compile(str);
                    var elem=html(scope);
                    var url=elem.attr('src');
                    if (!url) {
                        var img=elem.find('img');
                        if (img.length>0) url=img.attr('src');
                    }
                    if (!url) {
                        url=e.dataTransfer.getData('text/plain');                        
                    }
                    if (url) scope.imagedrop(url);
                }
                element.removeClass('dragover');                
                //e.stopPropagation(); 
            });
        }
    };
}).directive("labeldrag", function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var e=$(element);
            e.draggable();            
        }
    };
});


app.directive('mycurrency', function () {
    var FLOAT_REGEXP = /^\-?\d+(\.\d+)?$/;
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl){
            ctrl.char='€';
            if (attrs.mycurrency) ctrl.char=attrs.mycurrency;
            var focused=false; //to avoid run parsers on form submit
            ctrl.$formatters.unshift(function(val){    
                if (val!==null && val!==undefined && val!=='') {
                    if (FLOAT_REGEXP.test(val)) return '' + val +ctrl.char;
                    return val;
                }
                return '--';
            });
            ctrl.$parsers.unshift(function(val){   
                if (focused) {
                    var value=val.replace(',','.').replace(ctrl.char, '');
                    if (value=='') {
                        ctrl.$setValidity('float', true);
                        value=null; //set null to the model
                    } else if (FLOAT_REGEXP.test(value)) {
                        ctrl.$setValidity('float', true);
                    } else {
                        ctrl.$setValidity('float', false);
                    }                

                    //update view if necessary
                    if (value!==val) {
                        ctrl.$viewValue = value;
                        ctrl.$render();   
                    }
                    return value;
                }
                return val;
            });
            element.on('focus', function() {
                focused=true;
                element[0].select();
            });

            element.on('blur', function() {
                focused=false;
                var viewValue = ctrl.$modelValue;
                for (var i in ctrl.$formatters) {
                    viewValue = ctrl.$formatters[i](viewValue);
                }
                ctrl.$viewValue = viewValue;
                ctrl.$render();           
            });
        }
    };
});

app.directive('validateFloat', function () {
    
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    
    return {
        require: "ngModel",
        link: function(scope, elm, attrs, ngModelController){

            ngModelController.$parsers.unshift(function(viewValue) {
                if (viewValue == '') {
                    ngModelController.$setValidity('float',  true);
                    return viewValue;
                }
                
                if (FLOAT_REGEXP.test(viewValue)) {
                    ngModelController.$setValidity('float', true);
                    var value=parseFloat(viewValue.replace(',', '.'));
                    if (attrs.min) ngModelController.$setValidity('min',value>=attrs.min);
                    if (attrs.max) ngModelController.$setValidity('max',value<=attrs.max);
                    return value;
                } 
                
                ngModelController.$setValidity('float', false);
                return undefined;
            });
        }
    };
});



app.directive("contenteditable", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        ngModel.$setViewValue(element.html());
      }

      ngModel.$render = function() {
        element.html(ngModel.$viewValue || "");
      };

      element.bind("blur keyup change", function() {
        scope.$apply(read);
      });
    }
  };
});









//returns a new temporary id for newly created entities
var tmpIds={};
function newTmpId(space) {
    if (!tmpIds[space]) tmpIds[space]=-1;
    return tmpIds[space]--;
}
function resetTmpIds() {
    tmpIds={};
}

//save initial position of each collection (1-N relations)
function jsonInit(initial) {
    if (initial && initial.constructor ===Array) {
        for (var i=0;i<initial.length;i++) {
            jsonInit(initial[i]);
            initial[i].__row=i;
        }
    } else {
        for (var property in initial) {
            if (initial.hasOwnProperty(property)) {
                var value=initial[property];                
                if (value && value.constructor === Array) jsonInit(value);
            }
        }
    }
}

function jsonDiff(initial,final) {
    if (final && final.constructor ===Array) {
        var diff=[];
        for (var i=0;i<final.length;i++) {
            var __row=final[i]?final[i].__row:undefined; //initial position of this row
            if (__row!==undefined) diff.push(jsonDiff(initial?initial[__row]:{},final[i]));
            else diff.push(jsonDiff({},final[i])); //new record
        }
    } else {
        var diff={};
        for (var property in final) {
            if (final.hasOwnProperty(property)) {
                var finalValue=final[property];
                var initialValue=initial?initial[property]:null;
                
                if (property.indexOf('__error')!==-1) {
                    //do nothing with errors
                } else if (property.indexOf('$$')===0) {
                    //do nothing with angular fields                
                } else if (property.indexOf('__')===0 && property!=='__row') {         
                    //do nothing with temporal fields 
                } else if (finalValue && finalValue.constructor === Object) { //N-1, 1-1
                    diff[property]=jsonDiff(initialValue, finalValue);
                } else if (finalValue && finalValue.constructor === Array) {//1-N or N-M
                    var a=diff[property]=jsonDiff(initialValue,finalValue);
                } else if (finalValue && finalValue.constructor === Function) {
                    //do nothing with methods
                } else if (finalValue && finalValue.constructor === Date) {
                    finalValue=dateToYMD(finalValue);
                    if (finalValue!==initialValue) diff[property]=finalValue;

                } else if (property==='__row' || finalValue!==initialValue) {
                    //add __row fields, and modified fields
                    diff[property]=finalValue;
                }
            }
        }
    }
    return diff;
}


function jsonClearErrors(entity) {
    //clean previous errors
    for (var property in entity) {
        if (entity.hasOwnProperty(property)) {
            if (property.indexOf('__error')!==-1) entity[property]=null;
            else {
                var o=entity[property];
                if (o && o.constructor === Array) {
                    for(var i=0;i<o.length;i++) jsonClearErrors(o[i]);
                }
            }
        }
    }
}


function jsonAddErrors(entity,errors) {
    jsonClearErrors(entity);
    //add new errors
    for (var i=0;i<errors.length;i++) {
        var path=errors[i].path;
        var parts=path.split('.');
        var j=0;
        var o=entity;
        while (j<parts.length-1) {
            var part=parts[j];
            var pos=part.indexOf('[');
            if (pos>0) {
                var field=part.substr(0,pos);
                var index=part.substr(pos+1,part.length-pos-2);
                o=o[field][index];
            }
            j++;
        }
        if (!o['__error'+parts[j]]) {
            o['__error'+parts[j]]=errors[i].message;
        }
    }    
}

function two0(num) { return ('0'+num).slice(-2); }

function dateToYMD(date) {
    if (!date) return null;
    var ymd = date.getFullYear()+"-"+ 
            ("0"+(date.getMonth()+1)).slice(-2) + "-" + 
            ("0" + date.getDate()).slice(-2);
    return ymd;
}

function dateToDMY(date) {
    if (date) {
        var dmy = ("0" + date.getDate()).slice(-2)+"-"+
                ("0"+(date.getMonth()+1)).slice(-2) + "-" + 
                date.getFullYear();
        return dmy;
    }
    return null;
}

function YMDToDate(ymd) {
    return ymd?new Date(ymd.substr(0,4), ymd.substr(5,2)-1, ymd.substr(8,2), 12,0,0,0):null;
}

function YMDToDMY(ymd) {
    return dateToDMY(YMDToDate(ymd));
}

function dateToMMDD(date) {
    var md = ("0"+(date.getMonth()+1)).slice(-2)+ ("0" + date.getDate()).slice(-2);
    return md;
}

function DDMMtoMMDD(dm) {
    var md=null;
    dm=dm.replace('.','-');
    dm=dm.replace('/','-');
    var a=dm.split('-');
    if (a.length===2) 
        md=("00"+a[1]).slice(-2)+ ("00" + a[0]).slice(-2);
    return md;    
}

function toInt(s) {
    var i=parseInt(s);
    return isNaN(i)?null:i;
}

function toFloat(s) {
    var f=parseFloat(normalizeFloat(s));
    return isNaN(f)?null:f;
}

function to2Decimals(v) {
    return Math.round(v*100)/100;
}

function normalizeFloat(f) {    
    if (f!==null && (typeof f === 'string' || f instanceof String)) f=f.replace(',','.');
    return f;
}



function toAssocArray(a,id) {
    var r={};
    for(var i=0;i<a.length;i++)
        r[a[i][id]]=a[i];
    return r;
}


function prepareMultiSelect(items, o, target, find) {
    var a=o[target]=[];
    for (var i=0;i<items.length;i++) {
        var obj=find(items[i]);
        if (obj) a.push(obj);
    }
}

function finishMultiSelect(items, o, target, id, notify) {
    var n=[];
    var a=o[target];
    var ids=angular.copy(items); //keep ui dependendent controls with the same value
    if (a) for (var i=0;i<a.length;i++) {
        var found=null;
        for (var j=0;j<ids.length;j++)
            if (a[i][id]===ids[j].id) { if (notify) notify(a[i],ids[j]); found=a[i]; ids.splice(j,1); break; }
        if (found) n.push(found);
    }
    if (ids) for (var i=0;i<ids.length; i++) {
        var no={};
        no[id]=ids[i].id;
        if (notify) notify(no,ids[i]);
        n.push(no);
    }
    o[target]=n;
}





var DimensionIterator=function(options) {
    var dimensions=[];
    var l=options.length;
    for (var i=0;i<options.length;i++) {
        var o=options[i];
        var dimension=find(dimensions,o.parameterId);
        if (!dimension) {dimension={id:o.parameterId, options:[]}; dimensions.push(dimension); }
        dimension.options.push({id:o.id, name:o.name});
    }
    if (dimensions.length===0) dimensions.push({id:null, options:[{id:null,name:''}]});

    var N=1;
    for (var i=0;i<dimensions.length;i++) N*=dimensions[i].options.length;

    var _N=N;
    var repeat=1;
    var table=[];
    for (var i=0;i<dimensions.length;i++) {
        var dimension=dimensions[i];
        var n=dimension.options.length;
        var span=dimension.span=_N/n;
        var row=[];
        for (var j=0;j<repeat;j++) {
            for (var k=0;k<n;k++) {
                row.push({name:dimension.options[k].name,span:span});
            }
        }
        table.push(row);
        repeat*=n;
        _N/=n;
    }
    
    this.getN=function() {
        return N;
    };

    this.getColumns=function() {
        return table;
    };
    
    this.getRows=function(sort) {
        var rows=[];
        var combinationIds=[];
        for (var i=0;i<dimensions.length;i++) combinationIds.push(-1);
        
        for (var i=0;i<N;i++) {
            var subitems=[];
            for (var j=0;j<dimensions.length;j++) {
                var dimension=dimensions[j];
                if (i%dimension.span===0) {
                    var nOption=(i/dimension.span)%dimension.options.length;
                    subitems.push({name:dimension.options[nOption].name, 
                        id:dimension.options[nOption].id, span:dimension.span});
                    combinationIds[j]=dimension.options[nOption].id;
                }
            }
            rows.push({subitems:subitems, combinations:[], combinationIds:combinationIds.slice().sort(function (a,b) {return a - b;}).join()});
        }
        return {dimensions:dimensions.length, items:rows};
    };

    this.begin=function() {
        this.current=[];
        for (var i=0;i<dimensions.length;i++) this.current.push(0);
        if (dimensions.length>0) this.current[dimensions.length-1]=-1; //the next will be the first
    };

    this.next=function() {
        var i=dimensions.length-1;
        var done=false, end=false;
        while (!done && !end) {
            if (i>=0) {
                this.current[i]++;
                if (this.current[i]>=dimensions[i].options.length) {
                    this.current[i]=0;
                    i--;                
                } else done=true;
            } else {
                end=true;
            }
        }
        return !end;
    };        

    this.getOptionIds=function() {
        var a=[];
        for (var i=0;i<this.current.length;i++) {
            var optionId=dimensions[i].options[this.current[i]].id
            if (optionId) a.push({id:optionId});
        }
        return a;
    };

    this.findOptionDimension=function(id) {
        for (var i=0;i<dimensions.length;i++) {
            var d=dimensions[i];
            for (var j=0;j<d.options.length;j++)
                if (d.options[j].id===id) return {dimension:d, index:j};
        }
        return null;
    };

    this.getIndex=function(optionIds) {
        //if has no dimensions (just a dummy one)
        if (dimensions[0].id===null) return {index:0, matches:1};
        
        //if has at least one dimension
        var index=0, matches=0;
        for (var i=0;i<optionIds.length;i++) {
            var d=this.findOptionDimension(optionIds[i].id);
            if (d) {
                index+=d.dimension.span*d.index;
                matches++;
            }
        }        
        return {index:index, matches:matches};
    };
};        

function find(levels,id) {
    for (var i=0;i<levels.length;i++) {
        if (levels[i].id===id) return levels[i];
    }
    return null;
}






//=============================================================================
