app.controller('RulesCtrl', function ($scope, $http, $filter, $uibModal,$log) {
    $scope.colors=['#1010ff','#9090ff','#c0c0ff','#e0e0ff', '#c0ffc0','#90ff90'];
     var dateFilter = $filter('date');
    $scope.periodeIn=new Date();
    $scope.periodeIn.setDate(1);
    $scope.periodeIn.setMonth(0);
    
    $scope.periodeEnd=new Date();
    $scope.periodeEnd.setDate(31);
    $scope.periodeEnd.setMonth(11);
    
    $scope.periodeInOpened=false;
    $scope.periodeInOpen=function() { $scope.periodeInOpened=true; };
    $scope.periodeEndOpened=false;
    $scope.periodeEndOpen=function() { $scope.periodeEndOpened=true; };
    
    var nDays;
    var parameters=[];
    var options=[];
            
    $scope.update=function() {
        $http.get(entitiesJson+"?dateIn="+dateFilter($scope.periodeIn,'dd-MM-yyyy')
                   +"&dateEnd="+dateFilter($scope.periodeEnd,'dd-MM-yyyy')).then(function(response) {
            $scope.rules=[];
            for (var i=0;i<response.data.rules.length;i++) 
                $scope.rules.push(new Rule(response.data.rules[i]));
            nDays=Calendar.daysBetween($scope.periodeIn,$scope.periodeEnd);
            $scope.monthColors=['#f0f0ff','#fafaff'];
            $scope.months=Calendar.prepareMonths($scope.periodeIn,$scope.periodeEnd);
            for (var i=0;i<$scope.rules.length;i++) 
                $scope.rules[i].updateStretchs($scope.periodeIn,$scope.periodeEnd);

            //parameters & options           
            parameters=response.data.parameters;
            options=response.data.options;
            ruleSets=response.data.ruleSets;
            for (var i=0;i<parameters.length;i++) parameters[i].options=[];
            for (var i=0;i<options.length;i++) {
                var p=findParameter(options[i].parameterId);
                if (p) {
                    options[i].parameter=p;
                    p.options.push(options[i]);
                }
            }
        }); 
    };
    $scope.update();
         
    function findParameter(id) {
        for (var i=0; i<parameters.length;i++)
            if (parameters[i].id===id) return parameters[i];
        return null;
    }
  
    var index=-1;
    $scope.edit=function(i) {
        index=i;
        var c=new Rule($scope.rules[index]); // clone
        edit(c, null, function (c) {
            if (c.op==='delete') $scope.rules.splice(index,1);
            else if (c.op==='edit') {
                $scope.rules[index]=c.rule; 
                c.rule.updateStretchs($scope.periodeIn,$scope.periodeEnd);
            } else if (c.op==='clone') {
                $scope.rules.push(c.rule);
            }
        });
    };
    
    function edit(r, setValues, onEdited) {        
        var $dlgCalendar = $uibModal.open({
            animation: true,
            templateUrl: 'rule.html',
            controller: 'RuleCtrl',
            windowClass: 'foo',
            backdropClass: 'foo',
            size:'lg',
            resolve: { rule: function () { return r;} ,
                setValues:function(){ return setValues;},
                parameters:function() {return parameters; },
                options:function() {return options; },
                ruleSets:function() {return ruleSets; }                
            }
        });
        $dlgCalendar.result.then(onEdited, function () { });
    }
    
    $scope.add=function() { 
        var rule=new Rule();        
        edit(rule, function(r) {
            r.name='...';
            var lastRule=$scope.rules.length>0?$scope.rules[$scope.rules.length-1]:null;
            r.ordering=lastRule?lastRule.ordering+1:1;
            r.__sellDateIn=new Date();
            r.__sellDateEnd=new Date();
        }, function (c) {
            if (c.op==='edit') {
                $scope.rules.push(c.rule);
                c.rule.updateStretchs($scope.periodeIn,$scope.periodeEnd);
            }
        });
    };
    
    $scope.orderUp=function(index,event) {
        if (index>0) swap(index-1,index);
        event.stopPropagation();
    };
    
    $scope.orderDown=function(index,event) {
        if (index<$scope.rules.length-1) swap(index,index+1);
        event.stopPropagation();
    };
    
    function swap(indexA, indexB) {        
        var rA=$scope.rules[indexA];
        var rB=$scope.rules[indexB];
        $http.put(entityJson.substring(0,entityJson.length-1)+'s/order',{id1:rA.id, id2:rB.id}).then(function(){
            $scope.rules[indexA]=rB;
            $scope.rules[indexB]=rA;
            var ordering=rA.ordering;
            rA.ordering=rB.ordering;
            rB.ordering=ordering;
        });
    }
    
    $scope.stretchStyle=function(index, stretch) {
        return {
            position:'absolute', 
            height:'20px', 
            'background-color':$scope.colors[index % $scope.colors.length],
            width:stretch.width+'%',
            left:stretch.left+'%'
        };
    };
   
   $scope.monthStyle=function(index,month) {
        return { 
            position:'absolute',
            'margin-top':'-8px',
            height:'1000px', 
            'text-align':'center',
            'line-height': '35px', 
            'background-color':$scope.monthColors[index%2],
            width:month.width+'%',
            left:month.left+'%'
        };
   };
});

app.controller('RuleCtrl', function ($scope, $http, $uibModalInstance, $uibModal, rule, setValues, parameters, options, ruleSets) {
    $scope.ruleSets=ruleSets;
    $scope.options=[];
    for (var i=0;i<options.length;i++)
        $scope.options.push({id:options[i].id, name:options[i].name, parameterName:options[i].parameter.name, parameterId:options[i].parameter.id});
    
    $scope.tab=0;
    
    $scope.rule=rule;    
    jsonInit(rule);
    var initial=angular.copy(rule);    
    if (setValues) setValues(rule);
    
    function optionToMultiselect(ruleOption) {
        for (var i=0;i<$scope.options.length;i++) 
            if ($scope.options[i].id===ruleOption.optionId) {
                $scope.options[i].dispo=ruleOption.dispo;
                return $scope.options[i];
            }
        return null; 
    }
    
    //separate row and column options
    var rowOptions=[], colOptions=[];
    for (var i=0;i<rule.options.length;i++) 
        if (rule.options[i].isHorizontal) colOptions.push(rule.options[i]);
        else rowOptions.push(rule.options[i]);
    prepareMultiSelect(rowOptions, $scope.rule, '__rows', optionToMultiselect);
    prepareMultiSelect(colOptions, $scope.rule, '__columns', optionToMultiselect);
    
    var rowDimensions, colDimensions;
    
    $scope.optionsChange=function() {
        //rates
        rowDimensions=new DimensionIterator($scope.rule.__rows);
        colDimensions=new DimensionIterator($scope.rule.__columns);
        $scope.columns=colDimensions.getColumns();
        $scope.rows=rowDimensions.getRows();
        loadRates();
        
        //dispos
        rule.__dispoOptions=[];
        for (var i=0;i<$scope.rule.__rows.length;i++) {
            var o=$scope.rule.__rows[i];
            var parameter=find(parameters,o.parameterId);
            if (parameter.hasDispo) rule.__dispoOptions.push(o);
        }
        for (var i=0;i<$scope.rule.__columns.length;i++) {
            var o=$scope.rule.__columns[i];
            var parameter=find(parameters,o.parameterId);
            if (parameter.hasDispo) rule.__dispoOptions.push(o);
        }
    };    
    
    function loadRates() {
        var oldCombinations=rule.combinations;
        if (rule.__lostCombinations) oldCombinations=oldCombinations.concat(rule.__lostCombinations);
        rule.combinations=[];
        rowDimensions.begin();
        var row=0;
        while (rowDimensions.next()) {
            $scope.rows.items[row].combinations=[];
            colDimensions.begin();
            while (colDimensions.next()) {
                var combination={rate:null, options:colDimensions.getOptionIds().concat(rowDimensions.getOptionIds())};
                $scope.rows.items[row].combinations.push(rule.combinations.length);
                rule.combinations.push(combination);
            }
            row++;
        }
        
        var candidates={};
        rule.__lostCombinations=[];
        for (var i=0;i<oldCombinations.length;i++) {
            var row=rowDimensions.getIndex(oldCombinations[i].options);
            if (row.matches>0) {
                var col=colDimensions.getIndex(oldCombinations[i].options);
                if (col.matches>0) {
                    var pos=row.index*colDimensions.getN()+col.index;
                    if (!candidates[pos]) candidates[pos]=[];
                    candidates[pos].push({matches:row.matches+col.matches, combination:oldCombinations[i]});
                } else {
                    rule.__lostCombinations.push(oldCombinations[i]);
                }
            } else {
                rule.__lostCombinations.push(oldCombinations[i]);
            }
        }
        
        for (var i=0;i<rule.combinations.length;i++) {
            if (candidates[i]) {
                var bestMatch=null;
                var matches=candidates[i];
                for (var j=0;j<matches.length;j++) {
                    var match=matches[j];
                    if (!bestMatch || bestMatch.matches<match.matches) bestMatch=match;
                }
                if (bestMatch) {
                    //check if array of options have the same elements
                    if (!hasSameOptions(rule.combinations[i].options, bestMatch.combination.options))
                        bestMatch.combination.options=rule.combinations[i].options;
                    
                    //replace combination by the previous one
                    rule.combinations[i]=bestMatch.combination;                    
                }
                for (var j=0;j<matches.length;j++) {
                    var match=matches[j];
                    if (bestMatch!==match && match.combination.rate!==null) rule.__lostCombinations.push(match.combination);
                }
            }
        }
    }
    
    function hasSameOptions(newOptions, oldOptions) {
        if (newOptions.length!==oldOptions.length) return false;
        
        for (var i=0;i<newOptions.length;i++) {
            if (!find(oldOptions,newOptions[i].id)) return false;            
        }
        return true;
    }
    
    
    $scope.optionsChange();
        
    $scope.aDays={};
    
    $scope.addCalendar=function() {
        rule.calendars.push(new Calendar());
    };
    
    $scope.delCalendar=function(index) {
        rule.calendars.splice(index,1);
    };   
    
    function multiselectToOption(ruleOption, option) {
        ruleOption.dispo=option.dispo?option.dispo:null;
        ruleOption.isHorizontal=option.isHorizontal;
    }
    
    $scope.ok = function () {
        rule.update();        
        
        for (var i=0;i<rule.__rows.length;i++) rule.__rows[i].isHorizontal=false;
        for (var i=0;i<rule.__columns.length;i++) rule.__columns[i].isHorizontal=true;
        finishMultiSelect(rule.__rows.concat(rule.__columns), rule, 'options','optionId', multiselectToOption);
        
        var diff=jsonDiff(initial,rule);
        var data = JSON.stringify(diff);
        $http.put(entityJson+rule.id,data).then(function(response){
            if (response.data.ok) {
                if (rule.id===-1) rule.id=response.data.id;
                $uibModalInstance.close({op:'edit', rule:rule});            
            }
            else {
                jsonAddErrors(rule,response.data.errors);
            }
        });
    };
    
    $scope.clone=function() {
        var clone=angular.copy(rule);
        clone.id=undefined;
        for (var i=0;i<clone.calendars.length;i++) { clone.calendars[i].id=undefined; clone.calendars[i].__row=undefined; }
        var diff=jsonDiff({},clone);
        var data = JSON.stringify(diff);
        $http.put(entityJson+clone.id,data).then(function(response){
            if (response.data.ok) {
                clone.id=response.data.id;
                $uibModalInstance.close({op:'clone', rule:clone});            
            } else {
                jsonAddErrors(rule,response.data.errors);
            }
        });
    };    

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    
    $scope.delete=function() {
        var $dlgClient = $uibModal.open({
            animation: true,
            templateUrl: '../dlg.html',
            controller:'DlgCtrl',
            size:'md',
            resolve: { title: function(){return 'Eliminar Tarifa';}, message: function(){return 'Vols eliminar la tarifa '+rule.name+' ?';}, }
        });
        $dlgClient.result.then(function () {
            $http.delete(entityJson+rule.id).then(function(response){
                $uibModalInstance.close({op:'delete', rule:null});            
            });
        }, function () {      
        });        
    };
  
});


    