app.controller('UsersCtrl', function ($scope, $http, $uibModal) {
    $scope.datatable={
        pageLength: pageLength,            
        ajax: entitiesJson,
        order: [ 1, 'asc' ],
        columns: [{ "orderable": false},{},{},{},{"orderable": false}],
        del:function(entity, reload) {
            var id=entity?entity.id:-1;
            var $dlgClient = $uibModal.open({
                animation: true,
                templateUrl: 'dlg.html',
                controller:'DlgCtrl',
                size:'md',
                resolve: { title: function(){return 'Eliminar Usuari';}, message: function(){return 'Vols eliminar l\'Usuari '+entity.name+' ?';}, }
            });
            $dlgClient.result.then(function () {
                    $http.delete(entityJson+'/'+id).then(function(){
                        reload(); //refresh datatable                    
                    });
                }, function () {      
                });
        },
        edit:function(entity, reload) {
            var $dlg = $uibModal.open({
                animation: true,
                templateUrl: 'user.html',
                controller: 'UserCtrl',
                size:'lg',
                resolve: { id: function(){return entity?entity.id:-1;} }
            });
            $dlg.result.then(function (c) {
                    reload();
                }, function () {      
                });
        }
    };  
});

app.controller('UserCtrl', function ($scope, $http, $uibModalInstance, id) {
    $scope.roles=[{id:0,name:'Usuari'},{id:1,name:'Administrador'}];
    $scope.countries=countries;
    
    $http.get(entityJson+'/'+id).then(function(response) {
        $scope.initial= response.data;
        $scope.initial.password=$scope.initial.repeatPassword='';
        $scope.entity=angular.copy(response.data);
    });    
    
    $scope.ok = function () {
        var diff=jsonDiff($scope.initial,$scope.entity);
        var data = JSON.stringify(diff);
        $http.put(entityJson+'/'+id,data).then(function(response){
            if (response.data.ok) $uibModalInstance.close();            
            else {
                jsonAddErrors($scope.entity,response.data.errors);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };    
    
    $scope.isPasswordValid=function() {
        return $scope.entity?$scope.entity.password==$scope.entity.repeatPassword:true;
    };
});
