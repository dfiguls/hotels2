app.controller('SpecialRatesCtrl', function ($scope, $http, $filter, $uibModal) {
    $scope.isCollapsed=true;   
    $scope.ruleSets=ruleSets;
    $scope.selectedRuleSet=1;
    
    var dateFilter = $filter('date');
    $scope.dateIn=new Date();
    
    var dateEnd=new Date($scope.dateIn);
    dateEnd.setDate(dateEnd.getDate()+13);  //default two weeks
    $scope.dateEnd=dateEnd;
    
    
    $scope.dateInOpened=false;
    $scope.dateInOpen=function() { $scope.dateInOpened=true; };
    $scope.dateEndOpened=false;
    $scope.dateEndOpen=function() { $scope.dateEndOpened=true; };
    
    function getSelectedOptions(object) {
        var options=[];
        for (var property in object) if (object[property]) options.push(property);
        return options;
    }
    
    function getURL() {
        return specialratesJson+"?dateIn="+dateFilter($scope.dateIn,'dd-MM-yyyy')
               +"&dateEnd="+dateFilter($scope.dateEnd,'dd-MM-yyyy')+
               "&idRuleSet="+$scope.selectedRuleSet;
    }
    
    $scope.rateChange=function(index, day) {
        var formula=day.object.formula;
        if (formula!==null && formula.indexOf('+')===-1 && formula.indexOf('-')===-1) {
            //hem modificicat un valor base
            var value=day.value;
            for (var i=0;i<$scope.rateRows.items.length;i++) {
                var day=$scope.rateRows.items[i].days[index];
                //si aquest rate depèn del modificat
                if (day.object.formula.indexOf(formula)!==-1) {
                    var f=day.object.formula.replace(formula,value);
                    day.value=eval(f);
                }
            }
        }
    };
        
    
    $scope.update=function() {
        $scope.isCollapsed=true;
        $http.get(getURL()).then(function(response) {  
            $scope.initial= response.data.map;
            jsonInit($scope.initial);
            $scope.map=angular.copy($scope.initial);            
            
            var days=[];
            var date=new Date($scope.dateIn);
            while (date<=$scope.dateEnd) {
                days.push(''+date.getDate()+'/'+(date.getMonth()+1));
                date.setDate(date.getDate()+1);
            }
            $scope.days=days;
            
            var options=[];
            var dispoRows=$scope.dispoRows=[];
            for (var i=0;i<response.data.parameters.length;i++) {
                var parameter=response.data.parameters[i];
                for (var j=0;j<parameter.options.length;j++) {
                    var option=parameter.options[j];
                    option.parameterId=parameter.id;
                    options.push(option);
                    if (parameter.hasDispo) dispoRows.push({id:option.id, name:option.name, days:[]});
                }
            }
            dimensions=new DimensionIterator(options);
            $scope.rateRows=dimensions.getRows(true);                    
            var rateRows=$scope.rateRows.items;
            
            
            //prepare hash to access the row by combinationIds (ex: 2,3,6)
            var combinationIds={};
            for (var i=0;i<rateRows.length;i++) {
                combinationIds[rateRows[i].combinationIds]=rateRows[i];
                rateRows[i].days=[];
            }
            
            for (var i=0;i<$scope.map.length;i++) {
                //add empty data at each cell
                for (var j=0;j<rateRows.length;j++)
                    rateRows[j].days.push({object:null});
                
                //place cacheRates to their place
                var cacheCalendar=$scope.map[i];
                for (var j=0;j<cacheCalendar.cacheRates.length;j++) {
                    var cacheRate=cacheCalendar.cacheRates[j];
                    var item=combinationIds[cacheRate.combinationIds];
                    if (item) {                        
                        var day={base:cacheRate.rate, value:cacheRate.specialRate?cacheRate.specialRate:null, object:cacheRate};
                        item.days[item.days.length-1]=day;
                    }
                }
                
                //add empty data at each cell
                for (var j=0;j<dispoRows.length;j++)
                    dispoRows[j].days.push({object:null});
                
                //place cacheRispos to their place
                for (var j=0;j<cacheCalendar.cacheDispos.length;j++) {
                    var cacheDispo=cacheCalendar.cacheDispos[j];
                    var option=find(dispoRows,cacheDispo.optionId);
                    if (option) {
                        var day={base:cacheDispo.dispo-cacheDispo.reservations, 
                            value:cacheDispo.specialDispo!==null?cacheDispo.specialDispo-cacheDispo.reservations:null, 
                            object:cacheDispo
                        };
                        option.days[option.days.length-1]=day;
                    }
                }
            }
            
       });
    };   
        
    
    $scope.ok=function() {
        var dlgSaving = $uibModal.open({
            animation: false,
            templateUrl: '../dlg.html',
            controller:'DlgCtrl',
            size:'md',
            backdrop : 'static',keyboard :false,
            resolve: { title: function(){return 'Guardant...';}, message: function(){return '';},onopened:function(){
                    var i=1;
                    i++;
            }}
        });
        dlgSaving.opened.then(function(){
            //rate rows to map
            for (var i=0;i<$scope.rateRows.items.length;i++) {
                var row=$scope.rateRows.items[i];
                for (var j=0;j<row.days.length;j++) {
                    var rowDay=row.days[j];
                    var mapDay=$scope.map[j];
                    var value=toFloat(rowDay.value); //convert string rate to float
                    if (rowDay.object) { //day with cacheRate
                        var o=rowDay.object;
                        if (value!==null) { //modified value
                            o.specialRate=value;
                        } else { 
                            o.specialRate=null;
                        }
                    } 
                }
            }
            
            //dispo rows to map
            for (var i=0;i<$scope.dispoRows.length;i++) {
                var row=$scope.dispoRows[i];
                for (var j=0;j<row.days.length;j++) {
                    var rowDay=row.days[j];
                    var mapDay=$scope.map[j];
                    var value=toInt(rowDay.value); //convert string dispo to int
                    if (rowDay.object) { //day with cacheDispo
                        var o=rowDay.object;
                        if (value!==null) { //modified value
                            var dispo=value+o.reservations;
                            if (dispo===rowDay.base) o.specialDispo=null;
                            else o.specialDispo=dispo;
                        } else { 
                            o.specialDispo=null;
                        }
                    } 
                }
            }

            //calculate differences
            var diff=jsonDiff($scope.initial,$scope.map);
            var data = JSON.stringify(diff);
            $http.put(getURL(),data).then(function(response){
                if (response.data.ok) {   
                    $scope.update();
                } else {
                    alert('error');
                }
                dlgSaving.dismiss();
            });
        });       
    };
    
    
    $scope.update();
    
    
});
